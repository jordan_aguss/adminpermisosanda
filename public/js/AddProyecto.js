var map;
var geocoder;
var drawingmanager;
var rutaAnterior = null;
var validator;
var direccion = "";
var areaPoligono = 0;
var contador = 1;
var urlAnalisis;
var urlPreguntas;

var ping = null;

initMap();

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat:13.98823513722932, lng:  -89.64003246277571},
            zoom: 27,
            mapTypeId: 'hybrid'
        });

        map.addListener("click", (mapsMouseEvent) => {

            if(ping){
                ping.setMap(null);
            }

            ping = new google.maps.Marker({
                position: mapsMouseEvent.latLng,
                map,
                title: "Hello World!",
              });

        });

        var radioBase = new google.maps.Circle({
            //            strokeColor: '#FF0000',
            //            strokeOpacity: 0.8,
                        strokeWeight: 0,
                        fillColor: 'gray',
                        fillOpacity: 0.35,
                        center: {lat:13.98823513722932, lng:  -89.64003246277571},
                        radius: 500          });
    radioBase.setMap(map);


    }

