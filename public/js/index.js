$("body").on("click", ".page-link", function(event){
    let url = $(this).attr('href');
    if(url.includes('page')){
        event.preventDefault();
        MakeRequestData(url, $(this).closest('.contentPager').attr('link'), true);
    }
});

function getDataRequest(IdForm, Datos){

    if(IdForm != ''){
        var Form = $(IdForm)[0];
        return new FormData(Form);
    }

    let DataSend = { '_token': $('input[name=_token]').val() };

    for( var i = 0; i < Datos.length; i++){
        let clave = Datos[i].split('/');
        DataSend[clave[0]] = clave[1];
    }

    return DataSend;
}


function ShowAlert(Titulo, Mensaje, Icono, showButton = true){
    Alerts.fire({
            title: Titulo,
            text: Mensaje,
            icon: Icono,
            confirmButtonColor: '#293643',
            showConfirmButton: true,
            allowOutsideClick: false,
            toast: true,
            confirmButtonText:'Aceptar'
        });

}

function MakeRequestData(
    Url,
    ItemReplace,
    ShowLoad = false,
    ModalName = '',
    type = 'GET',
    accion = 2,
    IdForm = '',
    ShowResult = false,
    closeModal= false,
    params = [],
    Metodo = SetDataResult){

    if(ValidarForm(IdForm)){ return; }
    let DataSend = getDataRequest(IdForm, params);
    if(ShowLoad) LoadingAlert();

    if(DataSend instanceof FormData){
        for (var pair of DataSend.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }
    }
    let confi = {
        type: type,
        url: Url,
        cache: false,
        data: (type == 'POST' ? DataSend : null),
        timeout: 800000,
        async: true,
        success: function(response) {
            if(ModalName != '') $(ModalName).modal('show');
            Metodo(ItemReplace, accion, response);
            Alerts.close();

            if(ShowResult) ShowAlert('Exito', 'El proceso se ha realizado correctamente','success', false);
            if($(ModalName).is(':visible') && closeModal) $(ModalName).modal('hide');
        },
        error: function(error){ ErrorRequest(error); }
    }

    if(IdForm != '') {
        confi['processData'] = false;
        confi['contentType'] = false;
    }

    $.ajax(confi);
}

function LoadPage(url){
    urlBack = url;
    MakeRequestData(url, '#bodyContent', true);
}


function printHTML() {
    if (window.print) {
      window.print();
    }
}

function LoadingAlert(){
    Alerts.fire({
        type: 'info',
        html: '<div class="spinner-border text-warning col-12" role="status"> <i class="fas fa-spinner"></i></div> <span class="col-12 text-center">Cargando</span>',
        allowOutsideClick: false,
        showConfirmButton: false,
        width: 160,
        padding: '0'
    });
}

function SetDataResult(ItemReplace, accion, response){
    switch(accion){
        case 0:
            $(ItemReplace).append(response);
            break;
        case 1:
            location.reload();
            break;
        case 2:
            $(ItemReplace).html(response);
            break;
        case 3:
            $(ItemReplace).replaceWith(response);
            break;
        case 4:
            $(ItemReplace).prepend(response);
            break;
    }
}

function ErrorRequest(response){

    Alerts.close();
    let r = jQuery.parseJSON(response.responseText);

    if(r.includes('^')){
        ShowAlert('Error', r.replace('^',''),'error');
    }
    else{
        ShowAlert('Error', 'Ocurrio un error al procesar la solicitud.','error');
    }
}

function AlertConfirmacion(TextAlert){
    return new Promise((resolve, reject) => {
        let Titulo = $(TextAlert).text();

        Alerts.fire({
            text: Titulo,
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            allowOutsideClick: false,
            confirmButtonText: 'Aceptar',
        }).then((result) =>{
            resolve(result.isConfirmed);
        });
    });
}

function readURL(input, ImgReplace) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(ImgReplace).attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}


function ValidarForm(form){
    if(form == '') return false;
    let newform = $(form)[0];

    if (!newform.checkValidity()) {
        newform.classList.add('was-validated');
        ShowAlert('Error', 'Uno o mas campos se han ingresado incorrectamente', 'error');
        return true;
    }
    return false;
}

function setDataTab(url, div = ""){
	$.ajax({
        url: url,
        type: 'GET',
        success: function(response){
			$('#' + div ).html(response);
			Alerts.close();
        },
        error: function(error){
			Alerts.close();
            var errors = error.responseJSON.errors;
			$('#' + div ).html("<h4 class='text-muted text-center mt-3'>No se encontraron registros</h4>");
            $.each(errors, function(key, val){
                $.each(val, function(key, mensaje){
                    app.notificacion('error', 'Ha ocurrido un error.'+mensaje);
                });
                return false;
            })
        }
    });
}
