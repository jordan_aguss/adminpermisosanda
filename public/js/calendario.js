
        var CalendarioItem;
        var FechaInicio;
        var FechaFin;
        var urlModal;

        function IniciarCalendario(){

            CalendarioItem = document.getElementById('calendario');

            var calendar = new FullCalendar.Calendar(CalendarioItem, {
                initialView: 'dayGridMonth',
                locale: "es",
                timeZone: 'America/El_Salvador',
                displayEventTime : true,
                headerToolbar: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
                },  
                editable: true,
                selectable: true,
                selectHelper: true,
                dayMaxEvents: true, // allow "more" link when too many events
                events: [
                        {
                            id:2001,
                            title: 'All Day Event',
                            start: '2021-08-04T15:00:00',
                            end : '2021-08-04T19:15:16'
                        },
                        {
                            id:0,
                        start: '0001-01-01',
                        end: '{{$Fecha}}',
                        overlap: false,
                        display: 'background'
                        },
                    ],
    

                select:function(data){
                    FechaInicio = data.startStr;
                    FechaFin = data.endStr;
                },
                
                eventClick: function(info) {
                    abrirModal(urlModal);
                },

                eventResize:function(data){
                    console.log('Funciona--------------------------------');
                    console.log(data.event.start.toISOString());
                    console.log(data.event.end.toISOString());
                    console.log('---------------------------------------------');
                },

                eventDrop: function(data){
                    console.log('Funciona el eliminar data -----------------------------------------');                    
                    console.log(data.event);
                    console.log(data.event.start.toISOString());
                    console.log(data.event.end.toISOString());
                    console.log('---------------------------------');
                }
            });
            calendar.render();
        }
        

        function StartScript(url){
            urlModal = url;
            IniciarCalendario();
        }