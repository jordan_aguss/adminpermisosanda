<?php

namespace Database\Seeders;

use App\Models\Usuario;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use App\Http\Controllers\RoleController;
use Spatie\Permission\Models\Permission;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\PermissionController;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***
         * Admin / Users
         */

        Permission::updateOrCreate(['name' => UsuarioController::PERMISSIONS['create']], [
            "description"   => "Creacion de Usuarios"
        ]);
        Permission::updateOrCreate(['name' => UsuarioController::PERMISSIONS['show']], [
            "description"   => "Listado y detalle de usuario"
        ]);
        Permission::updateOrCreate(['name' => UsuarioController::PERMISSIONS['edit']], [
            "description"   => "Edición de usuario"
        ]);


        /***
         * Admin / Permissions
         */

        Permission::updateOrCreate(['name' => PermissionController::PERMISSIONS['show']], [
            "description"   => "Listado y detalle de permisos"
        ]);

        /***
         * Admin / Roles
         */

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['create']], [
            "description"   => "Creación de Roles"
        ]);
        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['show']], [
            "description"   => "Listado y detalle de roles"
        ]);

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['edit']], [
            "description"   => "Edición de rol"
        ]);

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['delete']], [
            "description"   => "Eliminación de rol"
        ]);

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['agencia']], [
            "description"   => "Rol de Agencia"
        ]);

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['fRegional']], [
            "description"   => "Rol de Factibilidad Regional"
        ]);

        Permission::updateOrCreate(['name' => RoleController::PERMISSIONS['tecnico']], [
            "description"   => "Rol Tecnico de campo"
        ]);


        // Create roles and assign created permissions
        // $role = Role::updateOrCreate(['name' => 'administrador']);
        // $role->givePermissionTo(Permission::all());
        // $role = Role::updateOrCreate(['name' => 'Usuario Externo']);

        // $role = Role::updateOrCreate(['name' => 'Usuario Externo']);

        // // Assign roles to demo users
        // $user = new Usuario();
        // $user->usua_mail = "ptorres@tec101.com";
        // $user->usua_pass = password_hash("123", PASSWORD_DEFAULT);
        // $user->usua_nombre = "administrador";
        // $user->save();
        // $user->assignRole('administrador');

    }
}
