<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DepartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('GUANA_SEA.departamentos')->insert([
            'DEPA_NOMBRE' => Str::random(10),
            'DEPA_REGION' => Str::random(10),
            'DEPA_ESTADO' => 1,
        ]);
    }
}
