<div class="modal modal-blur fade" id="modal-principal" tabindex="-1" role="dialog" aria-hidden="true">
    <div {{ $attributes->merge(['class' => 'modal-dialog modal-lg'.($m_style ?? '')]) }} role="document">
        <div class="modal-content">
        </div>
    </div>
</div>
