<!-- template list component-->
<div class="col-12">
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">{{ $t }}</h3>
        <div class="ms-auto text-muted">
            {{ $ts }}
            <div class="ms-2 d-inline-block">
              <input type="text" class="form-control form-control-sm" aria-label="Search invoice">
            </div>
          </div>
      </div>
      <div class="table-responsive">
        <table class="table card-table table-vcenter text-nowrap datatable">
          <thead>
            <tr>
              {{ $headers }}
            </tr>
          </thead>
          <tbody>
            {{ $items }}
          </tbody>
        </table>
      </div>
      @if ($pg != 0)
        <span class="m-auto">
        {{ $paginator }}
        </span>
      @endif
    </div>
  </div>