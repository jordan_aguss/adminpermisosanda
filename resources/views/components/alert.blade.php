<div class="alert alert-{{$tipo}} alert-dismissible" role="alert">
    <div class="d-flex">
      <div></div>
      <div>
        <h4 class="alert-title">Error&hellip;</h4>
        @if (is_object($mensaje))
            <ul>
                @foreach ($mensaje->all() as $error)
                    <li><div class="text-muted">{{$error}}</div></li>
                @endforeach
            </ul>
        @else
        <div class="text-muted">{{$mensaje}}</div>
        @endif

      </div>
    </div>
    <a class="btn-close" data-bs-dismiss="alert" aria-label="close"></a>
  </div>
