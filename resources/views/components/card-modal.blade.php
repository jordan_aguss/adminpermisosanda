<div class="card">

    <div class="card-header pt-3 pb-3">
        <h2 class="card-title m-0">{{$titulo}}</h2>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>

    <div class="card-body">
        {{$body}}
    </div>

    <div class="card-footer text-end">
        {{$footer}}
    </div>

</div>
