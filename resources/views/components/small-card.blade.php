<div class="col-md-6 col-xl-3">
    <div class="card card-sm">
      <div class="card-body">
        <div class="row align-items-center">
          <div class="col-auto">
            <span class="bg-{{$bg}} text-white avatar">
                <i class="fas fa-{{$icon}}"></i>
            </span>
          </div>
          <div class="col">
            <div class="font-weight-medium">
              {{$title}}
            </div>
            <div class="text-muted">
              {{$detail}}
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>