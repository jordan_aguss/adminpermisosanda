
    <div class="card p-0" id="{{isset($idCardTable) ? $idCardTable : 'card-table' }}">
        <div class="card-header">
            <h3 class="card-title"> {{isset($titulo) ? $titulo : ''}} </h3>
            <div class="ms-auto text-muted">
                <div class="col-auto ms-auto d-print-none">
                    <div class="d-flex">

                        @if (isset($filtros))
                            <select class="form-control" id="OpcionBuscar">
                                @foreach ($filtros as $item)
                                    <option value="{{$item[0]}}">{{$item[1]}}</option>
                                @endforeach
                            </select>
                        @endif

                    <div class="me-2 {{isset($classBuscar) ? $classBuscar : ''}}">
                        <div class="input-icon">
                            <input type="text" id="{{isset($IdBuscar) ? $IdBuscar: "Buscador"}}" class="form-control" placeholder="Buscar..."
                            {{isset($accionBuscar) ? $accionBuscar: ""}} >
                            <span class="input-icon-addon">
                                <i class="fas fa-search"></i>
                            </span>
                        </div>
                    </div>

                    @if (isset($btnAdicionales)) {{$btnAdicionales}} @endif

                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive">
            @csrf
            <table class="table card-table table-vcenter text-nowrap" id="data-table">
                <thead>
                    <tr>
                        {{$header}}
                    </tr>
                </thead>
                <tbody @if (isset($idTabla)) id="{{$idTabla}}" link="#{{$idTabla}}" @else id="TableBody" link="TableBody" @endif class="contentPager">
                    {{$body}}
                </tbody>
            </table>

            @if (isset($paginador))
                {{$paginador}}
            @endif
        </div>

    </div>
