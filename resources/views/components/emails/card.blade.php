<div class="vh-100 d-flex flex-column align-items-center pt-5 bg-secondary">
    <div>
        {{ $logo }}
    </div>
    <div class="w-100 mt-5 mt-5 bg-white shadow-lg rounded">
        {{ $slot }}
    </div>
</div>