<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Acta</title>
    <style>
        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%;
        }

        body {
            margin: 0;
        }

        main {
            display: block;
        }

        a {
            background-color: transparent;
        }

        b,
        strong {
            font-weight: bolder;
        }

        small {
            font-size: 80%;
        }

        img {
            border-style: none;
        }

        .container {
            margin: 0 auto;
            width: 100%;
        }

        .text-center {
            text-align: center;
        }

        .uppercase {
            text-transform: uppercase;
        }

        p {
            margin-bottom: 1.25rem;
        }

        p.mb {
            margin-bottom: 2.5rem;
        }

        @media screen and (max-width: 992px) {
            .container {
                width: 75%
            }
        }

        @media screen and (max-width: 768px) {
            .container {
                width: 80%
            }
        }

        @media screen and (min-width: 1200px) {
            .container {
                width: 70%
            }
        }

    </style>
</head>

<body>
    <main class="container">
        <h1 class="text-center mb">DIRECCIÓN GENERAL DE</h1>
        <h3>Caso N:</h3>
        <p>
        <h5>En: ,</h5>
        <h5>municipio de ,departamento de ,</h5>
        <h5>a las horas con minutos del día del año dos mil .</h5>
        <h5>Siendo este lugar, día y año señalados, para practicar <b>visita de campo para verificar denuncia
                ambiental,</b> según consta en las
            diligencias de actuaciones previas, promovidas ante el Ministerio de Medio Ambiente y Recursos Naturales,
            por</h5>
        <h5>en contra de</h5>
        <h5>constituyo el (la) infraescrito (a)</h5>
        <h5>como personal delegado de la institución contando con la asistencia de las partes</h5>
        <h5>Procedí (mos) a dar inicio a la mencionada diligencia, obteniendo el siguiente resultado:</h5>
        <h5>y no habiendo más que hacer constar, se da por finalizada la presente Acta y dándole lectura integramente en
            un solo acto sin interrupción, se ratifica y para constancia firmamos.</h5>


        </p>
        </div>

</body>

</html>
