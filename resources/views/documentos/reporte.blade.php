<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formato de Informe Técnico</title>
    <style>
        html { line-height: 1.15; -webkit-text-size-adjust: 100%; }
        body { margin: 0; }
        main { display: block; }
        a { background-color: transparent; }
        b,
        strong { font-weight: bolder; }
        small { font-size: 80%; }
        img { border-style: none; }
        .container { 
            margin: 0 auto;
            width: 100%; 
        }
        .text-center { text-align: center; }
        .uppercase { text-transform: uppercase; }
        p {
            font-weight: bold;
            margin-bottom: 1.25rem;
        }
        p.mb { margin-bottom: 2.5rem; }
        @media screen and (max-width: 992px) {
            .container { width: 75% }
        }
        @media screen and (max-width: 768px) {
            .container { width: 80% }
        }
        @media screen and (min-width: 1200px) {
            .container { width: 70% }
        }
    </style>
</head>

<body>
    {{-- <img src="" alt="Escudo de El Salvador del MARN"> --}}
    <main class="container">
        <p class="text-center mb">Nombre área organizativa responsable</p>
        <p>Fecha y hora de inspección:</p>
        <p>Ubicación del lugar:</p>
        <p>Coordenadas:</p>
        <p>Técnicos que realizaron la inspección</p>
        <p>Nombre del denunciado:</p>
        <p>Objetivo de la Inspección:</p>
        <p>Antecedentes o reseña del caso abordado:</p>
        <p>Conclusiones:</p>
        <p>Medidas ambientales:</p>
        <p class="mb">Fecha de elaboración de informe:</p>
        <p class="text-center mb">Firmas</p>
        <p class="text-center uppercase">Albúm Fotografico</p>
    </main>
</body>
</html>