<x-card-modal>
    @slot('titulo')
    Opciones de la pregunta
    @endslot

    @slot('body')
        <x-table>  
            @slot('idTabla', 'TableOpciones')

            @slot('btnAdicionales')
                <div class="col-auto">
                    <button type="button" class="btn btn-icon btn-primary" 
                    onclick="MakeRequestData( '{{ route('form-opciones', [0, $pregunta]) }}', '.modal-content', true, '#modal-principal')">
                    <i class="fas fa-plus"></i>&nbsp;&nbsp;Nuevo</button>
                </div>    
            @endslot


            @slot('header')
                <th>Nombre</th>
                <th>Preguntas</th>
                <th>Acciones</th>
            @endslot
            
            @slot('body')
                @include('main.preguntas-opcion.data')
            @endslot
        </x-table>
    @endslot

    @slot('footer')
        <button class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp; &nbsp; Cancel</button>
    @endslot

</x-card-modal>
