<x-card-modal>
    @slot('titulo')
    Información de la opción
    @endslot

    @slot('body')
        <form id="FrmOpcion" novalidate>
            @csrf        
            <div>
                <input type="text" class="form-control" name="pregunta" id="pregunta" value="{{ $pregunta }}" hidden>
                <input type="hidden" name="id" value="{{$Info->opci_id}}">
                <div class="row">    
                    <div class="col-12 ">
                        <div class="mb-3">
                            <label class="form-label">Nombre de la opción</label>
                            <input type="text" class="form-control" name="Nombre" id="Nombre" value="{{$Info->opci_nombre}}" maxlength="100"
                            required>
                        </div>
                    </div>
        
                </div>
            </div>
        </form>
    @endslot

    @slot('footer')
        <a onclick="MakeRequestData( '{{ route('get-opciones', [$pregunta]) }}', '.modal-content', true, '#modal-principal')" class="btn btn-danger">
            Cancel &nbsp; <i class="text-light fas fa-times"></i> </a>

        <a id="guardar" type="button" onclick="MakeRequestData( '{{ route('save-opcion') }}', '.modal-content', true,
         '#modal-principal', 'POST', 2, '#FrmOpcion')" class="btn btn-primary">
            <i class="text-light fas fa-save"></i>&nbsp;&nbsp; Guardar
        </a>    
    @endslot

</x-card-modal>


