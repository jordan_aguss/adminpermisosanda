@foreach ($Opciones as $item)
    <tr id="opcion-{{$item->opci_id}}">
        <td> {{$item->opci_nombre}} </td>
        <td> {{$item->preguntas}} </td>
        <td> 
            <button class="btn btn-warning"
            onclick="MakeRequestData( '{{ route('form-opciones', [$item->opci_id, $pregunta]) }}', '.modal-content', true, '#modal-principal')">
                <i class="fas fa-edit"></i>
            </button>
            <button class="btn btn-danger"
                onclick="MakeRequestData( '{{ route('eliminar-opcion', [$item->opci_id, $pregunta]) }}', '.modal-content', true, '#modal-principal')">
                    <i class="fas fa-trash"></i>
            </button>
        </td>
    </tr>    
@endforeach