
<x-card-modal>
    @slot('titulo')
        Verificación de documentación
    @endslot

    @slot('body')

    <style>
        .my-custom-scrollbar {
position: relative;
height: 200px;
overflow: auto;
}
.table-wrapper-scroll-y {
display: block;
}
    </style>

    <div class="row">
        <div class="col-lg-12">
            <div class="mb-3 table-wrapper-scroll-y my-custom-scrollbar">
                <label class="form-label">Listado de Preguntas</label>
                <table class="table table-bordered table-striped mb-0">
                    <thead>
                        <tr>

                          <th scope="col">Nombre</th>
                          <th scope="col">Respuesta</th>
                          <th scope="col">Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="denegarPermiso">
                          @include('main.permisos.tecnicos.denegar-permiso')
                      </tbody>
                </table>
            </div>
        </div>

    <form id="formDenegar" method="POST" novalidate>
        @csrf
        <div>
            <input type="hidden" class="form-control d-none" name="id" id="" value="{{$request->Id}}">

                <hr>

                <div class="col-lg-12">
                    <div class="mb-3">
                        <label class="form-label">Observaciones</label>
                       <textarea id="text" class="form-control" name="observaciones" style="height: 100px"> </textarea>
                    </div>

                </div>
            </div>
        </div>

        <a onclick=" MakeRequestData(
            '{{route('denegarPermiso')}}',
            '#denegarPermiso',
            true,
            '',
            'POST',
            0,
            '#formDenegar', true)"  href="" type="button" class="btn btn-info" data-bs-dismiss="modal">Enviar Observaciones</a>
    </form>
    @endslot

    @slot('footer')

    @endslot

</x-card-modal>



