<x-card-modal>
    @slot('titulo')
        Secciones de Comentario
    @endslot
    @slot('body')
        <div class="row">
            <small class=""><strong>Agrega un comentario</strong></small>
        </div>
        <form id="formComentario" novalidate autocomplete="off">
            @csrf
            <input type="hidden" value="{{ $idPermiso }}" name="id">
            <input type="hidden" value="{{ $idFormulario }}" name="idForm">
            <input type="hidden" value="{{isset($request->estado) ? $request->estado : '' }}" name="estado">
            <div class="row gx-3 mb-3">
                <div class="col-md-12">
                    <div class="form-floating">
                        <textarea class="form-control" maxlength="255" name="Comentario" placeholder="Deja tu comentario" id="Comentario" style="height: 150px"></textarea>
                        <label for="floatingTextarea " class="textGB">Comentario</label>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-floating">
                        <select name="asignar" id="asignar">
                            <option value="">Seleccionar Técnico</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
    @endslot
    @slot('footer')
        <button type="button" title="Aprobar" class="btn btn-success" type="button"
            onclick="MakeRequestData('{{route('EstadosSolicitud')}}', '#{{$request->Contenedor}}', true,
            '#modal-principal', 'POST', 1,'#formComentario' ,false, true)">
            <i class="fas fa-check"></i>&nbsp;Aprobar
        </button>
        <button class="btn btn-danger" type="button" data-bs-dismiss="modal">
            <i class="fas fa-times"></i></i>&nbsp;Cancelar
        </button>
    @endslot
</x-card-modal>
