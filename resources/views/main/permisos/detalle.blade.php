@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', $permiso->estg_nombre)
@section('scripts')
    <script src="{{ asset('js/index.js') }}" type="text/javascript"></script>
@endsection
@section('content')

    <div class="card">

        <div class="row d-flex align-items-center my-3 mx-3">
            <div class="col-sm-12 col-md-2">
                <a href="#" onclick="window.history.back()" class="btn btn-light" data-toggle='tooltip' title="Regresar"> <i
                        class="fas fa-chevron-left"></i> &nbsp; Regresar </a>
            </div>
            {{-- {{ $estados }} --}}
            <div class="col-sm-12 col-md-10">
                <div class="row d-flex justify-content-center my-0 py-0">

                    <h1 class="text-center font-weight-bold text-secundary my-0 py-0">{{ $permiso->form_nombre }}</h1>

                </div>
            </div>
        </div>
        <hr class="bg-secundary shadow border-4 border-top border-secundary my-0">

        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12 col-md-12 mb-sm-3 col-xl-4">
                        <div class="card">
                            <div class="row col-12 m-0 p-0">
                                <div class="card-body px-4 col-xl-12 col-md-6 col-sm-6">
                                    <div class="row  mb-4">
                                        <div class="col-1 d-flex justify-content-end"><a><i
                                                    class="text-center fas fa-info"></i></a></div>
                                        <div class="col-11">
                                            <h3 class="title">Información General</h3>
                                        </div>
                                        <hr class="bg-danger border-4 border-top border-primary my-0">

                                        <div class="col-sm-12 col-md-6">
                                            <div class="my-1">
                                                <div class="row my-0 py-0 ">
                                                    <h4 class="font-weight-bold my-0 py-0">
                                                        {{ \Carbon\Carbon::parse($permiso->perm_fecha)->format('d/m/Y') }}
                                                    </h4>
                                                </div>
                                                <div class="row my-0 py-0">
                                                    <p class="text-muted my-0 py-0">Fecha creación</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6">
                                            <div class="my-1">
                                                <div class="row my-0 py-0 ">
                                                    <h2 class="font-weight-bold my-0 py-0"> {{ $permiso->perm_codigo }}</h2>
                                                </div>
                                                <div class="row my-0 py-0">
                                                    <p class="text-muted my-0 py-0">Código de expediente</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="my-1">
                                                <div class="row my-0 py-0 ">
                                                    <h2 class="font-weight-bold my-0 py-0"> {{ $Tipo->opci_nombre }}</h2>
                                                </div>
                                                <div class="row my-0 py-0">
                                                    <p class="text-muted my-0 py-0">Tipo de proyecto</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="my-1">
                                                <div class="row my-0 py-0 ">
                                                    <h2 class="font-weight-bold my-0 py-0"> {{ $NombreP }}</h2>
                                                </div>
                                                <div class="row my-0 py-0">
                                                    <p class="text-muted my-0 py-0">Nombre del proyecto</p>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-sm-12 col-md-6">
                                            <div class="my-1">
                                                <div class="row my-0 py-0 ">
                                                    <h4 class="font-weight-bold my-0 py-0">{{ $permiso->esta_nombre }}</h4>
                                                </div>
                                                <div class="row my-0 py-0">
                                                    <p class="text-muted my-0 py-0">Estado</p>
                                                </div>
                                            </div>
                                        </div>

                                        <br>
                                        <a href="{{ route('permisos.detalle_preguntas', [$permiso->form_id, $permiso->perm_id, $permiso->perf_id]) }}"
                                            class="btn btn-secondary mt-2" data-toggle='tooltip' target="_blank"
                                            title="Detalle preguntas de categorización"> Detalle de Solicitud &nbsp; <i
                                                class="fas fa-chevron-right"></i> </a>

                                    </div>

                                </div>
                                <style type="text/css">
                                    #global {
                                        height: 300px;
                                        overflow-y: scroll;
                                    }

                                </style>
                            </div>

                        </div>
                    </div>

                        <div class="col-sm-12 col-md-12 col-md-12 col-xl-8">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            @foreach ($estados as $estado)

                                                @can($estado->name)
                                                    @switch($estado->estc_tipo)
                                                        @case(0) {{-- Cambiar estado --}}
                                                            <button class="btn btn-{{$estado->estc_color}}"
                                                                onclick="MakeRequestData( '{{ route('modal-estados') }}',
                                                                    '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
                                                                    ['id/{{ $estado->perm_id }}', 'idForm/{{ $estado->form_id }}', 'estado/{{ $estado->esta_id }}', 'type/1'])">
                                                                {{ $estado->esta_nombre }}
                                                            </button>
                                                        @break
                                                        @case(1) {{-- Agregar --}}
                                                        @case(2) {{-- Editar --}}
                                                        @case(7) {{-- Editar --}}
                                                            <button onclick="MakeRequestData( '{{route('modal-formulario')}}', '.modal-content', true,
                                                                '#modal-principal', 'POST', 2, '', false, false,
                                                                ['Id/{{$permiso->perm_id}}', 'Accion/0','Contenedor/tableForm','form/{{$estado->form_new}}', 'tipo/{{$estado->estc_tipo}}'])"
                                                                class="btn btn-{{$estado->estc_color}} d-none d-sm-inline-block" id="nuevo">
                                                                {{$estado->esta_nombre}}
                                                            </button>
                                                        @break
                                                        @case(3) {{-- Asignar tecnicos --}}
                                                            <button onclick="MakeRequestData( '{{ route('modal-permisos') }}', '.modal-content', true,
                                                                '#modal-principal', 'POST', 2, '', false, false,['Id/{{$permiso->perm_id}}', 'estado/{{$estado->esta_id}}'])"
                                                                class="btn btn-{{$estado->estc_color}}" itema-toggle='tooltip'> {{$estado->esta_nombre}}</button>
                                                        @break
                                                        @case(4) {{-- Verificacion de documentos --}}
                                                            <a  type="button" onclick="MakeRequestData( '{{ route('modal-permisos-denegar') }}', '.modal-content', true, '#modal-principal',
                                                                'POST', 2, '', false, false,['Id/{{$permiso->perm_id}}', 'form/{{$estado->form_new}}'])"
                                                                class="btn btn-{{$estado->estc_color}}" itema-toggle='tooltip'>{{$estado->esta_nombre}}</a>
                                                        @break
                                                        @case(5) {{-- impresion de actas --}}
                                                            @if (isset($estado->form_new))
                                                            <button onclick="MakeRequestData( '{{route('modal-formulario-acta')}}', '.modal-content', true,
                                                                '#modal-principal', 'POST', 2, '', false, false,
                                                                ['Id/{{$permiso->perm_id}}', 'Accion/0','Contenedor/tableForm','form/{{$estado->form_new}}', 'tipo/2', 'doc/{{$estado->estc_documento}}', 'nombre/{{$estado->esta_nombre}}'])"
                                                                class="btn btn-{{$estado->estc_color}} d-none d-sm-inline-block" id="nuevo">
                                                                {{$estado->esta_nombre}}
                                                            </button>
                                                            @else
                                                                <a  type="button" target="_blank" href="{{route('permisos.acta', [$permiso->perm_id, $estado->estc_documento])}}" class="btn btn-success">{{$estado->esta_nombre}}</a>
                                                            @endif
                                                        @break
                                                        @case(6) {{-- verificar solvencia --}}
                                                            @if ($Tipo->opci_id != 246)
                                                                <a onclick="MakeRequestData( '{{ route('modal-solvencia') }}', '.modal-content', true,
                                                                    '#modal-principal', 'POST', 2, '', false, false,['Id/{{$permiso->perm_id}}'])" type="button" class="btn btn-{{$estado->estc_color}}">{{$estado->esta_nombre}}</a>
                                                            @endif
                                                        @break
                                                        @default
                                                        @break
                                                    @endswitch
                                                @endcan

                                            @endforeach
                                        </div>
                                        <br />

                                        <label class="d-none" id="DeleteDoc" hidden>¿Estas seguro que quires eliminar este documento?</label>

                                        <hr class="bg-danger border-4 border-top border-primary my-0">
                                        <br />
                                        <div class="col-12">
                                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                                <li class="nav-item px-1" role="presentation">
                                                    <button class="nav-link active" id="pills-Seguimiento-tab"
                                                        data-bs-toggle="pill" data-bs-target="#pills-Seguimiento"
                                                        type="button" role="tab" aria-controls="pills-Seguimiento"
                                                        aria-selected="true">Seguimiento</button>
                                                </li>
                                                    <li class="nav-item" role="presentation">
                                                        <button onclick="" class="nav-link" id="pills-Documentos-tab"
                                                            data-bs-toggle="pill" data-bs-target="#pills-Documentos"
                                                            type="button" role="tab" aria-controls="pills-Documentos"
                                                            aria-selected="false">Documentos adicionales</button>
                                                    </li>
                                                <li class="nav-item" role="presentation">
                                                    <button onclick="" class="nav-link" id="pills-Modificacion-tab"
                                                        data-bs-toggle="pill" data-bs-target="#pills-Modificacion"
                                                        type="button" role="tab" aria-controls="pills-Modificacion"
                                                        aria-selected="false">Formularios</button>
                                                </li>
                                            </ul>
                                        </div>

                                        <div class="tab-content" id="pills-tabContent">
                                            <hr class="bg-danger border-4 border-top border-primary my-0">


                                            {{-- Contenido de tab_seguimiento --}}
                                            <div class="tab-pane fade show active" id="pills-Seguimiento" role="tabpanel"
                                                aria-labelledby="pills-Seguimiento-tab">

                                                <div class="p-3">
                                                    <div class="card shadow">
                                                        <div class="card-header p-2">
                                                            <button class="btn btn-primary mb-2" type="button" data-toggle="collapse" data-target="#collapseAddDocSalida" aria-expanded="false" aria-controls="collapseAddDocSalida"
                                                                onclick="MakeRequestData( '{{ route('modal-estados') }}',
                                                                    '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
                                                                    ['id/{{ $permiso->perm_id }}', 'idForm/{{ $permiso->form_id }}', 'estado/1'
                                                                    , 'type/1', 'Contenedor/tableComments', 'accion/4'])">
                                                                Nuevo comentario &nbsp; <i class="fas fa-plus"></i>
                                                            </button>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="col-12 p-0 m-0">
                                                                @include('main.permisos.detalle-tab.seguimientos.info-table')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                            {{-- Tab_Documentos --}}
                                            <div class="tab-pane fade" id="pills-Documentos" role="tabpanel"
                                                aria-labelledby="pills-Documentos-tab">
                                                <div class="p-3">
                                                    <div class="card shadow">
                                                        <div class="card-header p-2">
                                                            <button class="btn btn-primary mb-2" type="button" data-toggle="collapse" data-target="#collapseAddDocSalida" aria-expanded="false" aria-controls="collapseAddDocSalida"
                                                            onclick="MakeRequestData(
                                                                '{{route('modal-documentos')}}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
                                                                ['id/{{$permiso->perm_id}}', 'Accion/0','Contenedor/lstDocumentos'])">
                                                                Nuevo documento &nbsp; <i class="fas fa-plus"></i>
                                                            </button>
                                                        </div>

                                                        <div class="col-12">
                                                            <div class="col-12 p-0 m-0"  id="tabdocs-entradas">
                                                                @include('main.permisos.detalle-tab.documentos.tabla')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{-- _Tab_Formularios --}}
                                            <div class="tab-pane fade" id="pills-Modificacion" role="tabpanel"
                                                aria-labelledby="pills-Modificacion-tab">

                                                <div class="p-3">
                                                    <div class="card shadow">

                                                        <div class="col-12">
                                                            <div class="col-12 p-0 m-0"  id="tabdocs-entradas">
                                                                @include('main.permisos.detalle-tab.dataFormulario')
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
@endsection
