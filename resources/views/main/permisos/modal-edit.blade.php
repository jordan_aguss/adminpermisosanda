<x-card-modal>
    @slot('titulo')
        Información del formulario
    @endslot
    @slot('body')
        <form id="form-general" class="form-horizontal" method="POST">
            @csrf
            <div>
                <div class="row">
                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Nombre de Formulario</label>
                        <select class="form-control" name="formulario" id="formulario">
                            @foreach($formularios as $formulario)
                                <option value="{{$formulario->form_nombre}}">{{$formulario->form_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                    <input type="hidden" value="{{$Info->form_id}}" name="Id" id="Id"/>
                    <div class="mb-3">
                        <label class="form-label">Secciones</label>
                        <div class="card">
                        @foreach ($secciones as $seccion)
                        <div class="accordion accordion-flush" id="accordionFlushExample">
                            <div class="accordion-item">
                              <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                    {{ $seccion->secc_nombre }}
                                </button>
                              </h2>
                              <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">Placeholder content for this accordion, which is intended to demonstrate the <code>.accordion-flush</code> class. This is the first item's accordion body.</div>
                              </div>
                            </div>
                          </div>
                        @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </form>
    @endslot
    @slot('footer')
        <button type="button" class="btn btn-outline-primary" onclick="MakeRequestData( '{{ route('form-save') }}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', {{$request->Accion}}, '#form-general', false, true)">
            <i class="fas fa-save"></i>&nbsp;&nbsp;&nbsp;Guardar
        </button>
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar
        </button>
    @endslot
</x-card-modal>

