@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Importar permisos')
@section("scripts")

@endsection
@section('content')
<div class="col-10 m-auto my-4" id="DivPrincipal">
    <form class="card" id="FormFile">

        <div class="card-header">
            <h5 class="card-title">Subida de archivo excel con permisos</h5>
        </div>

        <div class="card-body">
            @csrf
            <div class="form-control">
                <label for="" class="small text-black">Subir archivo excel</label>
                <input type="file" name="permisos" id="" class="form-control">
            </div>
        </div>


        <div class="card-footer text-end">
            <button class="btn btn-success" type="button"
            onclick="MakeRequestData('{{route('permisos.load-file')}}', '', true, '', 'POST',
            2, '#FormFile', true)">
                <i class="fas fa-save"></i> &nbsp; Guardar
            </button>
        </div>

    </form>
</div>
@endsection
