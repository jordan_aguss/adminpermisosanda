<html>
    <head>

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
      </head>
    <body>
        <div class="container">
            <div class="card">
            <div class="card-header font-weight-bold">
            <h5>Notificación de inspección técnica | ANDA</h5>
            </div>

            <div class="card-body">
            
            <p>Estimado Sr: {{$nombreUser}}</p>


            <p> Por este medio se le notifica que para {{$tipoSolicitud}} "{{$nameProyecto}}" del cual usted ingreso una solicitud de factibilidad,
                se ha coordinado una inspecciòn para el día {{$fecha}}
            </p>

            </div>

            </div>
        </div>
    </body>
</html>