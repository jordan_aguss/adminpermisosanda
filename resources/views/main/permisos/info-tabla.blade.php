
    <x-table>
        @slot('idTabla')
        bodyTable
        @endslot

        @slot('titulo')
        Visor de permisos
        @endslot

        @slot('btnAdicionales')

        @endslot

        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13) MakeRequestData('{{ route('buscar-permisos')}}' + '/' + $(this).val() , '#bodyTable', true)"
        @endslot


        @slot('header')
            <th id="perm_id" name="0" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" > Id &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="name" name="1" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" >Usuario &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="nombrePermiso" name="2" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" >Nombre permiso &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="fecha" name="3" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" >Fecha creación &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="estado" name="4" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" >Estado &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="proceso" name="5" onclick="MakeRequestData('{{route('ordenar-permisos')}}' + '/' + $(this).attr('name'), '#bodyTable', true)">Proceso &nbsp;  <i class="fas fa-sort"></i></th>
            <th>Acciones</th>
        @endslot

        @slot('body')
            @include('main.permisos.data')
        @endslot
        

    </x-table>
