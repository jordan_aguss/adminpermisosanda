<x-card-modal>
    @slot('titulo')
        Verificación de Solvencias
    @endslot
    @slot('body')

    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">Cargo</th>
            <th scope="col">Nombre</th>
            <th scope="col">Estado</th>
          </tr>
        </thead>
        <tbody>
            @foreach($listaSolventes as $item)
          <tr>
              <td>{{$item->preg_nombre}}</td>
            <td>{{$item->perd_respuesta}}</td>
            <td>Solvente <input type="checkbox" pregunta="{{$item->preg_id}}"  {{isset($item->pers_fecha) ? 'checked disabled' : ''}} name="" id="" onclick="Solvente(this)" ></td>
          </tr>
          @endforeach
        </tbody>
      </table>
    @endslot
    @slot('footer')

    <button class="btn btn-danger" type="button" data-bs-dismiss="modal">
        <i class="fas fa-times"></i></i>&nbsp;Cancelar
    </button>

    @endslot
</x-card-modal>


<script>
    var result = 0;
    function Solvente(item){
        $(item).prop('disabled', true);
        console.log( 'pregunta' + $(item).attr('pregunta'));
        MakeRequestData('{{route("save-solvencia", [$id, $estg_id])}}' + '/' + $(item).attr('pregunta'), '', true);
    }
</script>



