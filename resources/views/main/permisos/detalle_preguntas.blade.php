@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', $Permiso->form_nombre)
@section("scripts")
<script src="{{ asset("js/index.js") }}" type="text/javascript"></script>
@endsection
@section('content')
<div class="col-12 m-0 p-0" id="tabdoc">
    <div class="card shadow mb-3" >
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table card-table table-vcenter text-nowrap" id="data-table">
                    <thead>
                        <tr>
                            <th>PREGUNTA</th>
                            <th>RESPUESTA</th>
                        </tr>
                    </thead>
                    <tbody id="TBodyProyectos">
                        @if (count($Preguntas) == 0)
                            <tr><td colspan="6"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
                        @else
                            @foreach ($Preguntas as $item)
                                <tr>
                                    <td>{{$item->preg_nombre}}</td>
                                    <td>
                                        @if ($item->tipe_id == 9)
                                            <a href="http://anda2022.tec101sw.net/public/descargarAdjuntoPublico/{{$item->RESULTADO}}" target="_blank">
                                                {{$item->RESULTADO}}
                                            </a>
                                        @else
                                            {{$item->RESULTADO}}
                                        @endif
                                        </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
                <br>
            </div>
        </div>
    </div>
</div>


@endsection

<x-modal>
    @yield('title','Aceptar o anular proyecto')
</x-modal>
