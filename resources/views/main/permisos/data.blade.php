
@forelse ($Permisos as $item)
    <tr>
        <td>{{$item->perm_codigo}}</td>
        <td>{{$item->usua_nombre}}</td>
        <td>{{$item->form_nombre}}</td>
        <td>{{$item->perm_fecha}}</td>
        <td>{{$item->esta_nombre}}</td>
        <td>{{$item->estg_nombre}}</td>
        <td>
            <a href="{{route('permisos.detalle', [$item->perm_id, $item->form_id])}}" class="btn" itema-toggle='tooltip' title="Seguimiento del proyecto"> <i class="text-info fas fa-file-invoice"></i> </a>
        </td>
    </tr>
@empty
    <tr><td colspan="7"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
@endforelse

@include('main.home.paginador', ['Datos' => $Permisos, 'cols' => 7])

