
@forelse ($documentos as $documento)
    <tr id="docu-{{$documento->docu_id}}">
        <td>{{$documento->docu_nombre}}</td>
        <td>{{$documento->tipd_nombre}}</td>
        <td>{{$documento->docu_fecha}}</td>
        <td>{{$documento->docu_comentario}}</td>
        <td>{{$documento->tecn_id}}</td>
        <td>{{$documento->esta_nombre}}</td>
        <td>
            <a href="{{route('descargarDocs', [$documento->docu_url, $documento->perm_id])}}" target="_blank" class="btn btn-secondary">
                <i class="text-secundary fas fa-download"></i>
            </a>

            <button class="btn btn-danger"
            onclick="MakeRequestData('{{route('delete-doc')}}', '#docu-{{$documento->docu_id}}', true, '',
             'POST', 3, '', false, false, ['NeedAsk/#DeleteDoc', 'id/{{$documento->docu_id}}'])">
            <i class="fa fa-trash-alt"></i></button>
        </td>
    </tr>
@empty
    <tr><td colspan="6"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
@endforelse
