<x-card-modal>

    @slot('titulo')
        Formulario documentos
    @endslot

    @slot('body')

    <label class="d-none" id="DocsTab">¿Estas seguro que quires agregar un nuevo documento?</label>
        <form id="form_docsSalida" enctype="multipart/form-data" novalidate>
            <div class="row">
                @csrf

                <input type="hidden" class="form-control" name="permiso" id="permiso" value="{{ $request->id }}">

                <div class="col-sm-12 col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Documento adjunto</label>
                        <input type="file" class="form-control" name="adjunto" id="adjunto">
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Tipo de documento</label>

                        <select name="tipo" class="form-control" id="tipo" required>
                            <option value="">--Seleccionar--</option>
                            @foreach ($Tipos as $item)
                                <option value="{{$item->tipd_id}}">{{$item->tipd_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-sm-12 col-md-12">
                    <div class="mb-3">
                        <label class="form-label">Notas del documento</label>
                        <textarea class="form-control" name="comentario" id="comentario" cols="4" rows="3" required></textarea>
                    </div>
                </div>


            </div>
        </form>
    @endslot

    @slot('footer')

        <button class="btn btn-outline-dark mb-2" type="button" data-bs-dismiss="modal" aria-label="Close">
            Cancelar &nbsp; <i class="fas fa-times"></i>
        </button>

        <button class="btn btn-success mb-2" type="button" data-toggle="collapse" data-target="#collapseAddDocSalida" aria-expanded="false" aria-controls="collapseAddDocSalida"
            onclick="MakeRequestData('{{route('guardar-documentos')}}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', 0, '#form_docsSalida', true, true, ['NeedAsk/#DocsTab'])">
            Guardar &nbsp; <i class="fas fa-save"></i>
        </button>

    @endslot


</x-card-modal>
