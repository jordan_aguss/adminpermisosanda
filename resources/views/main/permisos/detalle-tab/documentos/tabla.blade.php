
<div class="table-responsive">
    <table class="table table-vcenter card-table" id="tableSeguimientoProyecto">
        <thead>
            <tr>
                <th>Nombre Documento</th>
                <th>Tipo documento</th>
                <th>Fecha</th>
                <th>Comentario</th>
                <th>Técnico</th>
                <th>Estado</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody id="lstDocumentos">
            @include('main.permisos.detalle-tab.documentos.item-documentos')
        </tbody>
    </table>
</div>
