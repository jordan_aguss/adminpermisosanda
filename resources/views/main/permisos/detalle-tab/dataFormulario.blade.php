<div class="table-responsive">
    <table class="table table-vcenter card-table" id="tableSeguimientoProyecto">
        <thead>
            <tr>
                <th>Permiso</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody id="tableForm">
            @include('main.permisos.detalle-tab.item-form')
        </tbody>
    </table>
</div>
