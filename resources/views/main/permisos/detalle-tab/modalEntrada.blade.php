<div class="col-12">
    <div class="d-flex">
        <h4 class="justify-content-start">Formulario documentos</h4>
        <button type="button" class="btn-close justify-content-end" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <hr class="col-12 mt-1 mb-3">
    <div class="card card-body bg-light">
        <div class="col-12 ">
            <form id="form_docsSalida" action="" method="post" enctype="multipart/form-data" novalidate>
                <div class="row">
                    @csrf

                    <input type="hidden" class="form-control" name="EVAL_ID" id="EVAL_ID" value="">
                    <input type="hidden" class="form-control" name="PROY_ID" id="PROY_ID" value="">
                    <input type="hidden" class="form-control" name="IdDoc" id="IdDoc" value="">

                    <div class="col-sm-12 col-md-12">
                        <div class="mb-3">
                            <label class="form-label"> Tipo de documento </label>
                            <select class="form-select" name="TIPD_ID" id="TIPD_ID" required>
                                {{-- @foreach ($TipoDocs as $item)
                                    @if($item->TIPD_ID == $request->Tipo)
                                        <option selected value="{{$item->TIPD_ID}}">{{$item->TIPD_NOMBRE}}</option>
                                    @else
                                        <option value="{{$item->TIPD_ID}}">{{$item->TIPD_NOMBRE}}</option>
                                    @endif
                                @endforeach --}}
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="mb-3">
                            <label class="form-label"> Notas </label>
                            <textarea class="form-control" name="DOCU_NOTA" id="DOCU_NOTA" cols="4" rows="3" required>Nota</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <div class="mb-3">
                            <label class="form-label"> Documento adjunto </label>
                            <input type="file" class="form-control" name="DOCU_ADJUNTO" id="DOCU_ADJUNTO">
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-auto">
                            <button class="btn btn-success mb-2" type="button" data-toggle="collapse" data-target="#collapseAddDocSalida" aria-expanded="false" aria-controls="collapseAddDocSalida"
                                onclick="">
                                Guardar &nbsp; <i class="fas fa-save"></i>
                            </button>
                        </div>
                        <div class="col-auto">
                            <button class="btn btn-outline-dark mb-2" type="button" data-bs-dismiss="modal" aria-label="Close">
                                Cancelar &nbsp; <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>