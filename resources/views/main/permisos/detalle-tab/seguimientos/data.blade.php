
@forelse ($seguimientos as $historia)
    <tr id="HActividad-{{ $historia->segu_id }}">
        <td> <h3> {{ $historia->perm_codigo }}</h3> </td>
        <td>{{ \Carbon\Carbon::parse($historia->segu_fecha)->format('d/m/Y') }} </td>
        <td>{{ $historia->segu_comentario }} </td>
        <td>{{ $historia->usua_nombre }} </td>
        <td>{{ $historia->esta_nombre }} </td>
    </tr>
@empty
    <tr>
        <td colspan="5">
            <h4
                class="text-muted text-center mt-3">
                No se encontraron
                registros</h4>
        </td>
    </tr>
@endforelse


@include('main.home.paginador', ['cols' => 5, 'Datos' => $seguimientos])
