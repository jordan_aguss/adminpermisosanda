
<div class="table-responsive">
    <table class="table table-vcenter card-table"
        id="tableSeguimientoProyecto">
        <thead>
            <tr>
                <th>Codigo</th>
                <th>Fecha</th>
                <th>Comentario</th>
                <th>Técnico</th>
                <th>Estado</th>
            </tr>
        </thead>
        <tbody id="tableComments" class="contentPager" link="#tableComments">
            @include('main.permisos.detalle-tab.seguimientos.data')
        </tbody>
    </table>
</div>
