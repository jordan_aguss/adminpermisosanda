
            <div class="table-responsive">
                <table class="table table-vcenter card-table" id="tableSeguimientoProyecto">
                    <thead>
                        <tr>
                            <th>Nombre Documento</th>
                            <th>Fecha</th>
                            <th>Comentario</th>
                            <th>Técnico</th>
                            <th>Estado</th>
                            <th>Accion</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if (count($documentos) == 0)
                            <tr><td colspan="6"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
                        @else
                            @foreach($documentos as $documento)
                                <tr id="Documento-">
                                    <td>{{$documento->docu_nombre}}</td>
                                    <td>{{$documento->docu_fecha}}</td>
                                    <td>{{$documento->docu_comentario}}</td>
                                    <td>{{$documento->docu_tecn_id}}</td>
                                    <td>{{$documento->esta_id}}</td>
                                    <td>
                                        <a target="_blank" href="" class="btn btn-outline-secondary"><i class="text-secundary fas fa-download"></i></a>
                                        <button class="btn btn-warning" type="button" onclick=""><i class="fas fa-edit"></i></button>
                                        <button class="btn btn-danger" onclick=""><i class="fa fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
