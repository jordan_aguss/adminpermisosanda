
@if (count($formularios) == 0)
    <tr><td colspan="6"><h4 class="text-muted text-center mt-3">No se encontraron registros</h4></td></tr>
@else
    @foreach($formularios as $formulario)
        <tr id="Formulario">
            <td>{{$formulario->form_nombre}}</td>
            <td><a href="{{route('permisos.detalle_preguntas', [$formulario->form_id, $permiso->perm_id, $formulario->perf_id])}}"  target="_blank" class="btn btn-primary" itema-toggle='tooltip' title="Detalle de Preguntas"> <i class="fas fa-eye"></i></a></td>
        </tr>
    @endforeach
@endif
