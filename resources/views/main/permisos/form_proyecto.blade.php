@extends('layouts.template')


@section('content')
<form class="card" id="FormPermiso">


    <div class="card-body">
        @csrf
        <h5 class="col-auto m-0 mt-3 mb-3"><b>Crear proyecto</b></h5>
        <div class="col-12 m-0 p-0 row mt-3 mb-3">
            <div class="font-weight-bold">- Información general del proyecto</div>
            <hr class="sidebar-divider colorSea col-12 justify-content-start m-0">
        </div>
        <div class="col-12 mb-2">
            <span class="text-black black small"><b>Permiso a solicitar</b></span>
            <select class="form-control" onchange="MakeRequestData('{{route('preguntas-form')}}' + '/' + $(this).val(), '#divPreguntas' )"
                    id="SelectForm" name="SelectForm" required>
                <option value="" disabled selected>Seleccionar</option>
            </select>
            <div class="invalid-feedback small">
                Selecciona el tipo de proyecto.
            </div>
        </div>

        <style>.scrolling-wrapper{
            overflow-x: auto;
        }</style>

        <div class="col-12 p-0 m-0 row" id="divPreguntas">

        </div>

        <div class="col-12 m-0 p-0 row mt-3 mb-3">
            <div class="font-weight-bold">- Sensibilidad al medio</div>
            <hr class="sidebar-divider colorSea col-12 justify-content-start m-0">
        </div>
        <div class="col-12 p-0 m-0" id="divDVigea"></div>

        <button class="btn btn-success" type="button" onclick="saveLastSection();MakeRequestData('{{route('guardar-permiso')}}', '#bodyContent', true, '', 'POST', 2, '#FormPermiso', true)">
            <i class="fas fa-save"></i> Guardar
            </button>
    </div>
</form>



<script src="{{asset('js/AddProyectocopy.js')}}"></script>
<script src="{{asset('js/AddProyecto.js')}}"></script>



<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_KBybVlLgRyMt_P6Ky5_IMtnptXpvCUU&signed_in=true&libraries=drawing&callback=initMap">
</script>

<script>
    @foreach($Forms as $item)
    //Proyectos y Comunidad
        @if( $item->form_id == 2 || $item->form_id == 3)
            $('#SelectForm').append('<option value="{{$item->form_id}}" selected>{{$item->form_nombre}}</option>');
            MakeRequestData('{{route('preguntas-form', [$item->form_id, 0])}}', '#divPreguntas' );
        @else
      //  $('#SelectForm').append('<option value="{{$item->form_id}}" >{{$item->form_nombre}}</option>');
        @endif
    @endforeach

    setUrls("", "{{route('get-preguntas-hijas')}}", "{{route('prem-save-preguntas')}}", "{{route('prem-save-pregunta')}}");


    function saveLastSection(){
        let tab = $("a.nav-item.nav-link.active").data("tab");
        console.log(tab);
        console.log("LLEGO");
        MakeRequestData('{{route("prem-save-preguntas")}}', '', false, '', 'POST', 2, '#FormTab-' + tab);
}

    $('body').on('click', '.btnNext', function() {

$('input').prop('required', false);

let tab = $(this).attr('tabItem');
let next =  (parseInt(tab) + 1);
let item = '.tabSection-' + next;

$('.tabPage-' + tab).removeClass('active show');
$('.tabPage-' + next ).addClass('active show');
$(item).removeClass('disabled');
$('.tabSection-' + tab).removeClass('active');
$(item).addClass('active');
$('.formSecundario').removeAttr("required");

SetDataResult(this, 3, '');
$('#FormTab-' + tab).addClass('enviado');

console.log($(this).attr('tabItem'));
$("html,body").animate({scrollTop: $('.tabSection-' + tab).offset().top}, 1000);
MakeRequestData('{{route('prem-save-preguntas')}}', '', false, '', 'POST', 2, '#FormTab-' + tab);
});

$('body').on('change', '.enviado input', function(){
console.log('Mensaje');
MakeRequestData('{{route('prem-save-pregunta')}}', '', false, '', 'POST', 2, '', false, false,
['seccion/' + $(this).closest('form').attr('seccion'), 'nombre/' + $(this).attr('id').replace('campoPregunta', ''), 'valor/' + $(this).val()]);
});



</script>
@endsection
