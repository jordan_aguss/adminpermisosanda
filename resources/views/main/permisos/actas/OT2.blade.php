<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">
         <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="35%" ></center>  <br> 
           <center><h5 > <p class="font-weight-bold">OPINION TECNICA</p></h5 ></center>
      
          
            <table class="table table-bordered table-sm" style="font-size:90%;">
               <tbody>
                   <tr>
                       <td colspan="2">lugar:{{isset($preguntas[344] )  ? $preguntas[344] : '' }}</td>
                       <td colspan="2">Hora:{{isset($preguntas[367] )  ? $preguntas[367] : '' }}</td>
                 
                   </tr>
                   <tr>
                     
                   
                   </tr>
                   <tr>
                      
                     <td colspan="2">Fecha Inspeccion: {{isset($preguntas[4] )  ? $preguntas[4] : '' }} </td>
                    <td colspan="2">No de Expediente:  {{$permiso->perm_codigo}} </td>
                       </td>
                                   <tr>

      

                   </tr>
                   <tr>
                       <td colspan="4">Nombre del Proyecto: {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</td>
                     
                   </tr>

                   <tr>
                       <td colspan="4">Propietario: {{isset($preguntas[193] )  ? $preguntas[193] : '' }}</td>
                       
                   </tr>

                   <tr>
                       <td colspan="2"> Presenta: {{isset($preguntas[174] )  ? $preguntas[174] : '' }}</td>
                       <td colspan="2">  {{isset($preguntas[484] )  ? $preguntas[484] : '' }}</td>
                   </tr>
  <tr>
                    
                     <td colspan="4">  <b>  Cantidad de Servicios Solicitados</b></td>
                   </tr>
                   <tr>
                   
                       <td colspan="2">Cantidad de Acueductos: {{isset($preguntas[10] )  ? $preguntas[10] : '' }}</td>
                       <td colspan="2">Cantidad de Alcantarillados:  {{isset($preguntas[9] )  ? $preguntas[9] : '' }}  </td>
                    
                   </tr>

                   <tr>
                       <td colspan="4">Tipo:  {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</td>
                 
                   </tr>
                   <tr>
                         <td colspan="2">Factibilidad Acueductos:  {{isset($preguntas[11] )  ? $preguntas[11] : '' }}</td>
                       <td colspan="2">Factibilidad Alcantarillados:  {{isset($preguntas[30] )  ? $preguntas[30] : '' }}</td>
                            </tr>
                   <tr>
                       <td colspan="2">Cantidad de Servicios Factibles:</td>
                       <td colspan="1">Cantidad Acueducto: {{isset($preguntas[10] )  ? $preguntas[10] : '' }}</td>
                       <td colspan="1">Cantidad Alcantarillado: {{isset($preguntas[9] )  ? $preguntas[9] : '' }}</td>
                       </tr>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">PARA ACUEDUCTO</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td colspan="4">El abastecimiento del proyecto puede ser realizado a partir de:{{isset($preguntas[363] )  ? $preguntas[363] : '' }} </td>
                      
                    </tr>
                    <tr>
                        <td colspan="2">No de Horas de Servicio: {{isset($preguntas[364] )  ? $preguntas[364] : '' }} </td>
                       <td colspan="2">Horario de Servicio: {{isset($preguntas[365] )  ? $preguntas[365] : '' }}</td>
                             </tr>
                    <tr>
                        <td colspan="3">Presión en el Punto de Entronque es de : {{isset($preguntas[366] )  ? $preguntas[366] : '' }}</td>
                         <td colspan="1">Hora: {{isset($preguntas[367] )  ? $preguntas[367] : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="4">Distancia de Distribucion mas Cercana: {{isset($preguntas[368] )  ? $preguntas[368] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Presión en Distintos Puntos de Red: {{isset($preguntas[369] )  ? $preguntas[369] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Identificar Áreas Críticas de la Zona: {{isset($preguntas[370] )  ? $preguntas[370] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Caudal de Abastecimiento en Zona: {{isset($preguntas[371] )  ? $preguntas[371] : '' }}</td>
                    </tr>
                       <tr>
                        <td colspan="4">Caudal Medio Solicitado: {{isset($preguntas[372] )  ? $preguntas[372] : '' }}</td>
                    </tr>
                       <td colspan="4">Caudal Disponible para Nuevos Usuarios: {{isset($preguntas[373] )  ? $preguntas[373] : '' }}</td>
                    </tr>
                    <tr>
                       <td colspan="4">Tipos de Servicios Existentes: {{isset($preguntas[374] )  ? $preguntas[374] : '' }}</td>                     
                    </tr> 
                    <tr>
                        <td colspan="4">Estado Actual y Antecedentes del Sistema: {{isset($preguntas[375] )  ? $preguntas[375] : '' }}</td>
                       </tr>
                       <tr>  <td colspan="4">Fuentes de Abastecimiento: {{isset($preguntas[376] )  ? $preguntas[376] : '' }}</td></tr>
                         <tr>  <td colspan="4">OBSERVACIONES: {{isset($preguntas[377] )  ? $preguntas[377] : '' }}</td></tr>
                </tbody>
            </table>

            <hr>
                <center> <strong> <label for="">PARA ALCANTARILLADO SANITARIO</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                    <td colspan="4">La descarga puede realizarce al : {{isset($preguntas[378] )  ? $preguntas[378] : '' }} </td>
                      
                    </tr>
                    <tr>
                        <td colspan="4">Diametro de colector existente: {{isset($preguntas[379] )  ? $preguntas[379] : '' }} </td>
                                            
                    </tr>
                    <tr>
                        <td colspan="4">Tirante Hidraulico en el Punto de Descarga : {{isset($preguntas[380] )  ? $preguntas[380] : '' }}</td>
                       
                    </tr>
                    <tr>
                        <td colspan="4">Tirante Hidraulico  en el punto previo al cambio de diámetro(OT):: {{isset($preguntas[381] )  ? $preguntas[381] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Capacidad de conducción y eficiencia a la que trabaja el colector(%): : {{isset($preguntas[383] )  ? $preguntas[383] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Caudal y Tirante que Aumentara el Colector: {{isset($preguntas[386] )  ? $preguntas[386] : '' }}</td>
                    </tr>
                     <tr>
                        <td colspan="4">Identificar Tramos del Colector Aguas Abajo Trabajando a Máxima Capacidad: {{isset($preguntas[388] )  ? $preguntas[388] : '' }}</td>
                    </tr>
                       <tr>
                        <td colspan="4">Distancia del Punto de Red de Descarga: {{isset($preguntas[392] )  ? $preguntas[392] : '' }}</td>
                    </tr>
                       <td colspan="4">OBSERVACIONES: {{isset($preguntas[420] )  ? $preguntas[420] : '' }}</td>
                    </tr>
                       
            <hr>
            </table>
            
            <center> <strong> <label for=""> Nota:todos los datos mencionados Fueron Proporcionados por:</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td>Acueducto: {{isset($preguntas[211] )  ? $preguntas[211] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Álcantarillado: {{isset($preguntas[239] )  ? $preguntas[239] : '' }}</td>
                    </tr>
                
                </tbody>
            </table>


            <hr>

          
            <center> <strong> <label for=""> Firmas</label></strong></center>
             <table class="table table-bordered table-sm" style="font-size:90%;">
               <tr>
                         <td colspan="2"> ________________________________</td>
                       <td colspan="2">___________________________________</td>
                          
                   </tr>
                <tr>
                         <td colspan="2"> JEFE DEPARTAMENTO DE OPERACIONES</td>
                       <td colspan="2"> COORDINADOR ÁREA DE DISTRIBUCIÓN Y REDES </td>
                          
                   </tr>
                      <tr>
                       <tr>
                         <td colspan="2"> ________________________________</td>
                       <td colspan="2">___________________________________</td>
                          
                   </tr>
                         <td colspan="2">ENCARGADO/A ÁREA DE SANEAMIENTO     </td>
                       <td colspan="2">     GERENTE REGIÓN </td>
                          
                   </tr>
            </table>

          



     </div>
 </html>
 
 