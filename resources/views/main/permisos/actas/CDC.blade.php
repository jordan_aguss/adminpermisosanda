<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="width:600px; margin:0 auto;">
        <div style="text-align: right;">
            <p>San Salvador,<br />
            Referencia: {{ isset($preguntas[445] ) ? $preguntas[426] : '' }}
            </p>
        </div>
        <br><br><br>
        <p><br /><strong>{{isset($preguntas[174] )  ? $preguntas[174] : '' }}<br /></strong></p>
         <p><br /><strong>{{isset($preguntas[461] )  ? $preguntas[461] : '' }}<br /></strong>Presente</p>
        <p>&nbsp;</p>
        <p style="text-align: center;"><strong>CONSTANCIA DE CAMPO</strong></p>
        <p>&nbsp;</p>
        <p style="text-align: justify;">Atendiendo su solicitud referente a obtener <span style="text-decoration: underline;"><strong>Recepción Final</strong></span> para el proyecto: {{isset($preguntas[419] )  ? $preguntas[419] : '' }}, propiedad de:<strong>{{isset($preguntas[193] )  ? $preguntas[193] : '' }}</strong>, ubicado en:<strong>{{isset($preguntas[194] )  ? $preguntas[194] : '' }}</strong>,aprobado mediante Resolución <strong> {{ isset($preguntas[482] ) ? $preguntas[482] : '' }}</strong>.</p>
        <br>
        <p style="text-align: justify;">Al respecto se informa que se llevaron a cabo Pruebas de Hermetecidad en la tuberia del alcantarillado
            sanitario y de presi&oacute;n en las tuberias de acueducto, as&iacute; como se revisaron: pozos, valvulas y
            todos los elementos necesarios para el buen funcionamiento de los sistemas del proyecto, con resultados
            sastifactorios, por lo que se dan por recibidas las instalaciones hidr&aacute;ulicas y se extiende a su
            interes la presente constancia.</p>
        <p>&nbsp;</p>
        <p style="text-align: justify;"><strong>NOTA:</strong>
            {{ isset($preguntas[447] ) ? $preguntas[447] : '' }}</p>
        <p>&nbsp;</p>
        <p>Atentamente,</p>
        

         <hr>

          

            <table>
                <tbody>
                    <tr>
                        
            <td colspan="2"> <br>
            <center> <strong> <label for="">___________________________________</label></strong></center>

                            <br>
                            <center> <strong> <label for="">TECNICO RESPONSABLE </label></strong></center>
                        
                        </td>
                         <td colspan="2"> <br>
            <center> <strong> <label for="">______________________________________</label></strong></center>

                            <br>
                            <center> <strong> <label for="">ENCARGADO DE FACTIBILIDADES REGIONAL </label></strong></center>
                        
                        </td>

                    </tr>
                </tbody>
            </table>
        </div>

    </div>
</body>

</html>