<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
      <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
    <div style="width:800px; margin:0 auto;">
        <p><strong><center>CONSTANCIA DE HABILITACION</center></strong>
        </p>
        <p><strong></strong>Referencia {{$perm_codigo}}</p>
        <p style="text-align: justify;">A solicitud de {{isset($preguntas[174] )  ? $preguntas[174] : '' }} {{isset($preguntas[461] )  ? $preguntas[461] : '' }}, ANDA, extiende la presente Constancia de habilitación por medio de la cual se dan por recibidas las instalaciones de: {{isset($preguntas[483] )  ? $preguntas[483] : '' }} para el proyecto: {{isset($preguntas[419] )  ? $preguntas[419] : '' }}, propiedad de:<strong>{{isset($preguntas[193] )  ? $preguntas[193] : '' }}</strong>, ubicado en:<strong>{{isset($preguntas[194] )  ? $preguntas[194] : '' }}</strong>.
      
        <p style="text-align: justify;">Tal como le informa el Area de factibilidades de esta Dirección en Nota con Referencia {{isset($preguntas[438] )  ? $preguntas[438] : '' }} de fecha  {{isset($preguntas[484] )  ? $preguntas[484] : '' }}  las instalaciones de {{isset($preguntas[483] )  ? $preguntas[483] : '' }} fueron construidas conforme a las especificaciones técnicas requeridas por esta institución establecidas en cumplimiento a la Resolución {{isset($preguntas[439] )  ? $preguntas[439] : '' }}, Ref de fecha {{isset($preguntas[485] )  ? $preguntas[485] : '' }}</p>
        <p style="text-align: justify;">Los rubros urbanisticos correspondientes a dicho proyecto ya fueron efectuados por el urbanizador según comprobante de crédito fiscal No.  {isset($preguntas[441] )  ? $preguntas[441] : '' }} (Aporte proporcional costes de Obra y mejoramiento o ampliación de sistema por bombeo) de fecha {{isset($preguntas[442] )  ? $preguntas[442] : '' }} , facturas No. {{isset($preguntas[443] )  ? $preguntas[443] : '' }}(Prueba  hidráulica Limpieza y otros).</p>
        <p style="text-align: justify;">Se puede proceder a la ejecución del entronque por parte de la Gerencia Regional y a la legalización de los servicios descritos anteriormente por parte de la Gerencia Comercial de esta institución.</p>
        <p>&nbsp;</p>
        <p>San Salvador,</p>
        <p>&nbsp; &nbsp;</p>
        <p>F._______________________</p>
        <p>&nbsp;</p>
        <p></p>
        <p></p>
        <p><center><strong> </strong><br />Director Tecnico</center></p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
    </div>
</body>

</html>