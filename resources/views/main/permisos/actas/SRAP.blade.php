


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">
         <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
         <center><h5 > <p class="font-weight-bold"> SOLICITUD DE REVISIÓN Y APROBACIÓN DE PLANOS </p></h5 ></center>
           
      

            <table  class="table table-bordered table-sm" style="font-size:90%;">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso:
                        </th>
                        <th>
                            Expendiente N.°:
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <p> {{$fecha_creacion}} </p></td>
                        <td> <p> {{$perm_codigo}} </p></td>
                    </tr>
                </tbody>
            </table>


<center> <strong> <label for="">DATOS DEL SOLICITANTE</label></strong></center>
 
 <table class="table table-bordered table-sm" style="font-size:90%;">
    <tbody>
        <tr>
            <td colspan="2">Nombre:{{isset($preguntas[174] )  ? $preguntas[174] : '' }}</td>
            <td colspan="1" >Apellidos:{{isset($preguntas[461] )  ? $preguntas[461] : '' }}</td>
             <td colspan="1"  >Tipo de Solicitante:  {{isset($preguntas[175] )  ? $preguntas[175] : '' }}</td>
        </tr>
                        
        <tr>
           
            <td colspan="2" >Tipo de Documento: {{isset($preguntas[178] )  ? $preguntas[178] : '' }}</td>
            <td colspan="2">Número de Documento: {{isset($preguntas[457] )  ? $preguntas[457] : '' }}</td>
        </tr>
       
        <tr>
            <td colspan="1">Nacionalidad: {{isset($preguntas[179] )  ? $preguntas[179] : '' }}</td>
            <td colspan="2">Domicilio:  {{isset($preguntas[180] )  ? $preguntas[180] : '' }}</td>
            <td colspan="1">Departamento:  {{isset($preguntas[181] )  ? $preguntas[181] : '' }}</td>
        </tr>

        <tr>
            <td colspan="2">Correo Eléctronico: {{isset($preguntas[182] )  ? $preguntas[182] : '' }}</td>
            <td colspan="2">Télefono:  {{isset($preguntas[183] )  ? $preguntas[183] : '' }}</td>
        </tr>

        <tr>
            <td colspan="4"> Denominación o Razón Social (Si Aplica): {{isset($preguntas[184] )  ? $preguntas[185] : '' }}</td>
        </tr>

        <tr>
            <td>Inscrito en el Registro de Comercio: {{isset($preguntas[185] )  ? $preguntas[185] : '' }}</td>
            <td>Libro:  {{isset($preguntas[186] )  ? $preguntas[186] : '' }}</td>
            <td>Fecha: {{isset($preguntas[187] )  ? $preguntas[187] : '' }}</td>
            <td>NIT:  {{isset($preguntas[188] )  ? $preguntas[188] : '' }}</td>
        </tr>

        <tr>
            <td colspan="2">Nombre del Proyecto:  {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</td>
            <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[189] )  ? $preguntas[189] : '' }}</td>
        </tr>
        <tr>
            <td colspan="2">Propietario del Proyecto:  {{isset($preguntas[193] )  ? $preguntas[193] : '' }}</td>
            <td colspan="2">Propietario del Terreno:  {{isset($preguntas[421] )  ? $preguntas[421] : '' }}</td>
            
            
        </tr>
        <tr>
            <td colspan="2">Ubicación: {{isset($preguntas[194] )  ? $preguntas[194] : '' }} </td>
            <td colspan="1">Municipio: {{isset($preguntas[195] )  ? $preguntas[195] : '' }}</td>
            <td colspan="1">Departamento: {{isset($preguntas[196] )  ? $preguntas[196] : '' }}</td>
            </tr>
        </tr>
    </tbody>
 </table>

 <hr>


            <center> <strong> <label for="">DATOS COMPLEMENTARIOS (Sí aplica)</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td colspan="4">Factibilidad Vigente: {{isset($preguntas[268] )  ? $preguntas[268] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Nº :{{isset($preguntas[269] )  ? $preguntas[269] : '' }}</td>
                        <td>Referencia Urb.: {{isset($preguntas[271] )  ? $preguntas[271] : '' }}</td>
                        <td>Referencia Com: {{isset($preguntas[272] )  ? $preguntas[272] : '' }}</td>
                        <td>Fecha: {{isset($preguntas[270] )  ? $preguntas[270] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> DECLARACIÓN JURADA</label></strong></center>
            <p align="justify">
                El suscrito en calidad de titular, propietario o representante legal de la sociedad dueña del proyecto doy fe de la veracidad de la información detallada en el presente documento y anexos,
                cumpliendo con los requisitos de ley exigidos, razón por la cual asumo la responsabilidad consencuente derivada de esta declaración que tiene calidad de declaración jurada.
            </p>


            <hr>

            <center> <strong> <label for="">FIRMA DEL SOLICITANTE</label></strong></center>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                        <center> <strong> <label for="">__________________________________________________________________ </label></strong></center>
                        <center> <strong> <label for="">  Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsable)</label></strong></center>
                            <br>
                          
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 