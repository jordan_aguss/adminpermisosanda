<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">
           <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="35%" ></center>  <br> 
           <center><h5 > <p class="font-weight-bold">  SOLICITUD DE FACTIBILIDAD </p></h5 ></center>
          
      

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <thead>
                    <tr>
                        <th >
                         Fecha de Solicitud:
                        </th>
                        <th>
                          Expendiente N.°:
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <p> {{$fecha_creacion}} </p></td>
                        <td> <p>  {{$perm_codigo}}</p></td>
                    </tr>
                </tbody>
            </table>
    
     
           <center> <strong> <label for="">DATOS DEL SOLICITANTE</label></strong></center>
 
            <table class="table table-bordered table-sm" style="font-size:90%;">
               <tbody>
                   <tr>
                       <td colspan="2">Nombre:{{isset($preguntas[174] )  ? $preguntas[174] : '' }}</td>
                       <td colspan="1" >Apellidos:{{isset($preguntas[461] )  ? $preguntas[461] : '' }}</td>
                        <td colspan="1"  >Tipo de Solicitante:  {{isset($preguntas[175] )  ? $preguntas[175] : '' }}</td>
                   </tr>
                                   
                   <tr>
                      
                       <td colspan="2" >Tipo de Documento: {{isset($preguntas[178] )  ? $preguntas[178] : '' }}</td>
                       <td colspan="2">Número de Documento: {{isset($preguntas[457] )  ? $preguntas[457] : '' }}</td>
                   </tr>
                  
                   <tr>
                       <td colspan="1">Nacionalidad: {{isset($preguntas[179] )  ? $preguntas[179] : '' }}</td>
                       <td colspan="2">Domicilio:  {{isset($preguntas[180] )  ? $preguntas[180] : '' }}</td>
                       <td colspan="1">Departamento:  {{isset($preguntas[181] )  ? $preguntas[181] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Correo Eléctronico: {{isset($preguntas[182] )  ? $preguntas[182] : '' }}</td>
                       <td colspan="2">Télefono:  {{isset($preguntas[183] )  ? $preguntas[183] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="4"> Denominación o Razón Social (Si Aplica): {{isset($preguntas[184] )  ? $preguntas[185] : '' }}</td>
                   </tr>

                   <tr>
                       <td>Inscrito en el Registro de Comercio: {{isset($preguntas[185] )  ? $preguntas[185] : '' }}</td>
                       <td>Libro:  {{isset($preguntas[186] )  ? $preguntas[186] : '' }}</td>
                       <td>Fecha: {{isset($preguntas[187] )  ? $preguntas[187] : '' }}</td>
                       <td>NIT:  {{isset($preguntas[188] )  ? $preguntas[188] : '' }}</td>
                   </tr>

                   <tr>
                       <td colspan="2">Nombre del Proyecto:  {{isset($preguntas[419] )  ? $preguntas[419] : '' }}</td>
                       <td colspan="2">Tipo de Proyecto:  {{isset($preguntas[189] )  ? $preguntas[189] : '' }}</td>
                   </tr>
                   <tr>
                       <td colspan="2">Propietario del Proyecto:  {{isset($preguntas[193] )  ? $preguntas[193] : '' }}</td>
                       <td colspan="2">Propietario del Terreno:  {{isset($preguntas[193] )  ? $preguntas[421] : '' }}</td>
                       
                       
                   </tr>
                   <tr>
                       <td colspan="2">Ubicación: {{isset($preguntas[194] )  ? $preguntas[194] : '' }} </td>
                       <td colspan="1">Municipio: {{isset($preguntas[195] )  ? $preguntas[195] : '' }}</td>
                       <td colspan="1">Departamento: {{isset($preguntas[196] )  ? $preguntas[196] : '' }}</td>
                       </tr>
                   </tr>
               </tbody>
            </table>

            <hr>


            <center> <strong> <label for="">DATOS PARA SOLICITAR FACTIBILIDAD</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td>Factibilidad que solicita:</td>
                        <td>Cantidad Acueductos:  {{isset($preguntas[198] )  ? $preguntas[198] : '' }}</td>
                        <td>Cantidad Alcantarillado:  {{isset($preguntas[199] )  ? $preguntas[199] : '' }}  </td>
                    </tr>
                    <tr>
                        <th colspan="3">Datos Característicos del Proyecto</th>
                    </tr>
                    <tr>
                        <td colspan="3">Tipo de Proyecto a Desarrollar: {{isset($preguntas[200] )  ? $preguntas[200] : '' }}</td>
                    </tr>
                    <tr>
                        <td colspan="3">Otro Tipo de Proyecto: {{isset($preguntas[201] )  ? $preguntas[201] : '' }}</td>
                    </tr>
                   
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for=""> FACTIBILIDAD DE PROYECTOS</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td>Área Total En M2: {{isset($preguntas[206] )  ? $preguntas[206] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área útil M2: {{isset($preguntas[207] )  ? $preguntas[207] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de lotes: {{isset($preguntas[209] )  ? $preguntas[209] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de apartamentos: {{isset($preguntas[210] )  ? $preguntas[210] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de Viviendas: {{isset($preguntas[209] )  ? $preguntas[209] : '' }}</td>
                    </tr>
                    <tr>
                      <td>Número de locales: {{isset($preguntas[212] )  ? $preguntas[212] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Lotes en M2: {{isset($preguntas[215] )  ? $preguntas[215] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Apartamentos En M2: {{isset($preguntas[216] )  ? $preguntas[216] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Viviendas En M2: {{isset($preguntas[218] )  ? $preguntas[218] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Locales En M2:{{isset($preguntas[217] )  ? $preguntas[217] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción Total En M2:{{isset($preguntas[220] )  ? $preguntas[220] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en RestaurantesEn M2: {{isset($preguntas[221] )  ? $preguntas[221] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en Centro Comerciales M2:  {{isset($preguntas[222] )  ? $preguntas[222] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en Mercados M2:  {{isset($preguntas[223] )  ? $preguntas[223] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Construcción en Edificios para Oficina: {{isset($preguntas[224] )  ? $preguntas[224] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Alumnos en Escuela: {{isset($preguntas[225] )  ? $preguntas[225] : '' }}</td>
                    </tr>
                  
                    <tr>
                        <td>Número de Turnos: {{isset($preguntas[227] )  ? $preguntas[227] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Bombas en Gasolinera: {{isset($preguntas[228] )  ? $preguntas[228] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Camas en Hospitales: {{isset($preguntas[229] )  ? $preguntas[229] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de niveles en Edificios: {{isset($preguntas[230] )  ? $preguntas[230] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Sótanos para Estacionamiento: {{isset($preguntas[231] )  ? $preguntas[231] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Cantidad de Edificios: {{isset($preguntas[232] )  ? $preguntas[232] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Médicas: {{isset($preguntas[233] )  ? $preguntas[233] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Clínicas Dentales:{{isset($preguntas[234] )  ? $preguntas[234] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Número de Habitaciones en Hoteles/Pensión:{{isset($preguntas[235] )  ? $preguntas[235] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Estacionamiento: {{isset($preguntas[236] )  ? $preguntas[236] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área de Jardines:  {{isset($preguntas[237] )  ? $preguntas[237] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Capacidad Cines, Teatros (número de personas):{{isset($preguntas[238] )  ? $preguntas[238] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Monto o Valor de Inversion del Proyecto:{{isset($preguntas[466] )  ? $preguntas[466] : '' }}
                    </tr>
                    <tr>
                        <td>OBSERVACIONES:{{isset($preguntas[464] )  ? $preguntas[464] : '' }}
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> FACTIBILIDAD COMUNIDADES</label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td>Número de Viviendas: {{isset($preguntas[211] )  ? $preguntas[211] : '' }}</td>
                    </tr>
                    <tr>
                        <td>Área Promedio de Vivienda: {{isset($preguntas[239] )  ? $preguntas[239] : '' }}</td>
                    </tr>
                    <tr>
                        <td>OBSERVACIONES: {{isset($preguntas[465] )  ? $preguntas[465] : '' }}</td>
                    </tr>
                </tbody>
            </table>


            <hr>

            <center> <strong> <label for=""> En caso cuente con factibilidad vencida, por favor indicar.</label></strong></center>


            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td>Nº de Factibilidad {{isset($preguntas[243] )  ? $preguntas[243] : '' }}</td>
                        <td>Referencia  {{isset($preguntas[244] )  ? $preguntas[244] : '' }}</td>
                        <td>Fecha de emisión {{isset($preguntas[245] )  ? $preguntas[245] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <hr>
            <center> <strong> <label for=""> DECLARACIÓN JURADA</label></strong></center>
            <p align="justify">
                El suscrito en calidad de titular, propietario o representante legal de la socieda dueña del 
                proyecto doy fe de la veracidad de la información detallada en el presente documento y anexos,
                cumpliendo con los requisitos de ley exigidos, razón por la cual asumo la responsabilidad consencuente 
                derivada de esta declaración que tiene calidad de declaración jurada.
            </p>

            <hr>

            <center> <strong> <label for=""> FIRMA DEL SOLICITANTE</label></strong></center>

            <p>
                    <br>
            ___________________________________________________________<br>
            Nombre y Firma del solicitante (Propietario, Representante legal o Apoderado.)
            </p>



     </div>
 </html>
 
 