<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div style="width:700px; margin:0 auto;">
        <p style="text-align: right;">San Salvador, &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />Resolución
            N&deg;: {{$permiso->perm_codigo }}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp;&nbsp;<br />Referencia Com: {{isset($preguntas[449] )  ? $preguntas[449] : '' }}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp;&nbsp;</p>
        <p style="text-align: left;">Señor(es)<br /><br />Municipio de<br />Departamento de<br />Presente.</p>
        <p style="text-align: left;">Se ha tenido a la vista su solicitud, relativa a que se le revisen y aprueban a los
            planos correspondientes a la  {{isset($preguntas[419] )  ? $preguntas[419] : '' }} ubicada en {{isset($preguntas[344] )  ? $preguntas[344] : '' }} en lo concerniente al servicio de
            acueducto de conformidad con el planteamiento siguiente:</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">1.<span style="text-decoration: underline;">Datos caracter&iacute;sticos:</span>
        </p>
        <p style="text-align: left;">&nbsp; &nbsp;Número de viviendas<br />&nbsp; &nbsp;Habitantes por
            vivienda {{isset($preguntas[211] )  ? $preguntas[211] : '' }}<br />&nbsp; &nbsp;Número de habitantes {{isset($preguntas[58] )  ? $preguntas[58] : '' }}<br />&nbsp; &nbsp;Dotación&nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; l/p/d<br />&nbsp; &nbsp;Demanda media
            diara&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;l/s<br />&nbsp; &nbsp;Demanda maximia diaria&nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;l/s
            (k1=1.3)<br />&nbsp; &nbsp;Demanda maximia horaria&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;l/s (k2=2.4)<br /><br /></p>
        <p style="text-align: left;">2. Esta dirección, previo al analisis del estudio preparado por el
            &Aacute;rea de factibilidades, teniendo como base el certificado de factibilidad N&deg;&nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; , Referencia Com.&nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; de fecha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; , resuelve: Aprobar los planos de agua potable y la
            memoria técnica&nbsp; de&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; , en la forma que se indica a
            continuación:</p>
        <p style="text-align: left;">3.&nbsp;<span style="text-decoration: underline;">Acueducto {{isset($preguntas[434] )  ? $preguntas[434] : '' }}</span>:&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;">4.&nbsp;<span style="text-decoration: underline;">Alcantarillado: {{isset($preguntas[437] )  ? $preguntas[437] : '' }}</span></p>
        <p style="text-align: left;">&nbsp;</p>
        <p style="text-align: left;"><br /><br />El proyecto en mención comprende&nbsp; la instalación
            de: {{isset($preguntas[435] )  ? $preguntas[435] : '' }}<br /><br /><br /></p>
        <p style="text-align: left;">&nbsp; &nbsp;</p>
    </div>
</body>

</html>
