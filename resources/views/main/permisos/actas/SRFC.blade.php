


<!DOCTYPE html>
<html lang="en">


<html>
    <head>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
    </head>
    
    
         <div class="card-body">
         <center><img src="{{ storage_path() . '/app/public/images/logo_color.png' }}" width="45%" ></center>  <br> 
         <center><h5 > <p class="font-weight-bold"> SOLICITUD DE RECEPCIÓN FINAL DE CAMPO </p></h5 ></center>
          
      

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <thead>
                    <tr>
                        <th>
                            Fecha de Ingreso:
                        </th>
                        <th>
                            Expendiente N.°:
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> <p> {{$fecha_creacion}} </p></td>
                        <td> <p>  {{$perm_codigo}}</p></td>
                    </tr>
                </tbody>
            </table>
    
     
            <center> <strong> <label for="">DATOS DEL SOLICITANTE</label></strong></center>
 
 <table class="table table-bordered table-sm" style="font-size:90%;">
    <tbody>
        <tr>
            <td colspan="2"> Nombre:{{isset($preguntas[322] )  ? $preguntas[322] : '' }}</td>
            <td colspan="2"> Apellidos:  {{isset($preguntas[323] )  ? $preguntas[323] : '' }}</td>
        
        </tr>
        <tr>
            <td colspan="4"> Tipo de Personeria juridica:  {{isset($preguntas[324] )  ? $preguntas[324] : '' }}  </td>
        </tr>
        <tr>
           <td colspan="1"> Tipo de Documento:{{isset($preguntas[326] )  ? $preguntas[326] : '' }}</td>
           <td colspan="2"> Numero de Documento:  {{isset($preguntas[325] )  ? $preguntas[325] : '' }}</td>
        
        </tr>
        <tr>
            <td colspan="2"> Nacionalidad:  {{isset($preguntas[328] )  ? $preguntas[328] : '' }}</td>
            <td colspan="1"> Domicilio: {{isset($preguntas[329] )  ? $preguntas[329] : '' }}</td>
            <td colspan="1"> Departamento: {{isset($preguntas[330] )  ? $preguntas[330] : '' }}</td>
        </tr>

        <tr>
            <td colspan="2"> Correo Eléctronico: {{isset($preguntas[331] )  ? $preguntas[331] : '' }}</td>
            <td colspan="2"> Télefono: {{isset($preguntas[332] )  ? $preguntas[332] : '' }}</td>
        </tr>

        <tr>
            <td colspan="4"> Razón Social (Si Aplica): {{isset($preguntas[333] )  ? $preguntas[333] : '' }}</td>
        </tr>

        <tr>
            <td colspan="1"> Inscrito al N° CNR: {{isset($preguntas[334] )  ? $preguntas[334] : '' }}</td>
            <td colspan="1"> Libro:{{isset($preguntas[335] )  ? $preguntas[335] : '' }}</td>
            <td colspan="1"> Fecha: {{isset($preguntas[336] )  ? $preguntas[336] : '' }}</td>
            <td colspan="1"> NIT: {{isset($preguntas[337] )  ? $preguntas[337] : '' }} </td>
        </tr>

        <tr>
            <td colspan="2"> Nombre del Proyecto:{{isset($preguntas[338] )  ? $preguntas[338] : '' }} </td>
            <td colspan="2"> Tipo de Proyecto:  {{isset($preguntas[339] )  ? $preguntas[339] : '' }}</td>
        </tr>
        <tr>
            <td colspan="1"> Propietario:{{isset($preguntas[340] )  ? $preguntas[340] : '' }}</td>
            <td colspan="1"> Ubicación: {{isset($preguntas[341] )  ? $preguntas[341] : '' }} </td>
            <td colspan="1"> Municipio: {{isset($preguntas[342] )  ? $preguntas[342] : '' }}</td>
            <td colspan="1"> Departamento: </td>
        </tr>
    </tbody>
 </table>

 <hr>


            <center> <strong> <label for="">DATOS RECEPCIÓN FINAL DE CAMPO </label></strong></center>

            <table class="table table-bordered table-sm" style="font-size:90%;">
                <tbody>
                    <tr>
                        <td colspan="4">Fecha Aprobación ANDA: {{isset($preguntas[343] )  ? $preguntas[343] : '' }}</td>
                    </tr>
                    <tr>
                        <td> Nº {{isset($preguntas[345] )  ? $preguntas[345] : '' }}</td>
                        <td> Ref. Urb.  {{isset($preguntas[348] )  ? $preguntas[348] : '' }}</td>
                        <td> Ref. Com.  {{isset($preguntas[350] )  ? $preguntas[350] : '' }}</td>
                        <td> Fecha: {{isset($preguntas[351] )  ? $preguntas[351] : '' }}</td>
                    </tr>
                    <tr> 
                        <td colspan="2"> Sistema que solicita: {{isset($preguntas[354] )  ? $preguntas[354] : '' }}</td>
                        <td> Cantidad Alcantarillado: {{isset($preguntas[356] )  ? $preguntas[356] : '' }}</td>
                        <td> Cantidad Acueducto: {{isset($preguntas[357] )  ? $preguntas[357] : '' }}</td>
                    </tr>
                </tbody>
            </table>

            <table>
                <tbody>
                    <tr>
                        <td> <br>
                        
                        <center> <strong> <label for=""> __________________________________________________________________</label></strong></center>
                        <center> <strong> <label for=""> Nombre y firma del solicitante (Propietario, Representante Legal o Apoderado, Profesional responsable)</label></strong></center>
                        <br>                            
                        </td>
                    </tr>
                </tbody>
            </table>
         
     </div>
 </html>
 
 