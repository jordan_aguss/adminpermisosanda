
<x-card-modal>
    @slot('titulo')
        Asignaciones de Tecnicos
    @endslot

    @slot('body')

    <div class="row">
        <div class="col-lg-12">
            <div class="mb-3">
                <label class="form-label">Listado de Técnicos</label>
                <table class="table table-hover">
                    <thead>
                        <tr>
                          <th scope="col">ID </th>
                          <th scope="col">Técnico Asignado</th>
                          <th scope="col">Fecha Asignada</th>
                          <th scope="col">Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="tableTecnicos">
                          @include('main.permisos.tecnicos.info-tecnicos')
                      </tbody>
                </table>
            </div>
        </div>

    <form id="formTecnicos" method="POST" novalidate>
        @csrf
        <div>
            <input type="hidden" class="form-control d-none" name="id" id="" value="{{$request->Id}}">
            <input type="hidden" class="form-control d-none" name="estado" id="" value="{{$request->estado}}">
            <hr>

            <div class="col-lg-12">
                <div class="mb-3">
                    <label class="form-label">Tecnicos</label>
                    <select class="form-control" name="tecnico" id="tecnico">
                        <option value="" disabled selected>Seleccionar</option>
                            @foreach ($data as $item)
                            <option value="{{$item->usua_id}}">{{$item->usua_nombre}}</option>
                            @endforeach
                    </select>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="mb-3">
                    <label class="form-label">Fecha de visita</label>
                    <input type="date" name="fecha" min="{{date('Y-m-d')}}" class="form-control" required>
                </div>
            </div>
        </div>

    </form>
    @endslot

    @slot('footer')
        <button onclick=" MakeRequestData('{{route('guardarAsignacion')}}', '#tableTecnicos',
        true, '', 'POST', 0, '#formTecnicos', true)" type="button" class="btn btn-info">Asignar</button>
    @endslot

</x-card-modal>



