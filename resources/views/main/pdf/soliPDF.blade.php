<!DOCTYPE html>
<html>

<head>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
</head>
<script>function printHTML() {
  if (window.print) {
    window.print();
  }
}</script>


<body>

@php

$inicio= '@foreach($permisoDetalle as $item) @if($item->preg_id ==';
$fin = ') {{$item->perd_respuesta}} @break @endif @endforeach';
    
@endphp




<div class="container"> 
  <div class="card">
    <div class="card-header font-weight-bold"> <br>
      <h4> <p class="text-right">  @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta.' |'}} @break @endif @endforeach  @foreach($permisoDetalle as $item) @if($item->preg_id ==37) {{$item->perd_respuesta}} @break @endif @endforeach </p> </h4><br>

      <label for="fecha"> @foreach($permisoDetalle as $item) @if($item->preg_id ==36) {{$item->perd_respuesta}} @break @endif @endforeach</label> <br>

      <p class="font-weight-bold">Señores, <br>
      UNIDAD DE FACTIBILIDADES <br>
      ANDA
      </p>
      <a onclick="printHTML()" class="btn"><img src="{{asset('images/printer.png')}}" style="height: 25px;" /></a>
    </div>

    <div class="card-body">

      Estimados Sres.: <br>

      Atentamente solicito a usted el Certificado de Factibilidad de servicios de agua potable y aguas negras para un terreno  propiedad de
    <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==42) {{$item->perd_respuesta}} @break @endif @endforeach </strong>, ubicado
    en  <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==45) {{$item->perd_respuesta}}@break @endif @endforeach</strong>,  <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==37) {{$item->perd_respuesta}} @break @endif @endforeach .</strong>

   <br><br> El Proyecto Definitivo de denominará <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta}}@break @endif @endforeach.</strong>

   <br><br>

   <h5>DATOS CARACTERISTICOS</h5>

   <table class="table table-hover">
   
 
      <tbody>
        <tr>
          <th scope="row">Area Total</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==51) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area Util</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==50) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de Lotes, Aptos, Locales, etc.</th>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Area Promedio de lotes, aptos, locales, etc</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==57) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area de Construccion Total</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==53) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area de Construccion en restaurantes</th>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Area de Construccion en centros comerciales</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==56) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area de Construccion en mercados</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==54) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Area de Construccion en edificios para oficina</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==55) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de alumnos en la escuela</th>
          <td></td>
        </tr>
        <tr>
          <th scope="row">Numero de trabajadores en industria</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==60) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de turnos</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==61) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de bombas de gasolinera</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==64) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de camas/clinicas en hospitales</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==63) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Numero de niveles</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==62) {{$item->perd_respuesta}}@break @endif @endforeach</td>
        </tr>
        <tr>
          <th scope="row">Caudal Requerido para el Proyecto</th>
          <td>@foreach($permisoDetalle as $item) @if($item->preg_id ==67) {{$item->perd_respuesta}} 1/S @break @endif @endforeach</td>
        </tr>
      </tbody>
  </table>

  <p>
  Favor Notificar a: @foreach($permisoDetalle as $item) @if($item->preg_id ==68) {{$item->perd_respuesta}}@break @endif @endforeach <br>
  Telefono/Celular: @foreach($permisoDetalle as $item) @if($item->preg_id ==70) {{$item->perd_respuesta}}@break @endif @endforeach - @foreach($permisoDetalle as $item) @if($item->preg_id ==69) {{$item->perd_respuesta}}@break @endif @endforeach <br>
  Correo Electronico: @foreach($permisoDetalle as $item) @if($item->preg_id ==71) {{$item->perd_respuesta}}@break @endif @endforeach <br>

  Atentamente, <br><br><br>

  __________________________________________ <br>
  <strong> @foreach($permisoDetalle as $item) @if($item->preg_id ==68) {{$item->perd_respuesta}}@break @endif @endforeach </strong> <br>
  @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta}}@break @endif @endforeach
</p>
      

    </div>
    
  </div>
</div>




















<!--
<center><h3>Proyecto -  @foreach($permisoDetalle as $item) @if($item->preg_id ==41) {{$item->perd_respuesta}} @break @endif @endforeach</h3></center>
    <center><h2>Nombre del Solicitante: {{Auth::user()->name}} </h2></center>
    <table class="table table-hover">
        <thead>
          <tr>
            <th scope="col">ID Permiso</th>
            <th scope="col">Pregunta</th>
            <th scope="col">Respuesta</th> 
          </tr>
        </thead>
        @foreach ($permisoDetalle as $item)
        <tbody>
          <tr>
            <th>{{$item->perm_id}}</th>
          @foreach ($preguntaString as $key)
            @if($key->preg_id == $item->preg_id)
             <td>{{$key->preg_nombre}}</td>
            @endif
          @endforeach

            <td>{{$item->perd_respuesta}}</td>
          </tr>
        </tbody>
        @endforeach
      </table> -->

</body>

</html>