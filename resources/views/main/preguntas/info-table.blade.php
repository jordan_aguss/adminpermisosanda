
        <x-table>
            @slot('idTabla')
            bodyTable
            @endslot

            @slot('idCardTable')
            tablePreguntas
            @endslot

            @slot('titulo')
            Listado de preguntas
            @endslot


            @slot('accionBuscar')
                onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-preguntas')}}' + '/' + $(this).val() , '#bodyTable', true)"
            @endslot

            @slot('btnAdicionales')
                <button onclick="MakeRequestData( '{{ route('pregunta-form') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/0', 'Accion/@if(count($Preguntas) == 0) 2 @else 4 @endif','Contenedor/bodyTable'])" class="btn btn-primary d-none d-sm-inline-block" id="nuevo">
                    <i class="fas fa-plus"></i>
                    &nbsp;&nbsp;Nuevo
                </button>
            @endslot


            @slot('header')
                <th id="preg_id" name="0" onclick="MakeRequestData('{{route('ordenar-preguntas')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" >ID &nbsp;  <i class="fas fa-sort"></i></th>
                <th id="preg_nombre" name="1" onclick="MakeRequestData('{{route('ordenar-preguntas')}}' + '/' + $(this).attr('name'), '#bodyTable', true)">NOMBRE &nbsp;  <i class="fas fa-sort"></i></th>
                <th id="tipe_id" name="2" onclick="MakeRequestData('{{route('ordenar-preguntas')}}' + '/' + $(this).attr('name'), '#bodyTable', true)" class="ocultar">ENTRADA &nbsp;  <i class="fas fa-sort"></i></th>
                <th>ACCIONES</th>
            @endslot

            @slot('body')
            @include('main.preguntas.data')
            @endslot

        </x-table>
