
<x-card-modal>
    @slot('titulo')
        Secciones de la pregunta
    @endslot

    @slot('body')

    <form id="FrmPregunta" method="POST" novalidate>
        @csrf
        <div>
            <input type="hidden" class="form-control d-none" name="id" id="id" value="">

            <div class="row">

                <div class="col-lg-12">
                    <div class="mb-3">
                        <label class="form-label">Secciones</label>
                        <select class="form-control" name="seccionesP" id="seccionesP">
                            <option value="" disabled selected>Secciones</option>
                            @foreach($users as $item)
                            <option value="">{{$item->secc_nombre}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>
    @endslot

</x-card-modal>



