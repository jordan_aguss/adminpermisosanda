@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Preguntas')
@section("scripts")

    <script src="{{ asset("js/Sortable.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("js/sortable-jquery.js") }}" type="text/javascript"></script>

@endsection
@section('content')
<div class="col-12" id="DivPrincipal">
    <label class="d-none" hidden id="currentContent">#bodyTable</label>
    @csrf
    <div class="row">

        @include('main.preguntas.info-table')

    </div>
</div>
@endsection
