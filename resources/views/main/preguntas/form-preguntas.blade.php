<x-card-modal>

    @slot('titulo')
        Información del formulario {{$Info->form_nombre}}
    @endslot

    @slot('body')
        <form id="FormNForm">
            @csrf
            <input type="hidden" name="form" value="{{$Info->form_id}}">
            <input type="hidden" name="permiso" value="{{Session::get('perm_id')}}">
            <input type="hidden" name="tipo" value="{{$request->tipo}}">

            @php
                $FormActual = 0;
                $Tab = 1;
                $Fill = false;
                $selecciones = [];
                $hijas = true;
            @endphp

            <div class="row">
                @include('main.preguntas.form-respuestas')
            </div>
        </form>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>

        <button type="button" class="btn btn-outline-primary" onclick="MakeRequestData( '{{ route('preguntas-formulario') }}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', {{$request->Accion}}, '#FormNForm', false, true)">
            <i class="fas fa-save"></i>&nbsp;&nbsp;&nbsp;Guardar
        </button>

    @endslot

</x-card-modal>



