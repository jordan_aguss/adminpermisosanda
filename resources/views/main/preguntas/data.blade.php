
@foreach ($Preguntas as $item)

    <tr id="pregunta-{{$item->preg_id}}">
        <td class="ocultar">{{$item->preg_id}}</td>
        <td>{{$item->preg_nombre}}</td>
        <td class="ocultar">{{$item->tipe_nombre}}</td>
        <td>

            <button  class="col-auto btn btn-outline-warning"
            onclick="MakeRequestData( '{{ route('modal-secciones') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/{{$item->preg_id}}', 'Accion/3','Contenedor/pregunta-{{$item->preg_id}}'])">
                <i class="fas fa-align-justify"></i>
            </button>

            <button class="col-auto btn btn-outline-warning"
            onclick="MakeRequestData( '{{ route('pregunta-form') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/{{$item->preg_id}}', 'Accion/3','Contenedor/pregunta-{{$item->preg_id}}'])">
                <i class="fas fa-edit"></i>
            </button>

            @if (in_array($item->tipe_id, [2, 14]))
                <button class="btn btn-success ocultar"
                onclick="MakeRequestData( '{{ route('get-opciones', [$item->preg_id]) }}', '.modal-content', true, '#modal-principal')">
                    <i class="fas fa-cogs"></i>
                </button>

                <button class="btn btn-primary" onclick="MakeRequestData('{{route('get-children', [$item->preg_id])}}','#DivPrincipal', true)">
                    <i class="fas fa-code-branch"></i>
                </button>
            @endif
        </td>
    </tr>

@endforeach

@include('main.home.paginador', ['Datos' => $Preguntas])
