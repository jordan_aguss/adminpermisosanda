@foreach ($Preguntas as $item)
    <div class="card cursor" id="child-{{$item->pred_id}}">
        <div class="d-flex justify-content-between align-items-center ms-3 me-3 mt-1 mb-2 ps-2 pe-2 pt-1 pb-2">

            <span class="justify-content-start">{{$item->preg_nombre}} </span>

            <div class="justify-content-end">


                <div class="btn-group dropstart">
                    <button type="button" class="btn btn-success" data-bs-toggle="dropdown" aria-expanded="false">
                        <i class="fas fa-list"></i>
                    </button>
                    <ul class="dropdown-menu">

                        @foreach ($Opciones as $opci)
                            <a class="dropdown-item bg-{{$opci->opci_id == $item->opci_id ? 'danger' : 'primary'}} text-white pt-3 pb-3 pregop-{{$item->pred_id}}"
                            style="cursor: pointer"
                            onmouseover="$(this).addClass('bg-white'); $(this).removeClass('text-white')"
                            onmouseout="$(this).removeClass('bg-white'); $(this).addClass('text-white')"
                            onclick="CambiarOpcion( this, '.pregop-{{$item->pred_id}}','{{route('change-pregunta-option', [$item->pred_id, $opci->opci_id, $pregunta])}}')"> {{$opci->opci_nombre}} </a>
                        @endforeach
                    </ul>
                </div>
            </div>


        </div>
    </div>
@endforeach
