<script>

    $('#DataFree').sortable({
        group: {
            name: "preguntas-list",
            pull: true,
            put: true
        },
        animation: 150,
        easing: "cubic-bezier(0.895, 0.03, 0.685, 0.22)",
        chosenClass: "bg-primary",
        onAdd: (evento) => {

            let id = $(evento.item).attr('id');
            let seccion = $('#divSeccion').attr('padre');

            MakeRequestData('{{route($ruta, [1])}}' + "/" + seccion + '/' + id, '');
        },
    });

    $('#DataAdd').sortable({
        group: {
            name: "preguntas-list",
            pull: true,
            put: true
        },
        animation: 150,
        easing: "cubic-bezier(0.895, 0.03, 0.685, 0.22)",
        chosenClass: "bg-primary",
        onAdd: (evento) => {

            let id = $(evento.item).attr('id');
            let seccion = $('#divSeccion').attr('padre');

            MakeRequestData('{{route($ruta, [0])}}' + "/" + seccion + '/' + id, '');
        },
        onUpdate: (evento) => {
            //actualiza al mover
            let id = ($(evento.item).attr('id'));
            // alert($(evento.item).attr('padre'));
            MakeRequestData('{{isset($rutaOrden) ? $rutaOrden :  ''}}', '', false, '', 'POST', -1, '', false, false,
            ['id/' + id, 'inicio/' + evento.oldDraggableIndex, 'fin/' + evento.newIndex, 'padre/' + ($(evento.item).attr('padre')) ]);
        },
    });

    function CambiarOpcion(item, id, url){
            $(id).removeClass('bg-danger')
            $(id).addClass('bg-primary')
            $(item).addClass('bg-danger');
            MakeRequestData(url);
        }

</script>

<style>
    .scroll{
        overflow-y: scroll;
    }
</style>

<div class="row">
    <label class="d-none" hidden id="currentContent">#DataFree</label>

    <div class="col-12 mb-3">
        <div class="card">
            <div class="m-2 d-flex align-items-center" id="divSeccion" padre="{{$Info->id}}">

                <button class="btn-primary btn"
                onclick="MakeRequestData( '{{ route($rutaBack) }}', '#DivPrincipal', true)">
                    <i class="fas fa-arrow-left"></i> &nbsp; Regresar
                </button>

                <span class="card-title m-0 ms-3"> {{$Info->nombre}} </span>
            </div>
        </div>
    </div>

    <div class="col-xl-6 col-md-6 col-sm 12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Preguntas disponibles</h5>
            </div>

            <div class="card-body">
                <div class="input-icon mb-3">
                    <input type="text" class="form-control" placeholder="Buscar..."
                    onkeyup="if(event.keyCode == 13) MakeRequestData('{{$rutaBuscar}}' + '/' + $(this).val() , '#DataFree', true)" >
                    <span class="input-icon-addon">
                        <i class="fas fa-search"></i>
                    </span>
                </div>

                <div id="DataFree" class="scroll" style="height: 70vh;">
                    @include('main.preguntas.result-asing', ['busqueda' => false, 'pager' => 'paginador', 'rutaDetalle' => null])
                </div>

                <div id="pager" class="contentPager" link="#DataFree">
                    <div class="d-flex justify-content-center bg-white">
                        <p class="m-0 text-muted mt-2 float-left">Mostrando <span>{{($DataFree->currentpage()-1)*$DataFree->perpage()+1}}</span> a <span>{{$DataFree->currentpage()*$DataFree->perpage()}}</span> de <span>{{$DataFree->total()}}</span> registros</p>
                        <div class="mt-1 mb-0">{!! $DataFree->links() !!}</div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="col-xl-6 col-md-6 col-sm 12">
        <div class="card">
            <div class="card-header">
                <h5 class="card-title">Preguntas asignadas</h5>
            </div>

            <div class="card-body scroll" style="height: 80vh;" id="DataAdd">
                @include('main.preguntas.result-asing', ['busqueda' => false, 'DataFree' => $DataAdd, 'pager' => 'paginadorAgregadas'])
            </div>

            <div id="paginadorAgregadas" class="contentPager" link="#DataAdd">
                <div class="d-flex justify-content-center bg-white">
                    <p class="m-0 text-muted mt-2 float-left">Mostrando <span>{{($DataAdd->currentpage()-1)*$DataAdd->perpage()+1}}</span> a <span>{{$DataAdd->currentpage()*$DataAdd->perpage()}}</span> de <span>{{$DataAdd->total()}}</span> registros</p>
                    <div class="mt-1 mb-0">{!! $DataAdd->links() !!}</div>
                </div>
            </div>
        </div>
    </div>
</div>
