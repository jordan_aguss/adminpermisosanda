
@foreach ($DataFree as $item)
<div class="card" id="{{$item->id}}" padre="{{isset($Info->id) ? $Info->id : ''}}">
    <div class="card-body">
        <div class="d-flex justify-content-between align-items-center">
            <div>
                {{$item->nombre}}
            </div>

            @if (isset($rutaDetalle))

                <div class="justify-content-end">


                    <div class="btn-group dropstart">
                        <button type="button" class="btn btn-success" data-bs-toggle="dropdown" aria-expanded="false">
                            <i class="fas fa-list"></i>
                        </button>
                        <ul class="dropdown-menu">

                            @foreach ($Opciones as $opci)
                                <a class="dropdown-item bg-{{$opci->opci_id == $item->opci_id ? 'danger' : 'primary'}} text-white pt-3 pb-3 pregop-{{$item->id}}"
                                style="cursor: pointer"
                                onmouseover="$(this).addClass('bg-white'); $(this).removeClass('text-white')"
                                onmouseout="$(this).removeClass('bg-white'); $(this).addClass('text-white')"
                                onclick="CambiarOpcion( this, '.pregop-{{$item->id}}','{{route('change-pregunta-option', [$item->id, $opci->opci_id, $Info->id])}}')"> {{$opci->opci_nombre}} </a>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endforeach

@if (!isset($busqueda))

<div id="Newpager" class="d-none" hidden>
    <div class="d-flex justify-content-center bg-white">
        <p class="m-0 text-muted mt-2 float-left">Mostrando <span>{{($DataFree->currentpage()-1)*$DataFree->perpage()+1}}</span> a <span>{{$DataFree->currentpage()*$DataFree->perpage()}}</span> de <span>{{$DataFree->total()}}</span> registros</p>
        <div class="mt-1 mb-0">{!! $DataFree->links() !!}</div>
    </div>
</div>

<script>
    $('#' {{ isset($pager) ? $pager : 'pager'}}).html($('#Newpager').html());
    SetDataResult('#Newpager', 3, '');
</script>
@endif
