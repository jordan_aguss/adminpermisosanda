
<x-card-modal>
    @slot('titulo')
        Información de la pregunta
    @endslot

    @slot('body')

    <form id="FrmPregunta" method="POST" novalidate>
        @csrf
        <div>
            <input type="hidden" class="form-control d-none" name="id" id="id" value="{{$Info->preg_id}}">

            <div class="row">

                <div class="col-lg-12">
                    <div class="mb-3">
                        <label class="form-label">Pregunta</label>
                        <textarea name="nombre" id="nombre" required class="form-control" rows="3" maxlength="255">{{$Info->preg_nombre}}</textarea>
                    </div>
                </div>


                <div class="col-lg-4">
                        <div class="mb-3">
                            <label class="form-label">Tipo de entrada</label>
                            <select class="form-select" name="tipo" id="tipo" required onchange="ShowInput($(this).val())">
                                <option value="" selected>---Seleccionar---</option>
                                @foreach ($Entradas as $item)
                                <option value="{{$item->tipe_id}}" {{$Info->tipe_id == $item->tipe_id ? 'selected':''}}>{{$item->tipe_nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
    
                    <div class="col-lg-4">
                        <div class="mb-3">
                            <label class="form-label">Pregunta padre</label>
                            <select class="form-select" name="padre" id="padre" required>
                                <option {{$Info->preg_padre ? '' : 'selected'}} value="0">No</option>
                                <option {{$Info->preg_padre ? 'selected' : ''}} value="1">Si</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="mb-3">
                            <label class="form-label">Tipo de validación</label>
                            <select class="form-select" name="tipoV" id="tipoV" required>
                                <option value="" selected>---Seleccionar---</option>
                                @foreach ($Validaciones as $item)
                                <option value="{{$item->vali_id}}" {{$Info->vali_id == $item->vali_id ? 'selected':''}}>{{$item->vali_nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>


                <div class="col-lg-12">
                    <div class="mb-3">
                        <label class="form-label">Info de ayuda</label>
                        <textarea name="ayuda" id="ayuda" class="form-control" rows="3" required>{{$Info->preg_ayuda}}</textarea>
                    </div>
                </div>


                <div class="col-lg-12 d-none" id="divJson">
                    <div class="mb-3">
                        <label class="form-label">Campos solicitar por los registros</label>
                        <textarea name="campos" id="campos" class="form-control" rows="3">{{$Info->preg_campos}}</textarea>
                    </div>
                </div>

            </div>
        </div>
    </form>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>

        <button type="button" class="btn btn-outline-primary" onclick="MakeRequestData( '{{ route('pregunta-save') }}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', {{$request->Accion}}, '#FrmPregunta', false, true)">
            <i class="fas fa-save"></i>&nbsp;&nbsp;&nbsp;Guardar
        </button>
    @endslot

</x-card-modal>


<script>
    function ShowInput(valor){
        $('#divJson').addClass('d-none');

        if(valor == 1){
            $('#divJson').removeClass('d-none');
        }
    }
</script>



