@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'INFORMACIÓN GENERAL')
@section("scripts")
<script src="{{ asset("js/index.js") }}" type="text/javascript"></script>
@endsection

@section('content')

<form id="FormInfo">
    @csrf
    <div class="row">
        <div class="col-xl-4">
            <!-- Profile picture card-->
            <div class="card mb-4 mb-xl-0">
                <div class="card-header"><strong>Logo de la institución</strong></div>
                <div class="card-body text-center">
                    <img class="img-account-profile rounded-circle mb-2" id="imgLogo" src="{{$Info->sucu_logo}}" alt="">
                    <!-- Profile picture help block-->
                    <div class="small font-italic text-muted mb-4">JPG or PNG no larger than 5 MB</div>
                    <input type="hidden" name="logo" value="{{$Info->sucu_logo}}">


                    <input type="file" id="Newlogo" name="Newlogo" class="d-none"
                    onchange="readURL(this, '#imgLogo')">

                    <!-- Profile picture upload button-->
                    <label for="Newlogo" class="btn btn-primary">Subir nuevo logo</label>
                </div>
            </div>
        </div>

        <input type="hidden" name="id" value="{{$Info->sucu_id}}">

        <div class="col-xl-8">
            <div class="card mb-4">
                <div class="card-header"> <strong> Detalles de la institución</strong></div>
                <div class="card-body pt-0">
                        <div class="row mb-3">
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1">Nombre</label>
                                <input class="form-control" name="nombre" type="text" value="{{$Info->sucu_nombre}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1" >Teléfono</label>
                                <input class="form-control" type="text" name="telefono" value="{{$Info->sucu_telefono}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1">Correo</label>
                                <input class="form-control" type="email" name="correo" value="{{$Info->sucu_correo}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1" >Facebook</label>
                                <input class="form-control" name="facebook" type="text" value="{{$Info->sucu_facebook}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1">Youtube</label>
                                <input class="form-control" name="youtube" type="text" value="{{$Info->sucu_youtube}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1" >Twitter</label>
                                <input class="form-control" name="twitter" type="text" value="{{$Info->sucu_twitter}}">
                            </div>
                            <div class="col-md-6 col-xl-6 col-sm-12 mt-3">
                                <label class="small mb-1" >Color de fondo</label>
                                <input class="form-control" name="color" type="color" value="{{$Info->sucu_color}}">
                            </div>
                            <div class="col-12 mt-3">
                                <label class="small mb-1">Dirección</label>
                                <textarea name="direccion" class="form-control" cols="30" rows="10">{{$Info->sucu_direccion}}</textarea>
                            </div>
                        </div>

                        <div class="text-end">
                            <button class="btn btn-success" type="button"
                            onclick="MakeRequestData('{{route('sucursal-save')}}', '',true, '', 'POST', 1, '#FormInfo')"
                            > <i class="fas fa-save"></i> &nbsp; Guardar información</button>
                        </div>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection
