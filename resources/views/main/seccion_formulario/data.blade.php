@foreach ($data_req as $dat)

    <tr id="itemSeccion-{{$dat->secc_id}}">
        <td>{{$dat->secc_id}}</td>
        <td>{{$dat->secc_nombre}}</td>
        <td>
            <button class="col-auto btn btn-outline-warning"
                onclick="MakeRequestData( '{{ route('seccion_formulario.modal') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/{{$dat->secc_id}}', 'Total/2','Contenedor/itemSeccion-{{$dat->secc_id}}', 'Accion/3'])">
                <i class="fas fa-edit"></i>
            </button>

            <button class="btn btn-success" onclick="MakeRequestData( '{{ route('seccion-preguntas', [$dat->secc_id]) }}', '#DivPrincipal', true)">
                <i class="fas fa-question"></i>
            </button>
        </td>
    </tr>

@endforeach



@include('main.home.paginador', ['Datos' => $data_req])


