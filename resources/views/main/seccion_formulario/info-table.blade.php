
    <x-table>  
        @slot('idTabla', 'bodySectionTable')

        @slot('titulo')
        Visor de secciones de preguntas
        @endslot
        
        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-secciones')}}' + '/' + $(this).val() , '#bodySectionTable', true)" 
        @endslot

        @slot('btnAdicionales')
            <button onclick="MakeRequestData( '{{ route('seccion_formulario.modal') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/0', 'Accion/@if(count($data_req) == 0) 2 @else 0 @endif','Contenedor/bodySectionTable'])" class="btn btn-primary d-none d-sm-inline-block" id="nuevo">
                <i class="fas fa-plus"></i>
                &nbsp;&nbsp;Nuevo
            </button>
        @endslot


        @slot('header')
            <th id="secc_id" name="0" onclick="MakeRequestData('{{route('ordenar-secciones')}}' + '/' + $(this).attr('name'), '#bodySectionTable', true)">ID &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="preg_id" name="1" onclick="MakeRequestData('{{route('ordenar-secciones')}}' + '/' + $(this).attr('name'), '#bodySectionTable', true)">NOMBRE &nbsp;  <i class="fas fa-sort"></i></th>
            <th>ACCIONES</th>
        @endslot
        
        @slot('body')
            @include('main.seccion_formulario.data')
        @endslot

    </x-table>