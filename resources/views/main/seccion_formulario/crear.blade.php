<x-card-modal>
    @slot('titulo')
    Información de la sección
    @endslot

    @slot('body')

        <form id="FormSeccion" novalidate>
            @csrf

            <div class="row p-0 m-0">
                <input type="hidden" value="{{$request->Id}}" name="Id" id="Id"/>
                <div class="mb-3 col-lg-6">
                    <label class="form-label">Nombre</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" value="{{$Info->secc_nombre}}" required>
                </div>
            </div>
        </form>

    @endslot

    @slot('footer')
        <button class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp; &nbsp; Cancel</button>
        <button onclick="MakeRequestData( '{{ route('seccion_formulario.guardar') }}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', {{$request->Accion}}, '#FormSeccion', false, true)"
                class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseAddHelpDesk" aria-expanded="false" aria-controls="collapseAddHelpDesk">
            Guardar &nbsp; <i class="fas fa-save"></i>
        </button>
    @endslot

</x-card-modal>
