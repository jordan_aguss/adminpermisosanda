@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Permisos')
@section("scripts")

@endsection

@section('content')

<x-table>
    @slot('idTabla')
    tablePermisos
    @endslot

    @slot('titulo')
    Listado de permisos
    @endslot


    @slot('accionBuscar')
        onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-preguntas')}}' + '/' + $(this).val() , '#tablePermisos', true)"
    @endslot

    @slot('btnAdicionales')
        <button onclick="MakeRequestData( '{{ route('admin.permission-modal') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
        ['Id/0', 'Accion/@if(count($rows) == 0) 2 @else 4 @endif','Contenedor/tablePermisos'])"
         class="btn btn-primary d-none d-sm-inline-block" id="nuevo">
            <i class="fas fa-plus"></i>
            &nbsp;&nbsp;Nuevo
        </button>
    @endslot


    @slot('header')
        <th>NOMBRE</th>
        <th>DESCRIPCIÓN</th>
        <th>ACCIONES</th>
    @endslot

    @slot('body')
        @include('main.permission.data')
    @endslot

</x-table>
@endsection
