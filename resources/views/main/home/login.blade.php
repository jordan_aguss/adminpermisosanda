<div class="container">
    <div class="row justify-content-center">
        <div class="col-xl-9 col-lg-11 col-md-12 p-0 m-0">
            <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-4">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pl-4 pr-4 pt-1">
                            <div class="p-1">
                                <div class="d-flex justify-content-between align-items-center">
                                    <div class="justify-content-start">
                                        <h1 class="h4 text-gray-900">Inicio de sesión</h1>
                                    </div>
                                    <div class="d-flex justify-content-end">
                                        <img src="{{asset('images/logo.jpeg')}}" height="auto" alt="">
                                    </div>
                                </div>
                                <form class="user" id="LoginForm" novalidate>
                                    @csrf
                                    <div class="form-group mb-4">
                                        <div class="d-flex justify-content-start ">
                                            <i class="fa fa-user black"></i>
                                            <h6 class="black">&nbsp&nbspUsuario o email</h6>
                                        </div>
                                        <input type="email" class="form-control" id="MailUser" name="MailUser" placeholder="Escribe tu usuario o email"
                                        onkeyup="if(event.keyCode === 13) SendInfoForm('{{route('Registro.Login')}}', '#LoginForm', '#BtnLogin', 1)">
                                    </div>
                                    <div class="form-group pt-2 mb-0">

                                        <div class="d-flex justify-content-start">
                                            <i class="fa fa-key black"></i>
                                            <h6 class="black">&nbsp&nbspContraseña</h6>
                                        </div>
                                        <input type="password" class="form-control" name="PassUser" id="PassUser" placeholder="Escribe tu contraseña"
                                        onkeyup="if(event.keyCode === 13) SendInfoForm('{{route('Registro.Login')}}', '#LoginForm', '#BtnLogin', 1)">
                                    </div>

                                    <div class="text-right mb-3" onclick="LoadPage('{{route('Home.LPage',[4])}}')">
                                        <a class="small" href="#">¿Olvidaste tu contraseña?</a>
                                    </div>

                                    <button type="button" class="btn colorSea text-white col-12" id="BtnLogin"
                                            onclick="SendInfoForm('{{route('Registro.Login')}}', '#LoginForm', '#BtnLogin', 1)"> <strong> Iniciar Sesión </strong></button>
                                </form>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
bootstrapValidate('#MailUser', 'email: Introduzca una direccion de correo valida');
bootstrapValidate('#PassUser', 'required: La constraseña es requerida');
</script>
