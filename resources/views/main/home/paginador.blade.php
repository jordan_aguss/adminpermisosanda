
@if (!is_array($Datos))
    <tr>
        <td colspan="{{isset($cols) ? $cols : 3}}">
            <div class="d-flex justify-content-center bg-white">
                <p class="m-0 text-muted mt-2 float-left">Mostrando <span>{{($Datos->currentpage()-1)*$Datos->perpage()+1}}</span> a <span>{{$Datos->currentpage()*$Datos->perpage()}}</span> de <span>{{$Datos->total()}}</span> registros</p>
                <div class="mt-1 mb-0">{!! $Datos->links() !!}</div>
            </div>
        </td>
    </tr>
@endif
