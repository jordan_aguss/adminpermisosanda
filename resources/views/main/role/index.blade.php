@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Roles')


@section('content')

    <x-table>
        @slot('idTabla')
        tablePermisos
        @endslot

        @slot('titulo')
        Listado de permisos
        @endslot


        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-preguntas')}}' + '/' + $(this).val() , '#tablePermisos', true)"
        @endslot

        @slot('btnAdicionales')
            <a href="{{ route('admin.role.create') }}" title="Crear Rol"
            class="btn btn-success">
                <i class="fa fa-plus"></i> Agregar nuevo Rol
            </a>
        @endslot


        @slot('header')
            <th>NOMBRE</th>
            <th>DESCRIPCIÓN</th>
            <th>ACCIONES</th>
        @endslot

        @slot('body')
            @foreach($rows as $row)
                <tr>
                    <td>{{ $row->name }}</td>
                    <td>{{ $row->description }}</td>
                    <td>
                        <a href="{{ route('admin.role.show' , $row->id) }}" class="btn btn-primary">Mostrar</a>
                        <a href="{{ route('admin.role.edit' , $row->id) }}" class="btn btn-secondary">Editar</a>
                    @if($row->name !== 'admin')
                        <form style="margin-top:10px!important;" method="POST" action="{{ route('admin.role.destroy' , $row->id) }}">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <div class="form-group">
                                <input type="submit" class="btn btn-danger delete-user" value="Eliminar Rol">
                            </div>
                        </form>
                    @endif

                    </td>
                </tr>
            @endforeach
        @endslot

    </x-table>
    {{ $rows->render() }}
@endsection
