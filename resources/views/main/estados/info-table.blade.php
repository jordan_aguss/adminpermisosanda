
    <x-table>
        @slot('idTabla', 'bodyStatusTable')

        @slot('titulo')
        Visor de Estados
        @endslot

        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-estado')}}' + '/' + $(this).val() , '#bodyStatusTable', true)"
        @endslot

        @slot('btnAdicionales')
            <button onclick="MakeRequestData( '{{ route('estado.modal') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/0', 'accion/@if(count($data_req) == 0) 2 @else 4 @endif','atributo/#bodyStatusTable'])" class="btn btn-primary d-none d-sm-inline-block" id="nuevo">
                <i class="fas fa-plus"></i>
                &nbsp;&nbsp;Nuevo
            </button>
        @endslot


        @slot('header')
            <th>ID</th>
            <th>NOMBRE</th>
            <th>DESCRIPCION</th>
            <th>ACCIONES</th>
        @endslot

        @slot('body')
            @include('main.estados.data')
        @endslot

    </x-table>
