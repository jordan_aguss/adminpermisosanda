@extends('layouts.template')

@section('tittle', 'Asignación de estados')

@section('content')

    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Formulario - {{$Info->form_nombre}}</h5>
        </div>

        <div class="card-body">

            <div class="card card-body" style="width: 500px;">
                <div class="d-flex justify-content-between col-12">

                    <button class="btn btn-warning"
                    onclick="

                    MakeRequestData('{{route('get-subestados')}}',  '#FrmEst-{{$Info->esta_id}}', true, '', 'POST', 2, '', false, false,
            ['form/{{$Info->form_id}}', 'estado/{{$Info->esta_id}}'])">
                        <i class="fas fa-arrows-alt"></i>
                    </button>

                    <h3 class="card-title m-0">
                        {{$Info->esta_nombre}}
                    </h3>

                    <div>
                        <button class="btn btn-success"
                        onclick="
                        MakeRequestData('{{route('modal-asign')}}',  '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
                            ['contenedor/#FrmEst-{{$Info->esta_id}}', 'form/{{$Info->form_id}}', 'estado/{{$Info->esta_id}}'])">
                            <i class="fas fa-plus"></i>
                        </button>
                    </div>
                </div>
                <hr class="mt-2 m-0">

                <div class="m-2 ms-4 bg-light" id="FrmEst-{{$Info->esta_id}}"></div>

            </div>

        </div>


    </div>

@endsection
