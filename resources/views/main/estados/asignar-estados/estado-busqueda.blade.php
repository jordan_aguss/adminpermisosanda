@foreach ($Estados as $item)

    <tr>
        <td>{{$item->esta_id}}</td>
        <td>{{$item->esta_nombre}}</td>
        <td>
            <button class="btn btn-primary"
            onclick="MakeRequestData('{{route('subestado.form')}}',  '.modal-content', true, '', 'POST', 2, '', false, false,
            ['contenedor/{{$request->contenedor}}', 'form/{{$request->form}}', 'estado/{{$request->estado}}', 'id/{{$item->esta_id}}', 'tipo/0', 'accion/{{$request->accion}}'])">
                <i class="fas fa-plus"></i>
            </button>
        </td>

    </tr>

@endforeach
