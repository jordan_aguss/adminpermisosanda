<x-card-modal>
    @slot('titulo')
        Información del formulario
    @endslot

    @slot('body')

    <x-table>
        @slot('idTabla')
        StatusTable
        @endslot

        @slot('titulo')
        Buscador de estados
        @endslot

        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13)
            MakeRequestData('{{route('buscar-estados-asign')}}',  '#StatusTable', true, '', 'POST', 2, '', false, false,
            ['contenedor/{{$request->contenedor}}', 'form/{{$request->form}}', 'estado/{{$request->estado}}', 'palabra/' + $(this).val()])"
        @endslot


        @slot('header')
            <th>ID</th>
            <th>NOMBRE</th>
            <th>ACCIONES</th>
        @endslot

        @slot('body')

        @endslot

    </x-table>

    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>
    @endslot

</x-card-modal>

