<x-card-modal>
    @slot('titulo')
        Configuración del estado: {{$Info->esta_nombre}}
    @endslot

    @slot('body')
        <form id="form-general" class="form-horizontal" method="POST">
            @csrf
            <div>
                <div class="row">
                    <input type="hidden" value="{{$request->tipo}}" name="tipoModal" id="tipoModal"/>
                    <input type="hidden" value="{{$Info->esta_id}}" name="Id" id="Id"/>
                    <input type="hidden" value="{{$request->form}}" name="form" id="Id"/>
                    <input type="hidden" value="{{$request->estado}}" name="estado" id="Id"/>
                    <input type="hidden" value="{{$Info->esta_nombre}}" name="nombre" id="Id"/>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Formulario a cargar</label>
                        <select name="newform" id="newform" class="form-control">
                            <option value="">--seleccionar--</option>
                            @foreach ($Forms as $item)
                                <option {{$item->form_id == $Info->form_new ? 'selected' : ''}} value="{{$item->form_id}}">{{$item->form_nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Privilegio necesario</label>
                        <select name="privilegio" id="privilegio" class="form-control" required>
                            <option value="">--seleccionar--</option>
                            @foreach ($Permisos as $item)
                                <option {{$item->id == $Info->priv_id ? 'selected' : ''}} value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>

                    @if ($request->tipo)
                        <input type="hidden" value="{{$Info->estc_tipo}}" name="tipo">
                    @else
                        <div class="mb-3 col-lg-6">
                            <label class="form-label">Tipo de estado</label>
                            <select name="tipo" id="tipo" required class="form-control">
                                <option value="">--Seleccionar--</option>
                                <option value="0">Cambio de estado</option>
                                <option value="1">Nuevo formulario</option>
                                <option value="2">Editar formulario</option>
                                <option value="3">Asignar técnico</option>
                                <option value="4">Verificación de documentos</option>
                                <option value="5">Generar documento</option>
                                <option value="6">Verificación de solvencia</option>
                                <option value="7">Multiple ingreso de formulario</option>
                            </select>
                        </div>
                    @endif

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Enviar correo</label>
                        <select name="correo" id="correo" required class="form-control">
                            <option value="">--Seleccionar--</option>
                            <option {{$Info->estc_correo == 0 ? 'selected' : ''}} value="0">No</option>
                            <option {{$Info->estc_correo == 1 ? 'selected' : ''}} value="1">Si</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Color del boton</label>
                        <select name="color" id="color" required class="form-control">
                            <option value="">--Seleccionar--</option>
                            <option {{$Info->estc_color == 'primary' ? 'selected' : ''}} value="primary">Azul</option>
                            <option {{$Info->estc_color == 'danger' ? 'selected' : ''}} value="danger">Rojo</option>
                            <option {{$Info->estc_color == 'success' ? 'selected' : ''}} value="success">Verde</option>
                            <option {{$Info->estc_color == 'info' ? 'selected' : ''}} value="info">Celeste</option>
                            <option {{$Info->estc_color == 'purple' ? 'selected' : ''}} value="purple">Morado</option>
                            <option {{$Info->estc_color == 'warning' ? 'selected' : ''}} value="warning">Amarillo</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Uso del estado</label>
                        <select name="cambio" id="cambio" required class="form-control">
                            <option value="">--Seleccionar--</option>
                            <option {{$Info->estc_cambio == 0 ? 'selected' : ''}} value="0">Interno</option>
                            <option {{$Info->estc_cambio == 1 ? 'selected' : ''}} value="1">Externo</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Requiere pago</label>
                        <select name="pago" id="pago" class="form-control">
                            <option value="">--Seleccionar--</option>
                            <option {{$Info->estc_pago == 0 ? 'selected' : ''}} value="0">No</option>
                            <option {{$Info->estc_pago == 1 ? 'selected' : ''}} value="1">Si</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Documento a generar</label>
                        <select name="documento" id="documento" class="form-control">
                            <option selected value="">--Seleccionar--</option>
                            <option {{$Info->estc_documento == 0 ? 'selected' : ''}} value="0">Opinion tecnica</option>
                            <option {{$Info->estc_documento == 1 ? 'selected' : ''}} value="1">Resolucion aprobación planos</option>
                            <option {{$Info->estc_documento == 2 ? 'selected' : ''}} value="2">Constancia de campo</option>
                            <option {{$Info->estc_documento == 3 ? 'selected' : ''}} value="3">Constancia de habilitación</option>
                            <option {{$Info->estc_documento == 4 ? 'selected' : ''}} value="4">Solicitud de habilitación de obra</option>
                            <option {{$Info->estc_documento == 5 ? 'selected' : ''}} value="5">Recepción final de campo</option>
                            <option {{$Info->estc_documento == 6 ? 'selected' : ''}} value="6">Certificado de factibilidad</option>
                            <option {{$Info->estc_documento == 7 ? 'selected' : ''}} value="7">Informe de infraestructura</option>
                            <option {{$Info->estc_documento == 8 ? 'selected' : ''}} value="8">Nota de denegación</option>
                        </select>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Fecha limite</label>
                        <input name="FLimite" id="FLimite" type="date" class="form-control" value="{{$Info->estc_vencimiento}}"/>
                    </div>

                    <div class="col-12 row p-0 m-0">
                        <div class="col-xl-6 col-md-6 col-sm-12">
                            <div class="input-group">
                                <div class="form-outline">
                                  <input type="search" id="form1" class="form-control" />
                                </div>
                                <button type="button" class="btn btn-primary"
                                onclick="MakeRequestData('{{route('buscar-estado')}}' + '/' + $('#form1').val() +'/1', '#vencido', true)">
                                  <i class="fas fa-search"></i>
                                </button>
                              </div>

                        </div>

                        <div class="mb-3 col-lg-6 col-sm-12">
                            <select name="vencido" id="vencido" class="form-control">
                                <option value="">--Seleccionar--</option>
                            </select>
                        </div>
                    </div>

                </div>
            </div>
        </form>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>

        <button type="button" class="btn btn-outline-primary"
        onclick="MakeRequestData( '{{ route('form-asign-status') }}', '{{$request->contenedor}}', true, '#modal-principal', 'POST', {{isset($request->accion) ? $request->accion : 0}}, '#form-general', false, true)">
            <i class="fas fa-save"></i>&nbsp;&nbsp;&nbsp;Guardar
        </button>
    @endslot

</x-card-modal>

<script>
     $("#form-general").on('submit', function(evt){
        evt.preventDefault();
    });
</script>

