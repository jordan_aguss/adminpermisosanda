@foreach ($Estados as $item)

    <div class="card card-body mb-4" style="width: 500px;" id="ContentEstado-{{$item->esta_id}}">
        <div class="d-flex justify-content-between col-12">

            @if ($item->estc_tipo == 0)
                <div class="align-items-center">
                    <button class="btn btn-warning"
                        onclick="MakeRequestData('{{route('get-subestados')}}',  '#FrmEst-{{$item->esta_id}}', true, '',
                        'POST', 2, '', false, false, ['form/{{$request->form}}', 'estado/{{$item->esta_id}}'])">
                        <i class="fas fa-arrows-alt"></i>
                    </button>
                </div>
            @endif

            <div class="justify-content-start">
                <h3 class="card-title m-0">
                    {{$item->esta_nombre}}
                </h3>
                <h6 class="text-primary">
                    @switch($item->estc_tipo)
                        @case(0)
                        Cambio de estado
                            @break
                        @case(1)
                        Nuevo formulario
                            @break
                        @case(2)
                        Editar formulario
                            @break
                        @case(3)
                        Asignar técnico
                            @break
                        @case(4)
                        Verificación de documentos
                            @break
                        @case(5)
                        Generar documento
                            @break
                        @case(6)
                        Verificación de solvencia
                            @break
                        @case(7)
                        Multiple ingreso de formulario
                            @break
                        @default

                    @endswitch </h6>
            </div>

            <div>

                <div class="align-items-center">
                    @if ($item->estc_tipo == 0)
                        <button class="btn btn-success"
                                onclick="MakeRequestData('{{route('modal-asign')}}',  '.modal-content', true, '#modal-principal',
                                'POST', 2, '', false, false, ['contenedor/#FrmEst-{{$item->esta_id}}', 'form/{{$request->form}}',
                                'estado/{{$item->esta_id}}', 'accion/0'])">
                            <i class="fas fa-plus"></i>
                        </button>
                    @endif

                    <button class="btn btn-danger"
                        onclick="MakeRequestData('{{route('eliminar-subestado', [$item->esta_id, $request->form, $request->estado])}}'
                        , '#ContentEstado-{{$item->esta_id}}', true, '', 'GET', 3)">
                        <i class="fas fa-trash"></i>
                    </button>

                    <button class="btn btn-yellow"
                    onclick="MakeRequestData('{{route('subestado.form')}}',  '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,
                    ['contenedor/{{$request->contenedor}}', 'form/{{$request->form}}', 'estado/{{$request->estado}}', 'id/{{$item->esta_id}}', 'tipo/1', 'accion/3'])">
                        <i class="fas fa-edit"></i>
                    </button>

                </div>

            </div>
        </div>
        <hr class="mt-2 m-0">

        <div class="m-2 ms-4 bg-light" id="FrmEst-{{$item->esta_id}}"></div>

    </div>
@endforeach
