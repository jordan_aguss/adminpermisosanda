<x-card-modal>
    @slot('titulo')
    Información del estado
    @endslot

    @slot('body')

        <form id="FormEstado" novalidate>
            @csrf

            <div class="row p-0 m-0">
                <input type="hidden" value="{{$request->Id}}" name="Id" id="Id"/>
                <div class="mb-3 col-lg-6">
                    <label class="form-label">Nombre</label>
                    <input type="text" class="form-control" name="nombre" id="nombre" value="{{$Info->esta_nombre}}" required>
                </div>
                <div class="mb-3 col-lg-6">
                    <label class="form-label">Descripción</label>
                    <input type="text" class="form-control" name="descripcion" id="descripcion" value="{{$Info->esta_descripcion}}" required>
                </div>

                <div class="mb-3 col-lg-6">
                    <label class="form-label">Estado general</label>
                    <select name="estadog" id="estadog" class="form-control">
                        <option value="">--seleccionar</option>
                        @foreach ($Estados as $item)
                            <option {{$item->estg_id == $Info->estg_id ? 'selected' : ''}} value="{{$item->estg_id}}">{{$item->estg_nombre}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </form>

    @endslot

    @slot('footer')
        <button class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp; &nbsp; Cancel</button>
        <button onclick="MakeRequestData( '{{ route('estado.guardar') }}', '{{$request->atributo}}', true, '#modal-principal', 'POST', {{$request->accion}}, '#FormEstado', false, true)"
                class="btn btn-success" type="button" data-toggle="collapse" data-target="#collapseAddHelpDesk" aria-expanded="false" aria-controls="collapseAddHelpDesk">
            Guardar &nbsp; <i class="fas fa-save"></i>
        </button>
    @endslot

</x-card-modal>
