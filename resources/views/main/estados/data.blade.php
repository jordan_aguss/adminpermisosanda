@foreach ($data_req as $dat)
    <tr id="itemSeccion-{{$dat->esta_id}}">
        <td>{{$dat->esta_id}}</td>
        <td>{{$dat->esta_nombre}}</td>
        <td>{{$dat->esta_descripcion}}</td>
        <td>
            <button class="col-auto btn btn-outline-warning"
                onclick="MakeRequestData( '{{ route('estado.modal') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/{{$dat->esta_id}}', 'atributo/#itemSeccion-{{$dat->esta_id}}','accion/3'])">
                <i class="fas fa-edit"></i>
            </button>

        </td>
    </tr>

@endforeach

@include('main.home.paginador', ['Datos' => $data_req])

