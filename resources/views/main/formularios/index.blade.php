@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'FORMULARIOS')
@section("scripts")

    <script src="{{ asset("js/Sortable.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("js/sortable-jquery.js") }}" type="text/javascript"></script>

@endsection
@section('content')
<div class="col-12" id="DivPrincipal">
    @csrf

    <label class="d-none" hidden id="currentContent">#bodyFormTable</label>
    @include('main.formularios.info-table')
</div>
</div>
@endsection
