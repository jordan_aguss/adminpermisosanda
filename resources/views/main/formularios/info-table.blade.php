
    <x-table>
        @slot('idTabla')
        bodyFormTable
        @endslot

        @slot('titulo')
        Visor de formularios
        @endslot

        @slot('btnAdicionales')
            <button onclick="MakeRequestData( '{{ route('form-editar') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/0', 'Accion/@if(count($data_req) == 0) 2 @else 0 @endif','Contenedor/bodyFormTable'])" class="btn btn-primary d-none d-sm-inline-block" id="nuevo">
                <i class="fas fa-plus"></i>
                &nbsp;&nbsp;Nuevo
            </button>
        @endslot

        @slot('accionBuscar')
            onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-formulario')}}' + '/' + $(this).val() , '#bodyFormTable', true)"
        @endslot


        @slot('header')
            <th id="form_id" name="0" onclick="MakeRequestData('{{route('ordenar-formularios')}}' + '/' + $(this).attr('name'), '#bodyFormTable', true)">ID &nbsp;  <i class="fas fa-sort"></i></th>
            <th id="form_name" name="1" onclick="MakeRequestData('{{route('ordenar-formularios')}}' + '/' + $(this).attr('name'), '#bodyFormTable', true)">NOMBRE &nbsp;  <i class="fas fa-sort"></i></th>
            <th>ACCIONES</th>
        @endslot

        @slot('body')
            @include('main.formularios.data')
        @endslot

    </x-table>
