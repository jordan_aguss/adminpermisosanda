<div id="divOrder">
    
@foreach ($data_req as $dat)
    <tr id="itemForm-{{$dat->form_id}}">
        <td>{{$dat->form_id}}</td>
        <td>{{$dat->form_nombre}}</td>
        <td>
            <button class="col-auto btn btn-outline-warning"
                onclick="MakeRequestData( '{{ route('form-editar') }}', '.modal-content', true, '#modal-principal', 'POST', 2, '', false, false,['Id/{{$dat->form_id}}', 'Total/2','Contenedor/itemForm-{{$dat->form_id}}', 'Accion/3'])">
                <i class="fas fa-edit"></i>
            </button>

            <button class="btn btn-success" onclick="MakeRequestData( '{{ route('formulario-secciones', [$dat->form_id]) }}', '#DivPrincipal', true)">
                <i class="fas fa-code-branch"></i>
            </button>

            <a class="btn btn-info" href="{{route('estados-form', [$dat->form_id])}}">
                <i class="fas fa-shoe-prints"></i>
            </a>

        </td>
    </tr>
@endforeach

</div>
@include('main.home.paginador', ['Datos' => $data_req])
