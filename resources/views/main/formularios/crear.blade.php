<x-card-modal>
    @slot('titulo')
        Información del formulario
    @endslot

    @slot('body')
        <form id="form-general" class="form-horizontal" method="POST">
            @csrf
            <div>
                <div class="row">
                    <input type="hidden" value="{{$Info->form_id}}" name="Id" id="Id"/>
                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Nombre</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" value="{{ $Info->form_nombre }}" placeholder="Nombre del sector..." required autofocus>
                    </div>

                    <div class="mb-3 col-lg-6">
                        <label class="form-label">Estado inicial</label>

                        <select name="estado" id="estado" class="form-control" required>
                            <option value="">--seleccionar--</option>
                            @foreach ($Estados as $item)
                                <option {{$item->esta_id == $Info->esta_id ? 'selected' : ''}} value="{{$item->esta_id}}">{{$item->esta_nombre}}</option>
                            @endforeach
                        </select>
                    </div>

                </div>
            </div>
        </form>
    @endslot

    @slot('footer')
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">
            <i class="fas fa-times"></i> &nbsp;
            Cancelar</button>

        <button type="button" class="btn btn-outline-primary" onclick="MakeRequestData( '{{ route('form-save') }}', '#{{$request->Contenedor}}', true, '#modal-principal', 'POST', {{$request->Accion}}, '#form-general', false, true)">
            <i class="fas fa-save"></i>&nbsp;&nbsp;&nbsp;Guardar
        </button>
    @endslot

</x-card-modal>

