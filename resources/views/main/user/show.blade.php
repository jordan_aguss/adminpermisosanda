@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Usuarios')
@section("scripts")

@endsection

@section('content')
<div class="row">
        <div class="col-12 col-md-4">
            <p><b>Nombre</b> : <br>{{ $row->usua_nombre }}</p>
        </div>
        <div class="col-12 col-md-8">
            <p><b>Email</b> : <br>{{ $row->usua_mail }}</p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4>Roles Asignados</h4>
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Descripción</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($row->roles as $role)
                        @if($row->hasRole($role->id))
                            <tr>
                                <td>{{ $role->name }}</td>
                                <td>{{ $role->description }}</td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
