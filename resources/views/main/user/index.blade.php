@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'USUARIOS')
@section("scripts")

@endsection

@section('content')


<x-table>
    @slot('idTabla')
    tablePermisos
    @endslot

    @slot('titulo')
    Listado de permisos
    @endslot


    @slot('accionBuscar')
        onkeyup="if(event.keyCode == 13) MakeRequestData('{{route('buscar-preguntas')}}' + '/' + $(this).val() , '#tablePermisos', true)"
    @endslot

    @slot('btnAdicionales')

        <a href="{{ route('admin.user.create') }}" title="Crear Usuario" style="padding-bottom:10px!important;"
        class="btn btn-success">
            <i class="fa fa-plus"></i> &nbsp; Agregar
        </a>
    @endslot


    @slot('header')
        <th>Nombre</th>
        <th>Email</th>
        <th>Roles</th>
        <th>Acciones</th>
    @endslot

    @slot('body')
        @foreach($rows as $row)
            <tr>
                <td>{{ $row->usua_nombre }}</td>
                <td>{{ $row->usua_mail }}</td>
                <td>
                    @if($row->roles->isNotEmpty())
                    @foreach($row->roles as $role)
                    {{$role->name}}
                    @endforeach
                    @endif
                </td>
                <td>

                    <a href="{{ route('admin.user.show', $row->usua_id) }}" class="btn btn-primary">Mostrar</a>
                    <a href="{{ route('admin.user.edit', $row->usua_id) }}" class="btn btn-secondary">Editar</a>

                </td>
            </tr>
        @endforeach
    @endslot

</x-table>

@endsection
