@extends('layouts.template')
@section('pretittle', 'Vista general')
@section('tittle', 'Usuarios')
@section("scripts")

@endsection

@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
@if (session()->has('flash'))
<div class="alert alert-info">{{ session('flash')}}</div>
@endif
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

<div class="card-body"> --}}

    <form method="post" action="{{ route('admin.user.update' , $row->usua_id) }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group row">
            <label for="firstname" class="col-md-4 col-form-label text-md-right">{{ __('Nombres') }}</label>

            <div class="col-md-6">
                <input id="firstname" type="text" class="form-control @error('name') is-invalid @enderror" name="nombre" value="{{ $row->usua_nombre }}" required autocomplete="firstname" autofocus>

                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="correo" value="{{ $row->usua_mail }}" required autocomplete="email">

                @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>


        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="pass" autocomplete="new-password">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
        </div>


<div class="form-group row">
    <label for="roles" class="col-md-4 col-form-label text-md-right">Asigne Roles al Usuario</label>
    <div class="col-md-6" style="margin-left: 15px;">
        @foreach ($roles as $role)
        <div>
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" name="roles[]" value="{{$role->id}}" @if($row->hasRole($role->id))
                checked
                @endif
                >{{ $role->name }}
                <em>({{ $role->description ?: 'Sin Descripcion' }})</em>
            </label>
        </div>
        @endforeach
        <div class="col-md-6">
        </div>
        <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
                <button type="submit" class="btn btn-primary">Actualizar </button>
            </div>
        </div>
        </form>
        {{-- </div>
            </div>
        </div>
    </div>
</div> --}}
@endsection
