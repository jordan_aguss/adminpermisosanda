@component('mail::message')
# Ministerio del Medio Ambiente y Recursos Naturales (MARN)
# SISTEMA DE DENUNCIAS AMBIENTALES

Estimado señor(a) denunciante:

Según el análisis técnico legal del caso, su denuncia es competencia de esta Cartera de Estado, para iniciar el procso de investigación se le ha asignado de denuncia: (##-2022).

Nuestro personal realizará las indagaciones previas requeridas, a fin de sustentas los elementos de la denuncia y llevar a cabo una inspección técnica para corroborar lo denunciado, en los casos que sea necesario, a fin de emitir las medidas ambientales o recomendaciones encaminadas a corregir, mitigar o atenuar el daño o impacto ambiental, en el correspondiente informe técnico.

Finalizado el proceso de diligenciamiento de la denuncia, se notificará a las partes involucradas y otras instancias competentes.

Antentamente,

Centro de Denuncias MARN

_Para el seguimiento ingresar a_:

@component('mail::button', ['url' => $url, 'color' => 'success'])
Estado de denuncia
@endcomponent

Si no funciona el botón, ingresar al siguiente enlace: <a href="{{ $url }}">Enlace al sitio web de denuncia</a>.

{{-- Gracias,<br>
{{ config('app.name') }} --}}
@endcomponent
