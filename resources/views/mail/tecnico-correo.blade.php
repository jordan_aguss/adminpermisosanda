@component('mail::message')
# Ministerio del Medio Ambiente y Recursos Naturales (MARN)
# SISTEMA DE DENUNCIAS AMBIENTALES

**Nombre del Técnico**

Estimado Señor(a)

Para su valoración y fines consiguientes de urgente investigación, se le asigna la denuncia ambiental (##-2022), a partir de esta fecha: {{ \Carbon\Carbon::now()->format('d/m/Y') }}

Se le solicita:
1. Investigue la verdad real de la infracción denunciada. De existir certeza sobre los mismos, proceder de acuerdo a lo señalado en la nomrativa ambiental vigente.
2. Indicar y/o supervisar según corresponda: a) medidas cautelares, b) motivo de rechazo de la denuncia, c) recomendación de iniciar proceso administrativo o d) recomendación de medidas preventivas.
3. Dictaminar la forma de proceder con respecto al supuesto ilícito, o en caso de que este dictamen ya haya sido emitido, darle el respectivo seguimiento.
4. Informar a la Dirección General de Gestión Territorial de las actividades realizadas para dar el debido seguimiento a esta denuncia.

Antentamente,

Centro de Denuncias MARN

_Para el seguimiento ingresar a_:

@component('mail::button', ['url' => $url, 'color' => 'success'])
Estado de denuncia
@endcomponent

Si no funciona el botón, ingresar al siguiente enlace: <a href="{{ $url }}">Enlace al sitio web de denuncia</a>.

{{-- Gracias,<br>
{{ config('app.name') }} --}}
@endcomponent
