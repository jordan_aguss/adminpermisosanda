@extends('layouts.template_log')
@section('content')
<div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="container-tight py-6">
      <form class="card card-md" action="{{ route('login') }}" method="POST" autocomplete="off">
        @csrf
        <div class="card-body">
            @if ($errors->any())
                <x-alert tipo="danger" :mensaje="$errors"></x-alert>
            @endif
          <div class="text-center mb-2">
            <a href="."><img src="{{ asset('images/icono2.png') }}" height="auto" alt=""></a>
          </div>
          <h2 class="text-center mt-1">{{ __('ANDA') }}</h2>
          <div class="mb-3">
            <label class="form-label">{{__('Usuario')}}</label>
            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}" required autofocus placeholder="{{__('Digite su usuario')}}">
                {{-- @error('email')
                <div class="invalid-feedback">error</div>
                @enderror --}}
          </div>
          <div class="mb-2">
            <label class="form-label">
                {{__('Contraseña')}}
            </label>
            <div class="input-group input-group-flat">
              <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror"  placeholder="{{__('Contraseña')}}"  autocomplete="off" required
                >
              <span class="input-group-text">
                <a href="#" class="link-secondary" onclick="verPass()" title="{{__('Mostrar contraseña')}}" data-bs-toggle="tooltip"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="2" /><path d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" /></svg></a>
              </span>
              @error('password')
                <div class="invalid-feedback">error</div>
              @enderror
            </div>
          </div>
          <div class="mb-2">
            <label class="form-check d-flex justify-content-between">
              <div>
                <input type="checkbox" class="form-check-input"/>
                <span class="form-check-label">{{__('Recordar')}}</span>
              </div>
              <span class="form-label-description ">
                <a href="#">He olvidado la contraseña</a>
              </span>
            </label>
          </div>
          <div class="form-footer">
            <button type="submit" class="btn w-100" style="background:#41495c; color: #FFF">{{__('Iniciar sesión')}}</button>
          </div>
        </div>
      </form>
    </div>
  </div>

@endsection
<script>
  function verPass(){
      var item = document.getElementById('password');
      if(item.type == 'password'){
          item.type = "text";
      }else{
          item.type = "password";
      }
  }
</script>

<x-modal>
</x-modal>
