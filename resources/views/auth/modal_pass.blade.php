<div class="row">
    <div class="d-flex justify-content-between bg-white">
        <h3><strong>Solicitud cambio de contraseña</strong></h3>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
    </div>
    <hr class="col-12 mt 1 mb-3">
    <form class="row m-0" novalidate id="ForgetPassForm">
        @csrf
        <div class="form-group">
            <label>Ingrese su correo electrónico</label>
            <input type="email" class="form-control" id="Email" name="Email" required/>
        </div>
        <div class="d-flex justify-content-end">
            <button class="btn btn-outline-primary" type="button"
            onclick="saveDataResultView('{{route('olvido-pass.mail')}}',  'POST', 'ForgetPassForm', '', 3, false)"><i class="fas fa-paper-plane"></i> &nbsp; Enviar</button>
            <button type="button" class="btn btn-outline-danger" data-bs-dismiss="modal"><i class="fas fa-times"></i> &nbsp; Cancelar</button>
        </div>
    </form>
</div>