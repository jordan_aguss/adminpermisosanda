<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.header')
</head>
<body class="antialiased theme-light border-top-wide border-primary d-flex flex-column">
    @yield('content')

    @include('layouts.footers')
    <script src="{{asset('js/jquery.mask.js')}}"></script>
</body>
</html>