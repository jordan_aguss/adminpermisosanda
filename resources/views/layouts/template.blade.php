<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.header')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js"
    crossorigin="anonymous"></script>
</head>
<body class="antialiased theme-light">
    <div class="page">
        <!-- AQUI EL NAVBAR -->
        @include('layouts.navbar')
        <!-- FIN NAVBAR -->
        <div class="content">
            <div class="container-fluid">
                <!--  CONTENIDO PRINCIPAL -->
                @include('layouts.tittle')
                <div id="contenido">
                    @yield('content')
                </div>
                <!-- FIN CONTENIDO PRINCIPAL -->
            </div>
            <!-- AQUI EL FOOTER -->
            @include('layouts.footer')
            <!-- FIN FOOTER -->
        </div>
    </div>

    <!-- AQUI MODAL -->
    <div class="modal modal-blur fade" id="modal-principal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            </div>
        </div>
    </div>

    <!-- FIN MODAL -->

    <!--AQUI SCRIPTS -->
    @include('layouts.footers')
    @yield('scriptsPlugins')
    @yield('scripts')
    <!-- FIN SCRIPTS -->
</body>
</html>
