<!-- Page title -->
<div class="page-header d-print-none">
    <div class="row align-items-center">
        <div class="col">
            <!-- Page pre-title -->
            <div class="page-pretitle">@yield('pretittle')</div>
            <h2 class="page-title">@yield('tittle')</h2>
        </div>

        @yield('tittleactions')
        
    </div>
</div>