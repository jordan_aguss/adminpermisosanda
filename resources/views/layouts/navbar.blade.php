<div class="navbar navbar-expand-md navbar-light d-print-none">
  <div class="container-fluid">
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbar-menu">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="navbar-nav flex-row order-md-last">

      <div class="d-flex justify-align-center">
        <a onclick="printHTML()" class="btn"><img src="{{asset('images/printer.png')}}" style="height: 25px;" /></a>
      </div>

      <div class="nav-item dropdown d-none d-md-flex me-3">
        <a href="#" class="nav-link px-0" data-bs-toggle="dropdown" tabindex="-1" aria-label="Show notifications">
          <svg class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
            <path stroke="none" d="M0 0h24v24H0z" fill="none" />
            <path d="M10 5a2 2 0 0 1 4 0a7 7 0 0 1 4 6v3a4 4 0 0 0 2 3h-16a4 4 0 0 0 2 -3v-3a7 7 0 0 1 4 -6" />
            <path d="M9 17v1a3 3 0 0 0 6 0v-1" />
          </svg>
          <span class="badge bg-red"></span>
        </a>
        <div class="dropdown-menu dropdown-menu-end dropdown-menu-card">
          <div class="card">
            <div class="card-body">
              Notificaciones
            </div>
          </div>
        </div>
      </div>
      <div class="nav-item dropdown">
        <a href="#" class="nav-link d-flex lh-1 text-reset p-0" data-bs-toggle="dropdown" aria-label="Open user menu">
          <span class="avatar avatar-sm" style="background-image: url({{ asset('image/logo.svg') }})"></span>
          <div class="d-none d-xl-block ps-2">
            <div>{{ Session::get('nombre_usuario')}}</div>
            <div class="mt-1 small text-muted">{{ Session::get('perfil_nombre')}}</div>
          </div>
        </a>
        <div class="dropdown-menu dropdown-menu-end dropdown-menu-arrow">
          <a href="#" class="dropdown-item">Mi estatus</a>
          <a href="#" class="dropdown-item">Perfil y cuenta</a>
          <a href="#" class="dropdown-item">Feedback</a>
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">Configuración</a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST">
            @csrf
            <button class="dropdown-item" type="submit">Cerrar sesión</button>
          </form>
        </div>
      </div>
    </div>
    <div class="collapse navbar-collapse" id="navbar-menu">
      <div class="d-flex flex-column flex-md-row flex-fill align-items-stretch align-items-md-center">
        <ul class="navbar-nav">

          <a class="nav-link" href="{{route('inicio')}}"><span class="nav-link-title">
              Inicio
            </span>
          </a>


          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle show" href="#navbar-base" data-bs-toggle="dropdown" role="button" aria-expanded="true">
              <span class="nav-link-title">
                Administración
              </span>
            </a>
            <div class="dropdown-menu" data-bs-popper="none">
              <div class="dropdown-menu-columns">
                <div class="dropdown-menu-column">
                  <a class="dropdown-item" href="{{route('permisos.index')}}">Permisos</a>
                  <a class="dropdown-item" href="{{route('preguntas.index')}}">Preguntas</a>
                  <a class="dropdown-item" href="{{route('seccion_formulario')}}">Secciones</a>
                  <a class="dropdown-item" href="{{route('sector')}}">Formularios</a>
                  <a class="dropdown-item" href="{{route('info-general')}}">Información de la institución</a>
                  <a class="dropdown-item" href="{{route('estados')}}">Estados</a>
                </div>
              </div>
            </div>
          </li>
          <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle show" href="#navbar-base" data-bs-toggle="dropdown" role="button" aria-expanded="true">
                <span class="nav-link-title">
                  Usuarios y Roles
                </span>
              </a>
              <div class="dropdown-menu" data-bs-popper="none">
                <div class="dropdown-menu-columns">
                  <div class="dropdown-menu-column">
                    <a class="dropdown-item" href="{{route('admin.user.index')}}">Usuarios</a>
                    <a class="dropdown-item" href="{{route('admin.role.index')}}">Roles</a>
                    <a class="dropdown-item" href="{{route('admin.permission.index')}}">Permisos</a>
                  </div>
                </div>
              </div>
          </li>

        </ul>

      </div>
    </div>
  </div>
</div>
