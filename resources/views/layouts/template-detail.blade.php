<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.header')
</head>
<body class="antialiased theme-light">
    <div class="page">
        <!-- AQUI EL NAVBAR -->
        @include('layouts.navbar')
        <!-- FIN NAVBAR -->
        <div class="content">
            <div class="container-fluid">
                <!--  CONTENIDO PRINCIPAL -->
                <div id="contenido">
                    @yield('content')
                </div>
                <!-- FIN CONTENIDO PRINCIPAL -->
            </div>
            <!-- AQUI EL FOOTER -->
            @include('layouts.footer')
            <!-- FIN FOOTER -->
        </div>
    </div>

    <!-- AQUI MODAL -->

    <!-- FIN MODAL -->

    <!--AQUI SCRIPTS -->
    @include('layouts.footers')
    @yield('scriptsPlugins')
    @yield('scripts')
    <!-- FIN SCRIPTS -->
</body>
</html>
