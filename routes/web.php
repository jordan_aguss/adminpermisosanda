<?php

use App\Http\Controllers\DocumentoController;
use App\Http\Controllers\EstadoController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\OpcionController;
use App\Http\Controllers\PermisoController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\SucursalController;
use App\Http\Controllers\PreguntasController;
use App\Http\Controllers\FormularioController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\seccionFormularioCont;
use App\Http\Controllers\tecnicoController;
use App\Models\Permiso;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['auth']], function(){

    Route::get('/',[HomeController::class, 'index'])->name('inicio');

    Route::prefix('secciones')->group(function () {

        Route::get('ordenar/{palabra?}',[seccionFormularioCont::class, 'OrdenarSecciones'])->name('ordenar-secciones');

        Route::post('modal',[seccionFormularioCont::class, 'ShowModal'])->name('seccion_formulario.modal');
        Route::get('/',[seccionFormularioCont::class, 'index'])->name('seccion_formulario');
        Route::get('datos-de-seccion',[seccionFormularioCont::class, 'TableInfo'])->name('seccion-info-table');
        Route::get('cambiar-pregunta-de-seccion/{tipo}/{seccion?}/{pregunta?}',[seccionFormularioCont::class, 'CambiarPregunta'])->name('cambiar-preg-seccion');
        Route::get('preguntas-de-seccion/{Info}',[seccionFormularioCont::class, 'GetPreguntasSeccion'])->name('seccion-preguntas');

        Route::get('buscar-pregunta-de-la-seccion/{id}/{palabra?}',[seccionFormularioCont::class, 'BuscarPreguntaAdd'])->name('buscar-preg-seccion');
        Route::post('guardar',[seccionFormularioCont::class, 'guardar'])->name('seccion_formulario.guardar');
        Route::get('buscar/{palabra?}',[seccionFormularioCont::class, 'Buscar'])->name('buscar-secciones');

    });

    Route::prefix('preguntas')->group(function () {

        Route::get('buscar/{palabra?}',[PreguntasController::class, 'Buscar'])->name('buscar-preguntas');
        Route::get('/',[PreguntasController::class, 'index'])->name('preguntas.index');
        Route::get('/lista-preguntas',[PreguntasController::class, 'TablesQuestions'])->name('preguntas-table');

        Route::get('/get-preguntas-form/{idForm?}', [PreguntasController::class, 'GetPreguntasForm'])->name('preguntas-form');
        Route::get('/get-preguntas-hijas/{pregunta?}/{opcion?}', [PreguntasController::class, 'GetPreguntasHijas'])->name('get-preguntas-hijas');

        Route::post('/modal-documentos', [DocumentoController::class, 'ModalDocumento'])->name('modal-documentos');
        Route::post('/guardar-documento', [DocumentoController::class, 'SaveDocument'])->name('guardar-documentos');
        Route::get('/descargarDocs/{ruta?}/{permiso?}', [DocumentoController::class, 'descargarAdjunto'])->name('descargarDocs');
        Route::post('/eliminar-documento', [DocumentoController::class, 'DeleteDocument'])->name('delete-doc');

        Route::get('cambiar-subpregunta/{tipo}/{padre?}/{pregunta?}',[seccionFormularioCont::class, 'CambiarPregunta'])->name('cambio-subpreguntas');
        Route::get('buscar-preguntas-free/{id}/{palabra?}',[PreguntasController::class, 'BuscarPreguntaAdd'])->name('buscar-preg-libres');

        Route::get('get-hijas/{pregunta?}',[PreguntasController::class, 'GetChildren'])->name('get-children');
        Route::post('guardar-pregunta-informacion',[PreguntasController::class, 'SavePregunta'])->name('pregunta-save');
        Route::post('guardar-preguntas-de-formulario',[PreguntasController::class, 'SavePreguntasForm'])->name('preguntas-formulario');
        Route::post('formulario-de-preguntas',[PreguntasController::class, 'ModalPregunta'])->name('pregunta-form');
        Route::get('agregar-sub-preguntas/{padre?}/{pregunta?}',[PreguntasController::class, 'AddSubPregunta'])->name('add-subpregunta');
        Route::get('cambiar-preguntas-hijas/{tipo}/{padre?}/{pregunta?}',[PreguntasController::class, 'ChangeSubPregunta'])->name('cambiar-preguntas-hijas');
        Route::post('modal-secciones',[PreguntasController::class, 'ModalSeccionPregunta'])->name('modal-secciones');

        Route::post('actuallizar-orden-de-pregunta',[PreguntasController::class, 'UpdateOrden'])->name('update-carga-pregunta');

        Route::get('ordenar/{palabra?}',[PreguntasController::class, 'OrdenarPreguntas'])->name('ordenar-preguntas');


    });


    Route::prefix('tecnicos')->group(function () {
        Route::post('modal-permisos',[tecnicoController::class, 'ModaPermiso'])->name('modal-permisos');
        Route::post('modal-permisos-denegar',[tecnicoController::class, 'ModaPermisoDenegar'])->name('modal-permisos-denegar');
        Route::post('guardarAsignacion',[tecnicoController::class, 'guardarAsignacion'])->name('guardarAsignacion');
        Route::post('denegarPermiso',[tecnicoController::class, 'denegarPermiso'])->name('denegarPermiso');
        Route::get('eliminarAsignacion/{idTecnico}/{idPermiso}/{estado}',[tecnicoController::class, 'eliminarAsignacion'])->name('eliminarAsignacion');
        Route::get('listado',[tecnicoController::class, 'index'])->name('permisos-tecnico.index');
        Route::post('modal-permisos-informe',[tecnicoController::class, 'ModaPermisoInforme'])->name('modal-permisos-informe');
        Route::post('/vista-formulario', [tecnicoController::class, 'FormPermiso'])->name('FormPermiso');
        Route::post('/guardar-estado-preguntas/{idForm?}', [tecnicoController::class, 'SavePreguntasInforme'])->name('informe-save-preguntas');
        Route::post('/guardar-permiso-informe', [tecnicoController::class, 'GuardarPermisoInforme'])->name('guardar-permiso-informe');
    });

    Route::prefix('permisos')->group(function () {

        Route::get('subida-de-permisos',[PermisoController::class, 'ViewLoadFile'])->name('vload-file');
        Route::get('listado',[PermisoController::class, 'index'])->name('permisos.index');
        Route::get('generar-acta/{id}/{tipo?}',[PermisoController::class, 'GenerarActa'])->name('permisos.acta');
        Route::get('guardar-solvencia/{id}/{estado}/{pregunta?}',[PermisoController::class, 'GuardarSolvencia'])->name('save-solvencia');

        Route::get('paginador-seguimiento',[PermisoController::class, 'PageSeguimiento'])->name('page-seguimiento');
        Route::post('subida-de-archivo',[PermisoController::class, 'LoadFiles'])->name('permisos.load-file');
        Route::get('buscar/{palabra?}',[PermisoController::class, 'Buscar'])->name('buscar-permisos');

        Route::get('ordenar/{palabra?}',[PermisoController::class, 'Ordenar'])->name('ordenar-permisos');


        Route::get('detalle/{id}/{idForm}',[PermisoController::class, 'detalle'])->name('permisos.detalle');
        Route::get('detalle/preguntas/{idForm}/{IdPermiso}/{perf_id}',[PermisoController::class, 'detallePreguntas'])->name('permisos.detalle_preguntas');
        Route::get('permisos/detalle-tab-documentos/{id}',[PermisoController::class, 'tabDocumentos'])->name('permisos.detalle-tab-documentos');
        Route::get('permisos/detalle-tab-formularios/{id}',[PermisoController::class, 'tabFormularios'])->name('permisos.detalle-tab-formularios');
        Route::post('detalle-tab-docs/modal',[PermisoController::class, 'ShowModalE'])->name('tabDocs-modalE');
        Route::post('cambio-estados', [PermisoController::class, 'EstadosSolicitud'])->name('EstadosSolicitud');
        Route::post('modal-estados',[PermisoController::class, 'ModalEstados'])->name('modal-estados');
        Route::post('modal-edit',[PermisoController::class, 'ModalEditar'])->name('modal-edit');

        Route::post('modal-formulario',[PreguntasController::class, 'GetPreguntasForm'])->name('modal-formulario');
        Route::post('modal-formulario-acta',[PreguntasController::class, 'SubForms'])->name('modal-formulario-acta');
        Route::post('guardar',[PermisoController::class, 'GuardarFormulario'])->name('form-save');

        Route::get('seccion-formulario/{id?}',[PermisoController::class, 'SeccionesFormulario'])->name('secciones-formulario');
        Route::get('preguntas-detralle/{id?}',[PermisoController::class, 'PreguntasDetalle'])->name('preguntas-detalle');

        Route::post('modal-solventes',[PermisoController::class, 'ModaPermisoSolventes'])->name('modal-solvencia');


    });



    Route::prefix('opciones')->group(function () {

        Route::get('cambiar-pregunta/{pregunta}/{opcion}/{padre}',[OpcionController::class, 'CambiarOpcion'])->name('change-pregunta-option');
        Route::get('opciones/{pregunta}',[OpcionController::class, 'GetOpciones'])->name('get-opciones');
        Route::post('guardar',[OpcionController::class, 'SaveOpcion'])->name('save-opcion');
        Route::get('formulario/{Info}/{pregunta}',[OpcionController::class, 'FormOpciones'])->name('form-opciones');
        Route::get('eliminar-opcion/{Info}/{pregunta}', [OpcionController::class, 'DeleteOpcion'])->name('eliminar-opcion');
    });

    Route::prefix('estados')->group(function () {

        Route::get('data',[EstadoController::class, 'index'])->name('estados');
        Route::post('modal',[EstadoController::class, 'showModal'])->name('estado.modal');
        Route::post('guardar',[EstadoController::class, 'guardar'])->name('estado.guardar');
        Route::post('form-subestado',[EstadoController::class, 'GetSubEstado'])->name('subestado.form');
        Route::get('buscar/{palabra?}/{vista?}',[EstadoController::class, 'Buscar'])->name('buscar-estado');
        Route::get('estados-formulario/{id?}',[EstadoController::class, 'EstadosForm'])->name('estados-form');
        Route::post('obtener-subestados',[EstadoController::class, 'SubEstados'])->name('get-subestados');
        Route::get('eliminar-subestado/{estado}/{form}/{padre}',[EstadoController::class, 'EliminarEstado'])->name('eliminar-subestado');
        Route::post('buscar-estados',[EstadoController::class, 'SEstadoAsign'])->name('buscar-estados-asign');
        Route::post('modal-asign-estados',[EstadoController::class, 'TableAdd'])->name('modal-asign');
        Route::post('nuevo-estado-de-proceso',[EstadoController::class, 'AsignStatus'])->name('form-asign-status');

    });

    Route::prefix('formularios')->group(function () {

        Route::get('ordenar/{palabra?}',[FormularioController::class, 'OrdenarFormularios'])->name('ordenar-formularios');


        Route::get('buscar-pregunta/{id}/{palabra?}',[FormularioController::class, 'BuscarPreguntaAdd'])->name('buscar-preg-form');
        Route::get('buscar/{palabra?}',[FormularioController::class, 'Buscar'])->name('buscar-formulario');

        Route::get('datos',[FormularioController::class, 'TableInfo'])->name('formulario-info-table');
        Route::get('cambiar-pregunta/{tipo}/{form?}/{seccion?}',[FormularioController::class, 'CambiarSeccion'])->name('cambiar-preg-formulario');

        Route::get('secciones/{Info}',[FormularioController::class, 'GetSectionsForm'])->name('formulario-secciones');
        Route::get('/',[FormularioController::class, 'index'])->name('sector');
        Route::put('sector/{id}',[FormularioController::class, 'actualizar'])->name('sector.actualizar');

        Route::post('editar',[FormularioController::class, 'ShowEditModal'])->name('form-editar');
        Route::post('guardar',[FormularioController::class, 'EditSector'])->name('form-save');

        Route::post('actuallizar-orden-de-seccion',[FormularioController::class, 'UpdateOrden'])->name('update-carga-secciones');

    });

    Route::get('informacion-general/',[SucursalController::class, 'index'])->name('info-general');
    Route::post('sucursal/actualizar-informacion',[SucursalController::class, 'SaveInfo'])->name('sucursal-save');


    Route::prefix('administracion')->name('admin.')->group(function(){

        /**
         *
         * Rutas CRUD Usuarios
         *
         */
        Route::get('usuarios', [UsuarioController::class, 'index'])->name('user.index');
        Route::get('usuarios/create', [UsuarioController::class, 'create'])->name('user.create');
        Route::post('usuarios/store', [UsuarioController::class, 'SaveUser'])->name('user.store');
        // Route::get('usuarios/destroy/{id}', [UsersController::class, 'destroy'])->name('user.destroy');


        Route::get('usuarios/show/{id}', [UsuarioController::class, 'show'])->name('user.show');
        Route::get('usuarios/edit/{id}', [UsuarioController::class, 'edit'])->name('user.edit');
        Route::put('usuarios/update/{id}', [UsuarioController::class, 'update'])->name('user.update');

        /**
         *
         * Rutas CRUD Roles
         *
         */

        Route::resource('roles', RoleController::class)
                ->names('role')
                ->parameters(['permisos'=>'role']);

        /**
         *
         * Rutas CRUD Roles
         *
         */

        Route::resource('permisos', PermissionController::class)
                ->names('permission')
                ->parameters(['permisos' => 'permissions' ])
                ->only(['index','show']);

        /*
            CRUD PARA PRIVILEGIOS
        */


        Route::post('modal-permission', [PermissionController::class, 'ShowModal'])->name('permission-modal');
        Route::post('save-info-permission', [PermissionController::class, 'SaveInfo'])->name('save-permission');
        Route::get('delete-permission/{id}', [PermissionController::class, 'DeleteInfo'])->name('delete-permission');

    });


    Route::get('buscar-pregunta-de-la-seccion/{id}/{palabra?}',[seccionFormularioCont::class, 'BuscarPreguntaAdd'])->name('buscar-preg-seccion');
    Route::post('guardar',[seccionFormularioCont::class, 'guardar'])->name('seccion_formulario.guardar');
    // Route::get('buscar/{palabra?}',[seccionFormularioCont::class, 'Buscar'])->name('buscar-secciones');

});


// Routes for email
Route::get('/email', function() {
    // Mail::to('samuel23mena@gmail.com')->send(new DemoMail());
    // return new TecnicoCorreo();
});


