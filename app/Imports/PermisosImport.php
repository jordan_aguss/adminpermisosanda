<?php

namespace App\Imports;

use App\Models\Permiso;
use App\Models\PermisoDetalle;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;


class PermisosImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {


        $data = array_keys($row);

        $permiso = Permiso::where('perm_codigo', $row[$data[0]])->first();

        return DB::transaction(function() use ($row, $permiso, $data){

            $idultimo = 144;
            $tipoform = 4;

            if(!isset($permiso->perm_id)){

                $permiso = new Permiso();
                $permiso->perm_fecha = date('Y-m-d');
                $permiso->perm_codigo = $row[$data[0]];
                $permiso->form_id = 1;
                $permiso->usua_id = 1;
                $permiso->save();
            }

            $data = array();
            $ultimo = $row['c' . $idultimo];
            unset($row['c' . $idultimo]);

            foreach($row as $key => $value){
                $preg = str_replace('c', '', $key);
                if (is_numeric($preg) && isset($value)) {
                    $data[] = ['perm_id' => $permiso->perm_id, 'secc_id' => 1, 'form_id' => $tipoform, 'preg_id' => $preg, 'perd_respuesta' => (in_array($preg, [3,4,36,38,72,80,96,121])  && is_numeric($value) ? $this->transformDate($value)  : $value) ];
                }
            }

            $array2 = array_chunk($data, 100);

            foreach($array2 as $datos){
                DB::table('permiso_detalle')->insert($datos);
            }

            return new PermisoDetalle([
                'perm_id' => $permiso->perm_id,
                'preg_id' => $idultimo,
                'perd_respuesta' => $ultimo,
                'secc_id' => 1,
                'form_id' => $tipoform
            ]);
        });
    }

    public function transformDate($value, $format = 'Y-m-d')
    {
        try {
            return \Carbon\Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($value));
        } catch (\ErrorException $e) {
            return \Carbon\Carbon::createFromFormat($format, $value);
        }
    }

}
