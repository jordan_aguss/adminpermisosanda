<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeccionFormulario extends Model
{
    use HasFactory;
    protected $table = 'seccion_formulario';
    protected $primaryKey = 'secc_id';
    protected $guarded = [];
    public $timestamps = false;
}
