<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta_Categorizacion extends Model
{
    use HasFactory;
    protected $table = 'pregunta_categorizacion';
    protected $primaryKey = 'PREC_ID';
    public $timestamps = false;
}
