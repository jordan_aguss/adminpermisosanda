<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreguntaAccion extends Model
{
    use HasFactory;
    protected $table = 'pregunta_accion';
    protected $primaryKey = 'preg_id';
    public $timestamps = false;
}
