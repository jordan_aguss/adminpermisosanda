<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Validaciones extends Model
{
    use HasFactory;
    protected $table = "validaciones"; 
    protected $primaryKey = 'vali_id';
    public $timestamps = false;
}

