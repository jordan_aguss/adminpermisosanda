<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreguntaFormulario extends Model
{
    use HasFactory;
    protected $table = 'pregunta_formulario';
    protected $primaryKey = 'preg_id';
    public $timestamps = false;
    public $incrementing = false;
}
