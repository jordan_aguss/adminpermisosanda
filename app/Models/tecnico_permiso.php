<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class tecnico_permiso extends Model
{
    use HasFactory;
    protected $table = "tecnicos_permiso"; 
    protected $primaryKey = 'id_tecnico';
    public $timestamps = false;
}
