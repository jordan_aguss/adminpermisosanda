<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstadoControl extends Model
{
    use HasFactory;
    protected $table = 'estado_control';
    protected $primaryKey = 'estc_id';
    protected $guarded = [];
    public $timestamps = false;
}
