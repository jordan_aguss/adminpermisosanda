<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pregunta_Opcion extends Model
{
    use HasFactory;
    protected $table = 'pregunta_opcion';
    protected $primaryKey = 'PREO_ID';
    public $timestamps = false;  
}
