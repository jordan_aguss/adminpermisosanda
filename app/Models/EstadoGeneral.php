<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EstadoGeneral extends Model
{
    use HasFactory;
    protected $table = "estado_general";
    protected $primaryKey = 'estg_id';
    public $timestamps = false;
}
