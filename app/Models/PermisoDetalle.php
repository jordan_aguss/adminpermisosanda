<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PermisoDetalle extends Model
{
    use HasFactory;
    protected $table = 'permiso_detalle';
    protected $primaryKey = 'perd_id';
    public $timestamps = false;

    protected $fillable = ['perd_respuesta', 'preg_id', 'perf_id', 'secc_id'];
}
