<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Seguimientos extends Model
{
    use HasFactory;
    protected $table = 'seguimientos';
    protected $primaryKey = 'segu_id';
    protected $guarded = [];    
    public $timestamps = false; 
}