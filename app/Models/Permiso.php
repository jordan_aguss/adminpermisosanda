<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Permiso extends Model
{
    use HasFactory;
    protected $table = 'permisos';
    protected $primaryKey = 'perm_id';
    public $timestamps = false;

    public function formularios(){
        return $this->hasOne(Formulario::class, 'form_id','form_id');
    }

    public function usuarios(){
        return $this->hasOne(Usuario::class, 'usua_id','usua_id');
    }

    public function estados(){
        return $this->hasOne(Estados::class, 'esta_id','esta_id');
    }
}
