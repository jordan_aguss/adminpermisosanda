<?php

namespace App\Console\Commands;

use App\Models\Perfiles;
use App\Models\Usuarios;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class Instalador extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sea:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Instalador de datos iniciales del proyecto';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if(!$this->verificar()){
            $rol = $this->crearRolSuperAdmin();
            $usuario =$this->crearUsuarioSuperAdmin();
            $this->info('El rol y usuario se instalaron correctamente!.');
        }else{
            $this->error('No se puede ejecutar el instalador, porque ya hay un rol creado.');
        }
    }

    private function verificar(){
        //$rol = Rol::find(1); //si find no encuentra nada devuelve nulo.
        //return $rol->isNotEmpty();
        return Perfiles::find(1);
    }

    private function crearRolSuperAdmin(){
        $rol = 'Administrador';
        return Perfiles::create([
            'PERF_NOMBRE'=>$rol,
            'PERF_ESTADO'=>1
        ]);
    }

    private function crearUsuarioSuperAdmin(){
        return Usuarios::create([
            'USUA_LOGIN'=>'lozano',
            'USUA_EMAIL'=>'lozano@gmail.com',
            'USUA_PASS'=>Hash::make('12345'),
            'PERF_ID'=> 1
        ]);
    }
}
