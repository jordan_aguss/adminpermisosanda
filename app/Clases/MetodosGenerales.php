<?php


namespace App\Clases;

use Exception;

class MetodosGenerales{

    private static $imgRoot = 'C:/xampp/htdocs/admincore/public/logos/';
    private static $imgRootURL = 'http://localhost/admincore/public/logos/';
    public static $IdSucursal = 1;

    public static function SubirImagen($Imagen, $ImagenCarpeta, $peso = 40000){

        if(($Imagen['size'] == 0) && (!$Imagen["name"] == "")) { throw new Exception('^Debe de seleccionar una imagen'); }

        if($Imagen["type"] != "image/jpg" && $Imagen["type"] != "image/jpeg" && $Imagen["type"] != "image/png"){ throw new Exception('^Tipo de imagen incorrecto (Tipos permitidos: jpg, jpeg, png)'); }

        if($Imagen["size"] > $peso){ throw new Exception('^El tamaño de la imagen no puede ser mayor a 40KB'); }

        $Carpetas = explode('/', $ImagenCarpeta);
        $Carpeta = preg_replace('/[^a-z0-9\.]+/', '', strtolower($Carpetas[count($Carpetas) - 2]));

        $NombreImagen = preg_replace('/[^a-z0-9\.]+/', '', strtolower($Imagen['name']));
        $Ruta = self::$imgRoot . $Carpeta;
        if(!file_exists($Ruta) && !mkdir($Ruta, 0755, true)) throw new Exception('^Ocurrio un error al subir la imagen');

        $Hora = date('YmdHis');

        $Origen = $Imagen['tmp_name'];
        if(!move_uploaded_file($Origen, $Ruta .'/'. $Hora. $NombreImagen)){ throw new Exception('^Ocurrio un error al guardar la imagen'); }
        return self::$imgRootURL . $Carpeta .'/'. $Hora . $NombreImagen;
    }


}



