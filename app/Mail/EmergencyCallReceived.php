<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class EmergencyCallReceived extends Mailable
{

    use Queueable, SerializesModels;
    protected $permisoDetalle, $preguntaString, $fecha;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombreUser, $nameProyecto, $fecha, $tipoSolicitud)
    
    {
        $this->nombreUser = $nombreUser;
        $this->nameProyecto = $nameProyecto;
        $this->fecha = $fecha;
        $this->tipoSolicitud = $tipoSolicitud;
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      
        return $this->view('main.permisos.tecnicos.plantilla-correo')
                    ->with([
                            "nombreUser" => $this->nombreUser,
                            "nameProyecto" => $this->nameProyecto,
                            "fecha" => $this->fecha,
                            "tipoSolicitud" => $this->tipoSolicitud,
                            ]);
    }
}
