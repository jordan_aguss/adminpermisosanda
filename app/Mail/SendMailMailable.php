<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailMailable extends Mailable
{
    use Queueable, SerializesModels;

    public $subject;
    public $Info;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($subject, $info)
    {
        $this->subject = $subject;
        $this->Info = $info;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('main.mails.index');
    }
}
