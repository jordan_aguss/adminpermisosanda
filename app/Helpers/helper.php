<?php

use App\Models\PreguntaOpcion;
use Illuminate\Support\Facades\Session;

if(!function_exists('isAdmin')){
    function isAdmin($url){
        //return in_array($url, Session::get('urls'));
        return Session::get('perfil_usuario') == 'Administrador';
    }
}


if(!function_exists('Prueba')){
    function Prueba(){
        return 'clase';
    }
}

if(!function_exists('OpcionesSelect')){
    function OpcionesSelect($selecciones){

        if (count($selecciones) > 0) {
            return PreguntaOpcion::whereIn('preg_id', $selecciones)
                ->join('opciones as o', 'o.opci_id', 'pregunta_opcion.opci_id')->get();
        }

        return [];
    }
}
