<?php

namespace App\Custom;

class PreguntaCustom 
{
    public $idPregunta = 0;
    public $titulo = '';
    public $tipoEntrada = '';
    public $longitud = '';
    public $idDepende = '';
    public $categorizacion = '';
    public $orden = '';
    public $ayuda = '';
    public $activa = 0;
    public $codigo = '';
    public $nombreTipoEntrada = '';
    public $hijos = 0;
    public $idSeccion = 0;
    public $idSector = 0;

    public $subCategoria = [];
 
}
