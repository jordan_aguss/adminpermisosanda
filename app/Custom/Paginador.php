<?php

namespace App\Custom;

    class Paginador {
        private $TotalRegistros = 0;
        private $FilasPagina = 0;
        private $PaginaActual = 0;
        private $Desplazamiento = 0;
        private $TotalPaginas = 0;
        
        public function __construct($TotalRegistros, $FilasPagina, $PaginaActual) {
            $this->TotalRegistros = $TotalRegistros;
            $this->FilasPagina = $FilasPagina;
            $this->changeActualPage($PaginaActual);
            $this->calcularDesplazamiento();
            $this->calcularTotalPaginas();
        }
        
        private function calcularDesplazamiento(){
            $this->Desplazamiento = ($this->PaginaActual - 1) * $this->FilasPagina;
        }
        
        private function calcularTotalPaginas(){
            $this->TotalPaginas = ceil($this->TotalRegistros / $this->FilasPagina);
        }
        
        public function getTotalRegistros(){
            return $this->TotalRegistros;
        }
        
        public function getFilasPagina(){
            return $this->FilasPagina;
        }
        
        public function getPaginaActual(){
            return $this->PaginaActual;
        }
        
        public function getDesplazamiento(){
            return $this->Desplazamiento;
        }
        
        public function getTotalPaginas(){
            return $this->TotalPaginas;
        }
        
        public function changeActualPage($Valor){
            if(is_numeric($Valor) && $Valor > 0){
                $this->PaginaActual = $Valor;
            }else{
                $this->PaginaActual = 1;
            }
        }
    }