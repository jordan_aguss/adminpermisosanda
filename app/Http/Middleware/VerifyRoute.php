<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class VerifyRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, $ruta)
    {
        if( !in_array($ruta, Session::get('urls'))){
            return redirect(route(Session::get('url_start')));
        }
        
        return $next($request);
    }
}
