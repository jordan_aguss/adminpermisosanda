<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    const PERMISSIONS = [
        'create'    => 'admin-role-create',
        'show'      => 'admin-role-show',
        'edit'      => 'admin-role-edit',
        'delete'    => 'admin-role-delete',
        'asignar'   => 'gerente-asignar',
        'visualizar'=> 'gerente-visualizar',
        'registrar' => 'tecnico-centro-registrar',
        'remitir'   => 'tecnico-centro-remitir',
        'finalizar' => 'tecnico-centro-finalizar',
        'actualizar'=> 'tecnico-general-actualizar',
        'documentos'=> 'tecnico-general-documentos'
    ];

    public function __construct(){
       
        $this->middleware('auth');
        
        $this->middleware('permission:'.self::PERMISSIONS['create'])->only(['create','store']);
        $this->middleware('permission:'.self::PERMISSIONS['show'])->only(['index','show']);
        $this->middleware('permission:'.self::PERMISSIONS['edit'])->only(['edit','update']);
        $this->middleware('permission:'.self::PERMISSIONS['delete'])->only(['destroy']);
    } 

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rows = Role::orderby('name')->paginate(20);

        return view('main.role.index', [
            'rows' => $rows,
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('main.role.create', [
            'row' => new Role(),
            'permissions' => Permission::all(),
        ]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        // dd($request->all());

        $row = new Role(array(
            "name"          => $request->name,
            "guard_name"    => "web"
        ));

        $row->save();

        $row->permissions()->sync($request->permission);

        return redirect()->route('admin.role.show', $row->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
        return view('main.role.show', [
            'row' => $role->load('permissions')
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('main.role.edit', [
            'row' => $role,
            'permissions' => Permission::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
        $role->update($request->all());

        $role->permissions()->sync($request->permission);

        return redirect()->route('admin.role.show', $role->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        $role->delete();

        return redirect()->route('admin.role.index');

    }
}
