<?php

namespace App\Http\Controllers;

use App\Models\Formulario;
use Illuminate\Http\Request;
use App\Models\Opcion;
use App\Models\Permiso;
use App\Models\PermisoDetalle;
use App\Models\PermisoFormulario;
use App\Models\Pregunta;
use App\Models\PreguntaAccion;
use App\Models\PreguntaOpcion;
use App\Models\PreguntaSeccion;
use App\Models\TipoEntrada;
use App\Models\Validaciones;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx\Rels;

class PreguntasController extends Controller
{
    //

    public function index($view = 'index'){

        $Preguntas = Pregunta::join('tipos_entrada as te', 'te.tipe_id', 'preguntas.tipe_id')->select('preguntas.*', 'tipe_nombre')->orderBy('preg_id')->paginate(50)->withPath(route('buscar-preguntas'));
        return view('main.preguntas.' . $view, compact('Preguntas'));
    }

    public function Buscar($palabra = ''){
        $Preguntas = Pregunta::join('tipos_entrada as te', 'te.tipe_id', 'preguntas.tipe_id')->select('preguntas.*', 'tipe_nombre')->where('preg_nombre', 'like', "%$palabra%")->orderBy('preg_id')->paginate(50)->withPath(route('buscar-preguntas'));
        return view('main.preguntas.data', compact('Preguntas'));
    }

    public function ModalSeccionPregunta(Request $request){

        $users = DB::table('pregunta_seccion')
        ->Join('secciones', 'pregunta_seccion.secc_id', 'secciones.secc_id')
        ->where('pregunta_seccion.preg_id',$request->Id)
        ->get();

        return view('main.preguntas.preguntas-seccion', compact('users'));
    }


    public function TablesQuestions(){
        return $this->index('info-table');
    }

    // METODOS PARA LA ASIGNACION DE PREGUNTAS HIJAS
    public function GetChildren($Info){

        $Info = Pregunta::where('preg_id' , $Info)->select('preg_id as id', 'preg_nombre as nombre')->first();

        $DataAdd = Pregunta::join('pregunta_accion as pa', 'pa.pred_id', 'preguntas.preg_id')
        ->where('pa.preg_id', $Info->id)->where('preg_padre', 0)->select('pa.preg_id as pred_id', 'pred_id as id', 'preg_nombre as nombre')->paginate(50);

        $DataFree = $this->PreguntasFree($Info->id);

        $ruta = 'cambiar-preguntas-hijas';
        $rutaDetalle = 'opciones-preguntas';
        $rutaBack = 'preguntas-table';
        $rutaBuscar = route('buscar-preg-libres', [$Info->id]);

       $Opciones = PreguntaOpcion::join('opciones as o', 'o.opci_id', 'pregunta_opcion.opci_id')->where('preg_id', $Info->id)->get();


        return view('main.preguntas.preguntas-asign', compact('DataAdd', 'DataFree', 'Info', 'ruta', 'rutaBack', 'rutaBuscar', 'rutaDetalle', 'Opciones'));
    }


    public function SaveOpcion(Request $request){

        if($request->id == 0){
            $Info = Opcion::where('opci_nombre', $request->Nombre)->first();
            if(!isset($Info)){
                $Info = new Opcion();
            }
        }else{
            $Info = Opcion::find($request->id);
        }

        $Info->opci_nombre = $request->Nombre;
        $Info->save();

        if($request->id == 0){
            DB::insert('insert into pregunta_opcion values(?,?)', [$Info->opci_id, $request->pregunta]);
        }
    }

    public function BuscarPreguntaAdd($id, $palabra = ''){
        $DataFree = $this->PreguntasFree($id, $palabra);
        $pager = 'paginador';
        return view('main.preguntas.result-asing', compact('DataFree', 'pager'));
    }

    private function PreguntasFree($id, $palabra = ''){
        return Pregunta::whereNotIn('preg_id', PreguntaAccion::where('preg_id', $id)->select('pred_id'))->where('preg_nombre', 'like', "%$palabra%")->where('preg_padre', 0)
        ->select('preg_nombre as nombre', 'preg_id as id')->paginate(50)->withPath(route('buscar-preg-seccion', [$id]));
    }


    public function ModalPregunta(Request $request){
        $Entradas = TipoEntrada::all();
        $Validaciones = Validaciones::all();

        if($request->Id == 0){
            $Info = new Pregunta();
        }else{
            $Info = Pregunta::find($request->Id);
        }
        return view('main.preguntas.formulario', compact('Info', 'Entradas', 'request', 'Validaciones'));
    }


    public function SavePregunta(Request $request){

        if($request->id == 0){
            $Pregunta = Pregunta::where('preg_nombre', $request->nombre)->where('tipe_id', $request->tipo)
            ->where('vali_id', $request->tipoV)->first();
            if(!isset($Pregunta)){
                $Pregunta = new Pregunta();
            }
        }else{
            $Pregunta = Pregunta::find($request->id);
        }

        $Pregunta->preg_nombre = $request->nombre;
        $Pregunta->tipe_id = $request->tipo;
        $Pregunta->preg_ayuda = $request->ayuda;
        $Pregunta->preg_padre = $request->padre;
        $Pregunta->preg_campos = isset($request->campos) ? $request->campos : '' ;
        $Pregunta->vali_id = $request->tipoV;
        $Pregunta->save();
        $Pregunta->tipe_nombre = TipoEntrada::find($request->tipo)->tipe_nombre;
        $Pregunta->vali_id = Validaciones::find($request->tipoV)->vali_nombre;

        return view('main.preguntas.data', ['Preguntas' => [$Pregunta]]);
    }


    public function AddSubPregunta($padre, $pregunta){
        if($padre == $pregunta) return "";

        DB::insert('insert into pregunta_accion values(?,?,?)', [$padre, 1, $pregunta]);

        $Opciones = PreguntaOpcion::join('opciones as o', 'o.opci_id', 'pregunta_opcion.opci_id')->where('pregunta_opcion.preg_id', $padre)
        ->leftjoin('pregunta_accion as pa', 'pa.opci_id', 'o.opci_id')->select('o.*', DB::raw('count(*) as preguntas'))->groupBy('o.opci_id')->get();

        $Preguntas = Pregunta::join('pregunta_accion as pa', 'pa.pred_id', 'preguntas.preg_id')
        ->where('pa.preg_id', $padre)->where('pa.pred_id', $pregunta)->get();

        return view('main.preguntas.children', ['Opciones' => $Opciones, 'Preguntas' => $Preguntas, 'pregunta' => $padre]);
    }

    public function ChangeSubPregunta( $tipo ,$padre, $pregunta){

        if($tipo){
            DB::delete('delete from pregunta_accion where preg_id = ? and pred_id = ?', [$padre, $pregunta]);
            return;
        }

        DB::insert('insert into pregunta_accion (preg_id, opci_id, pred_id) values(?,?,?)', [$padre, 1, $pregunta]);
    }

    public function UpdateOrden(Request $request){

        $info = PreguntaSeccion::where('preg_id',$request->id)->where('secc_id',$request->padre)->first();
        if($request->inicio > $request->fin){
            //arrriba
            $signo = '+';
            $request->total = $request->fin;
            $info->preg_orden = $request->fin;
            $request->fin = $request->inicio - 1;
        }else{
            //abajo
            $signo = '-';
            $request->total = $request->inicio + 1;
            $info->preg_orden = $request->fin;
            $request->fin = $request->fin;

        }

        DB::update('update pregunta_seccion set preg_orden = preg_orden ' . $signo . ' 1 where preg_orden between ? and ?  and preg_id != ? and secc_id = ?',
        [$request->total, $request->fin, $request->id, $request->padre]);

        $info->save();
    }


    public function GetPreguntasForm(Request $request){

        $Info = Formulario::find($request->form);
        $tipo = 1;

        if($request->tipo != 7){
            $info = PermisoFormulario::where('perm_id', $request->Id)->where('form_id', $request->form)->first();
            if(isset($info->perf_id)){
                $tipo = 2;
            }
        }

        Session::put('nuevo-permiso', $request->Id);

        if($tipo == 1){

            $Preguntas = DB::select("select p.preg_id, preg_nombre, sf.secc_id, PREG_AYUDA, tipe_id, vali_clase, if(isnull(pred_id), 0, 1) as DEPENDE, (select perd_respuesta from permiso_detalle pd
            inner join preguntas pr on pr.preg_id = pd.preg_id inner join permisos_formulario pf on pf.perf_id =
            pd.perf_id and pf.perm_id = ? where pr.preg_nombre like concat('%', SUBSTRING_INDEX(p.preg_nombre,'(', 1), '%')
            limit 1) as perd_respuesta from preguntas p inner join pregunta_seccion ps on ps.preg_id = p.preg_id inner join
            seccion_formulario sf on sf.secc_id = ps.secc_id  left join validaciones v on v.vali_id = p.vali_id left join
            pregunta_accion pa on pa.preg_id = p.preg_id where form_id = ? order by secf_orden, preg_orden", [$request->Id , $request->form]);

        }else{
            $Preguntas = $this->InfoPreguntas('s.secc_id', [], 'pa.preg_id', $request->form)->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
            ->where('preg_padre', 1)->where('sf.form_id', $request->form)->orderBy('secf_orden')->orderBy('preg_orden')->get();
        }

        return view('main.preguntas.form-preguntas', compact('Preguntas', 'Info', 'request'));
    }

    public function SubForms(Request $request){

        $Info = Formulario::find($request->form);
        Session::put('nuevo-permiso', $request->Id);
        $Preguntas = $this->InfoPreguntas('s.secc_id', [], 'pa.preg_id', $request->form)->join('seccion_formulario as sf', 'sf.secc_id', 's.secc_id')
        ->where('preg_padre', 1)->where('sf.form_id', $request->form)->orderBy('secf_orden')->orderBy('preg_orden')->get();

        return view('main.preguntas.subform', compact('Preguntas', 'Info', 'request'));
    }


    private function InfoPreguntas($seccion = '', $select = [], $idpreg = 'pa.preg_id', $form_id = 7){

        $info = array_merge(['p.*', DB::raw('if(isnull(pred_id), 0, 1) as DEPENDE'), 's.secc_id', 's.secc_nombre', 'perd_respuesta', 'vl.vali_clase'], $select);

        return PreguntaSeccion::join('preguntas as p', 'p.preg_id', 'pregunta_seccion.preg_id')
        ->join('secciones as s', 's.secc_id', 'pregunta_seccion.secc_id')
        ->leftjoin('validaciones as vl', 'vl.vali_id', 'p.vali_id')
        ->leftjoin('pregunta_accion as pa', $idpreg, 'p.preg_id')
        ->leftjoin('permisos_formulario as pf', function($query) use ($form_id){
            $query->where('pf.perm_id', Session::get('nuevo-permiso'));
            $query->where('pf.form_id', $form_id);
        })
        ->leftjoin('permiso_detalle as pd', function($join) use ($seccion){
            $join->on('pd.preg_id', 'p.preg_id');
            $join->on('pd.secc_id', $seccion);
            $join->on('pd.perf_id', 'pf.perf_id');
        })
        ->distinct()->select($info)->orderBy('secf_orden')->orderBy('preg_orden');
    }

    public function GetPreguntasHijas($pregunta, $opcion){

        $Preguntas = Pregunta::join('pregunta_accion as pa', 'pa.pred_id', 'preguntas.preg_id')->where('pa.preg_id', $pregunta)
        ->where('pa.opci_id', $opcion)->leftjoin('valiciones')->get('p.*', '0 as secc_id');
        $hijas = true;
        return view('preguntas.form-preguntas', compact('Preguntas', 'hijas'));
    }

    public function SavePreguntasForm(Request $request){

        try {

            if($request->tipo == 7){
                $Info = new PermisoFormulario();
                $Info->form_id = $request->form;
                $Info->perm_id = $request->permiso;
                $Info->usua_id = Session::get('user-info')->usua_id;
                $Info->perf_fecha = date('Y-m-d H:i:s');
                $Info->save();
            }else{
                $Info = PermisoFormulario::where('perm_id', $request->permiso)->where('form_id', $request->form)->first();
                if(!isset($Info->perf_id)){
                    $Info = new PermisoFormulario();
                    $Info->form_id = $request->form;
                    $Info->perm_id = $request->permiso;
                    $Info->usua_id = Session::get('user-info')->usua_id;
                    $Info->perf_fecha = date('Y-m-d H:i:s');
                    $Info->save();
                }
            }

            foreach($request->campoPregunta as $seccion => $preguntas){
                foreach($preguntas as $preg_id => $valor){
                    $this->SaveInfoPermiso($preg_id, $request, $valor, $seccion, $Info);
                }
            }

            if(isset($request->campoPreguntaFile)){
                foreach($request->campoPreguntaFile as $seccion => $preguntas ){
                    foreach($preguntas as $id => $valor){
                        $ext = $preguntas[$id]->getClientOriginalExtension();
                        $identificador = Session::get('nuevo-permiso').'_'.$id.'.'.$ext;
                        $valor = $identificador;
                        $preguntas[$id]->move(storage_path('uploads'), $identificador);

                        $this->SaveInfoPermiso($id, $request, $valor, $seccion, $Info);
                    }
                }
            }

        } catch (\Throwable $th) { }

        $formularios = Formulario::where('form_id', $request->form)->select('form_id', 'form_nombre', DB::raw( $Info->perf_id . ' as perf_id'))->get();

        $permiso = new Permiso();
        $permiso->perm_id = $request->permiso;
        return view('main.permisos.detalle-tab.item-form', compact('formularios', 'permiso'));
    }


    private function SaveInfoPermiso($id, $request, $valor, $seccion, $Info){

        $Data = PermisoDetalle::where('preg_id', $id)
        ->where('secc_id', $seccion)->where('perf_id', $Info->perf_id)->first();

        if(!isset($Data->perd_id)){
            $Data = new PermisoDetalle();
            $Data->secc_id = $seccion;
            $Data->perf_id = $Info->perf_id;
            $Data->preg_id = $id;
        }

        $Data->perd_respuesta = $valor;
        $Data->save();

    }

    public function OrdenarPreguntas($palabra){

        if(Session::get('order') == 'asc'){
            Session::put('order', 'desc');
            $orden = Session::get('order');
        }else{
            Session::put('order', 'asc');
            $orden = Session::get('order');
        }

       $asi = array(0=>'preg_id', 1=>'preg_nombre', 2=>'tipe_nombre');

                $Preguntas = Pregunta::join('tipos_entrada as te', 'te.tipe_id', 'preguntas.tipe_id')
                ->orderBy($asi[$palabra], $orden)
                ->paginate(50)
                ->withPath(route('buscar-preguntas'));

                 return view('main.preguntas.data', compact('Preguntas'));
        }
}

