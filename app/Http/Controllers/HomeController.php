<?php

namespace App\Http\Controllers;

use App\Models\Permiso;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

        $totalPermisos = Permiso::all()->count();
        $totalProyectos = Permiso::join('permisos_formulario as pf', 'pf.perm_id', 'permisos.perm_id')
                            ->join('permiso_detalle','pf.perf_id', 'permiso_detalle.perf_id')
                                ->where('preg_id', 246)->where('perd_respuesta', 43)->count();
        $totalComunidades = Permiso::join('permisos_formulario as pf', 'pf.perm_id', 'permisos.perm_id')
                            ->join('permiso_detalle','pf.perf_id', 'permiso_detalle.perf_id')
                                ->where('preg_id', 246)->where('perd_respuesta', 23)->count();

        return view('main.home.index', compact('totalComunidades', 'totalProyectos', 'totalPermisos'));
    }

    public function Login(){
        return view('main.home.login');
    }
}
