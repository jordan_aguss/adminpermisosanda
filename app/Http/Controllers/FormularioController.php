<?php

namespace App\Http\Controllers;

use App\Clases\MetodosGenerales;
use App\Http\Requests\ValidarSectores;
use App\Models\Estado;
use App\Models\Formulario;
use App\Models\Pregunta;
use App\Models\PreguntaFormulario;
use App\Models\Seccion;
use App\Models\SeccionFormulario;
use App\Models\Permiso;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormularioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_req = Formulario::where('sucu_id', MetodosGenerales::$IdSucursal)->paginate(50)->withPath(route('buscar-formulario'));
        return view('main.formularios.index', compact('data_req'));
    }

    public function GetSectionsForm($Info){

        $Info = Formulario::where('form_id', $Info)->select('form_id as id', 'form_nombre as nombre')->first();

        $DataAdd = Seccion::where('form_id', $Info->id)->join('seccion_formulario as sf', 'sf.secc_id', 'secciones.secc_id')
        ->select('secciones.secc_id as id', 'secc_nombre as nombre')->orderBy('secf_orden')->paginate(50);
        $DataFree = $this->SectionsFree($Info->id);

        $ruta = 'cambiar-preg-formulario';
        $rutaBack = 'formulario-info-table';
        $rutaBuscar = route('buscar-preg-form', [$Info->id]);
        $rutaOrden = route('update-carga-secciones');

        return view('main.preguntas.preguntas-asign', compact('DataAdd', 'DataFree', 'Info', 'ruta', 'rutaBack', 'rutaBuscar', 'rutaOrden'));
    }

    private function SectionsFree($id, $palabra = ''){
        return Seccion::whereNotIn('secc_id', SeccionFormulario::where('form_id', $id)->select('secc_id'))->where('secc_nombre', 'like', "%$palabra%")->select('secciones.secc_id as id', 'secc_nombre as nombre')->paginate(50)->withPath(route('buscar-preg-form', [$id]));
    }

    public function Buscar($palabra = ''){

        $data_req = Formulario::where('sucu_id', MetodosGenerales::$IdSucursal)->where('form_nombre', 'like', "%$palabra%")->paginate(50)->withPath(route('buscar-formulario'));
        return view('main.formularios.data', compact('data_req'));
    }

    public function BuscarPreguntaAdd($id, $palabra = ''){
        $DataFree = $this->SectionsFree($id, $palabra);
        return view('main.preguntas.result-asing', compact('DataFree'));
    }


    public function TableInfo(){
        $data_req = Formulario::where('sucu_id', MetodosGenerales::$IdSucursal)->paginate(50);
        return view('main.formularios.info-table', compact('data_req'));
    }

    public function CambiarSeccion($tipo, $form, $seccion){

        if($tipo){
            SeccionFormulario::where('secc_id', $seccion)->where('form_id', $form)->delete();
        }else{
            $ultimo = DB::select('select count(*) as Orden from seccion_formulario where form_id = ?', [$form])[0]->Orden;
            DB::insert('insert into seccion_formulario(secc_id, form_id, secf_orden) values(?,?,?)',
             [$seccion, $form, $ultimo]);
        }
    }

    public function UpdateOrden(Request $request){

        $info = SeccionFormulario::where('secc_id',$request->id)->where('form_id',$request->padre)->first();
        if($request->inicio > $request->fin){
            //arrriba
            $signo = '+';
            $request->total = $request->fin;
            $info->secf_orden = $request->fin;
            $request->fin = $request->inicio - 1;
        }else{
            //abajo
            $signo = '-';
            $request->total = $request->inicio + 1;
            $info->secf_orden = $request->fin;
            $request->fin = $request->fin;

        }

        DB::update('update seccion_formulario set secf_orden = secf_orden ' . $signo . ' 1 where secf_orden between ? and ?  and secc_id != ? and form_id = ?',
        [$request->total, $request->fin, $request->id, $request->padre]);

        $info->save();
    }


    public function ShowEditModal(Request $request)
    {
        if(!$request->ajax()){
            abort(404);
            return;
        }
        if ($request->Id > 0) {
            $Info = Formulario::find($request->Id);
        }else{
            $Info = new Formulario();
        }

        $Estados = Estado::orderBy('esta_nombre')->get();

        return view('main.formularios.crear', compact('Info','request', 'Estados'));
    }

    public function EditSector(Request $request)
    {
        if($request->Id == 0){
            $dat = new Formulario();
        }else{
            $dat = Formulario::find($request->Id);
        }

        $dat->form_nombre = $request->nombre;
        $dat->esta_id = $request->estado;
        $dat->sucu_id = MetodosGenerales::$IdSucursal;
        $dat->save();

        return view('main.formularios.data', ['data_req' => [$dat]]);
    }

    public function buscador(Request $request)
    {
        if($request->ajax()){
            $query = trim($request->get('buscar'));
            $data_req = Formulario::where('form_nombre', 'LIKE','%'.$query.'%')
            ->orderBy('form_id', 'desc')
            ->paginate(100);
            return view('main.sectores.data-search', compact('data_req'));
        }else{
            abort(404);
        }
    }

    public function OrdenarFormularios($palabra){
        Log::info($palabra);

        if(Session::get('order') == 'asc'){
            Session::put('order', 'desc');
            $orden = Session::get('order');
        }else{
            Session::put('order', 'asc');
            $orden = Session::get('order');
        }

       $asi = array(0=>'form_id', 1=>'form_nombre');
   
                $data_req = Formulario::where('form_id', '>=', 0)
                ->orderBy($asi[$palabra], $orden )
                ->paginate(10)
                ->withPath(route('buscar-formulario'));   
                
                 return view('main.formularios.data', compact('data_req'));
        }

}
