<?php

namespace App\Http\Controllers;

use App\Clases\MetodosGenerales;
use App\Models\Sucursal;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    public function index(){
        $Info = Sucursal::find(1);
        return view('main.sucursal.index', compact('Info'));
    }

    public function SaveInfo(Request $request){

        if(($_FILES["Newlogo"]['size'] > 0) && ($_FILES["Newlogo"]["name"] != "")){
            $request->logo = MetodosGenerales::SubirImagen($_FILES["Newlogo"], 'imagen/');
        }

        $Info = Sucursal::find($request->id);
        $Info->sucu_nombre = $request->nombre;
        $Info->sucu_direccion = $request->direccion;
        $Info->sucu_telefono = $request->telefono;
        $Info->sucu_correo = $request->correo;
        $Info->sucu_facebook = $request->facebook;
        $Info->sucu_twitter = $request->twitter;
        $Info->sucu_youtube = $request->youtube;
        $Info->sucu_color = $request->color;
        $Info->sucu_logo = $request->logo;
        $Info->save();
    }

}
