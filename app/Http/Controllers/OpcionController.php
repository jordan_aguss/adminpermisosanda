<?php

namespace App\Http\Controllers;

use App\Models\Opcion;
use App\Models\PreguntaOpcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OpcionController extends Controller
{

    public function CambiarOpcion($pregunta, $opcion, $padre){
        DB::update('update pregunta_accion set opci_id = ? where preg_id = ? and pred_id = ?', [$opcion, $padre, $pregunta]);
    }

    public function GetOpciones($pregunta){

        $Opciones = PreguntaOpcion::join('opciones as o', 'o.opci_id', 'pregunta_opcion.opci_id')->where('pregunta_opcion.preg_id', $pregunta)
        ->leftjoin('pregunta_accion as pa', 'pa.opci_id', 'o.opci_id')->select('o.*', DB::raw('count(*) as preguntas'))->groupBy('o.opci_id')->get();
        return view('main.preguntas-opcion.opciones', compact('Opciones', 'pregunta'));
    }


    public function DeleteOpcion($Info, $pregunta){
        //Aqui hare update a la relacion
        DB::table('pregunta_opcion')
            ->where('preg_id', $pregunta)
            ->where('opci_id', $Info)
            ->delete();

        return $this->GetOpciones($pregunta);

    }

    public function FormOpciones($Info, $pregunta){

        if($Info == 0){
            $Info = new Opcion();
        }else{
            $Info = Opcion::find($Info);
        }

        return view('main.preguntas-opcion.formulario', compact('Info', 'pregunta'));
    }

    public function SaveOpcion(Request $request){

        if($request->id == 0){
            $Info = Opcion::where('opci_nombre', $request->Nombre)->first();
            if(!isset($Info)){
                $Info = new Opcion();
            }
        }else{
            $Info = Opcion::find($request->id);
        }

        $Info->opci_nombre = $request->Nombre;
        $Info->save();

        if($request->id == 0){
            DB::insert('insert into pregunta_opcion values(?,?)', [$Info->opci_id, $request->pregunta]);
        }

        return $this->GetOpciones($request->pregunta);
    }

}
