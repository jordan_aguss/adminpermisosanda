<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\tecnico;
use App\Models\tecnico_permiso;
use App\Models\Usuario;
use App\Models\Permiso;
use App\Models\Sucursal;
use App\Models\Formulario;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\Models\PermisoDetalle;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use App\Models\Pregunta;
use Mail;
use App\Mail\EmergencyCallReceived;
use App\Mail\NotificacionDenegacion;
use App\Mail\TecnicoCorreo;
use Exception;
use Illuminate\Support\Facades\Log;

class tecnicoController extends Controller
{

    public function SavePreguntasInforme(Request $request, $idForm){
        Log::info($request);
       Log::info($idForm);
        foreach($request->campoPregunta as $id => $valor){
            DB::insert('insert into formulario_visita_campo (form_id, perm_id, perm_respuesta) values(?,?,?)', [$idForm, '', $valor]);
        }

        if(isset($request->campoPreguntaFile)){
            foreach($request->campoPreguntaFile as $id => $valor){
                $ext = $request->campoPreguntaFile[$id]->getClientOriginalExtension();
                $identificador = 'ejemplo'.'_'.$id.'.'.$ext;
                $valor = $identificador;
                $request->campoPreguntaFile[$id]->move(storage_path('uploads'), $identificador);
                DB::insert('insert into formulario_visita_campo (form_id, perm_id, perm_respuesta) values(?,?,?)', [$idForm, '', $valor]);
            }
        }

    }

    public function ModaPermiso(Request $request){

        $Permiso = Permiso::find($request->Id);
        $data = Usuario::permission('tecnico')->get();

        $tecnicos = Usuario::join('tecnicos_permiso as tp', 'tp.tecn_id', 'usua_id')
        ->where('perm_id', $request->Id)->where('tp.esta_id', $Permiso->esta_id)->get();

        return view('main.permisos.modal-permisos', compact('data','tecnicos','request'));
    }

    public function ModaPermisoDenegar(Request $request){

      $permiso = Permiso::where('perm_id', $request->Id)->orderBy('perm_id', 'desc')->get();
      $permisoDetalle =  DB::select('select preg_nombre, tipe_id, if(TIPE_ID = 2 OR TIPE_ID = 14, (select opci_nombre from opciones where opci_id = pd.perd_respuesta), perd_respuesta) as perd_respuesta from preguntas p inner join pregunta_seccion ps on  ps.preg_id = p.preg_id inner join seccion_formulario
      sf on sf.secc_id = ps.secc_id inner join permiso_detalle pd on pd.preg_id = p.preg_id inner join permisos_formulario pf on pf.perf_id =
      pd.perf_id and pf.perm_id = ? where sf.form_id = ?', [$request->Id, $request->form]);;

      return view('main.permisos.modal-permisos-denegar', compact('permisoDetalle','request'));
    }

    public function ModaPermisoInforme(Request $request){

      return view('main.permisos.modal-generar-informe', compact('request'));
    }

   public function denegarPermiso(Request $request){

        $tipoSolicitud = '';
        $nameProyecto = '';

        try{
            $tipoSolicitud = DB::select('select opci_nombre from opciones where opci_id =
                (select perd_respuesta from permiso_detalle where perm_id = ? and preg_id = 246 )', [$request->id])[0]->opci_nombre;

            $nameProyecto = PermisoDetalle::where('perm_id', $request->id)->where('preg_id', 419)->first()->perd_respuesta;

        }catch(Exception $ex){ }

        $idUser=  Permiso::where('perm_id', $request->id)->first()->usua_id;
        $nombreUser = User::find($idUser)->name;
        $mailUser = User::find($idUser)->email;

        $fecha = $request->observaciones;

        try{
            Mail::to($mailUser)->send(new NotificacionDenegacion($nombreUser, $nameProyecto, $fecha, $tipoSolicitud));
        }catch(Exception $ex){ }

    }

    public function guardarAsignacion(Request $request){

        $permiso = Permiso::find($request->id);

        $existe = tecnico_permiso::where('perm_id', $request->id)
        ->where('tecn_id', $request->tecnico)->where('esta_id', $permiso->esta_id)->first();

        if(isset($existe->pert_fecha)){
            return response()->json('^Ya ha sido agregado ese técnico a la visita', 500);
        }

        $tecPermisos = new tecnico_permiso();
        $tecPermisos->perm_id = $request->id;
        $tecPermisos->tecn_id = $request->tecnico;
        $tecPermisos->pert_fecha = $request->fecha;
        $tecPermisos->esta_id = $permiso->esta_id;
        $tecPermisos->save();

        $Tecnico = Usuario::find($request->tecnico);

        try{

            $tecPermisos->usua_id = $Tecnico->usua_id;
            $tecPermisos->usua_nombre = $Tecnico->usua_nombre;
            Mail::to($Tecnico->usua_mail)->send(new TecnicoCorreo());

        }catch(Exception $ex){ }

        return view('main.permisos.tecnicos.info-tecnicos', ['tecnicos' => [$tecPermisos], 'request' => $request]);
     }

    public function eliminarAsignacion($idTecnico, $idPermiso){
         $permiso = Permiso::find($idPermiso);
        tecnico_permiso::where('perm_id', $idPermiso)->where('tecn_id', $idTecnico)->where('esta_id', $permiso->esta_id)->delete();
    }

}
