<?php

namespace App\Http\Controllers;

use App\Clases\MetodosGenerales;
use App\Imports\PermisosImport;
use App\Mail\DenuncianteCorreo;
use App\Models\Documentos;
use App\Models\Estado;
use App\Models\EstadoControl;
use App\Models\Estados;
use App\Models\Formulario;
use App\Models\Mensaje;
use App\Models\Opcion;
use App\Models\Permiso;
use App\Models\PermisoDetalle;
use App\Models\PermisoFormulario;
use App\Models\Seccion;
use App\Models\SeccionFormulario;
use App\Models\Seguimientos;
use App\Models\Usuario;
use PDF;
use Carbon\Carbon;
use Exception;
use Mail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Calculation\LookupRef\Formula;

class PermisoController extends Controller
{

    public function index(){
        $Permisos = $this->GetInfo();
        return view('main.permisos.index', compact('Permisos'));
    }

    public function GuardarSolvencia($id, $estado, $pregunta){
        DB::insert('insert into permiso_solvencia (perm_id, preg_id, estg_id, pers_fecha) values(?,?,?,?)',
        [$id, $pregunta, $estado, date('Y-m-d H:i:s')]);
    }

    private function GetInfo($palabra = ''){

        $data = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
        ->join('estados as e', 'e.esta_id', 'permisos.esta_id')
        ->join('estado_general as eg', 'eg.estg_id', 'e.estg_id')
        ->leftjoin('usuarios as u', 'u.usua_id', 'permisos.usua_id')
        ->leftjoin('users as us', 'us.id', 'permisos.usua_id')
        ->join('estado_control as ec', 'ec.esta_id', 'permisos.esta_id')
        ->join('model_has_roles as mhr', 'mhr.model_id', DB::raw(Session::get('user-info')->usua_id))
        ->join('role_has_permissions as rhp', function($query){
            $query->on('mhr.role_id', 'rhp.role_id');
            $query->on('rhp.permission_id', 'ec.priv_id');
        })
        ->select('perm_id', 'esta_nombre', 'form_nombre', 'perm_fecha', 'usua_nombre', 'u.usua_id', 'f.form_id', 'estg_nombre', 'perm_codigo')
        ->orderBy('perm_id', 'desc');

        if(Session::get('user-info')->usua_tipo == 1){
            $data = $data->join('tecnicos_permiso as tp', 'tp.perm_id', 'permisos.perm_id')->where('tp.tecn_id', Session::get('user-info')->usua_id);
        }

        if($palabra){
            $data = $data->where('usua_nombre' , 'like', "%$palabra%")
            ->orwhere('form_nombre', 'like', "%$palabra%")
            ->orwhere('perm_codigo', 'like', "%$palabra%")
            ->orwhere('esta_nombre', 'like', "%$palabra%")
            ->orwhere('perm_fecha', 'like', "%$palabra%")
            ->orwhere('name', 'like', "%$palabra%");
        }

        return $data->distinct()->paginate(20)->withPath(route('buscar-permisos'));
    }

    public function Buscar($palabra = ''){
        $Permisos = $this->GetInfo($palabra);
        return view('main.permisos.data', compact('Permisos'));
    }

    public function ViewDetalle(){

        $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
        ->leftjoin('usuarios as u', 'u.usua_id', 'permisos.usua_id')->paginate(20);

        $Titulo = 'PERMISOS';
        return view('main.permisos.index', compact('Permisos', 'Titulo'));
    }

    public function LoadFiles(Request $request){
        $file = $request->file('permisos');
        Excel::import(new PermisosImport, $file);
    }

    public function ViewLoadfile(){
        return view('main.permisos.import');
    }

    public function detalle($id, $idForm, Request $request){

        Session::put('perm_id', $id);

        $TipoP = new Opcion();
        $NombreP = '';

        $permiso = Permiso::leftjoin('estados as e', 'e.esta_id', 'permisos.esta_id')
        ->leftjoin('estado_general as eg', 'eg.estg_id', 'e.estg_id')
        ->where('permisos.perm_id', $id)->join('permisos_formulario as pf', function($query){
            $query->on( 'pf.perm_id', 'permisos.perm_id');
            $query->where( 'pf.form_id', '7');
        })->select('permisos.perm_id', 'permisos.esta_id', 'perf_id', 'permisos.form_id',
        'esta_nombre', 'perm_codigo', 'estg_nombre')->first();


        try{
            $PFormulario = PermisoFormulario::find($permiso->perf_id);
            Log::info($PFormulario);

            $NombreP = PermisoDetalle::where('perf_id', $PFormulario->perf_id)->where('preg_id', 419)->first()->perd_respuesta;

            $TipoP = DB::select('select opci_nombre, opci_id from opciones where opci_id =
                (select perd_respuesta from permiso_detalle where perf_id = ? and preg_id = 246 )', [$PFormulario->perf_id])[0];


        }catch(Exception $ex){
            Log::info($ex);
        }


        $seguimientos = $this->GetSeguimientos($id);

        $estados = EstadoControl::join('permisos', 'estado_control.esta_id', 'permisos.esta_id')
            ->join('estados', 'estados.esta_id', 'estado_control.estc_id')
            ->leftjoin('permissions as ps', 'ps.id', 'priv_id')
            ->where('permisos.perm_id', $id)->where('estado_control.form_id', $idForm)->where('estc_cambio', 0)->orderBy('esta_nombre')->get();

        $documentos = Documentos::join('permisos', 'permisos.perm_id','documentos.perm_id')
            ->leftjoin('usuarios', 'usuarios.usua_id', 'documentos.tecn_id')
            ->leftjoin('tipo_documentos as td', 'td.tipd_id', 'documentos.tipd_id')
            ->join('estados', 'estados.esta_id' ,'documentos.esta_id', 'tipd_nombre')->where('documentos.perm_id', $id)->get();


        $formularios = PermisoFormulario::where('perm_id', $id)->join('formularios as f', 'f.form_id', 'permisos_formulario.form_id')->get();

        return view('main.permisos.detalle', [
            'permiso' => $permiso,
            'seguimientos' => $seguimientos,
            'estados' => $estados,
            'documentos' => $documentos,
            'formularios' => $formularios,
            'request' => $request,
            'Tipo' => $TipoP,
            'NombreP' => $NombreP]);
    }

    private function GetSeguimientos($id){
        return Seguimientos::join('estados','estados.esta_id','seguimientos.esta_id')
        ->join('permisos','permisos.perm_id','seguimientos.perm_id')
        ->join('usuarios as u', 'u.usua_id', 'tecn_id')
        ->where('permisos.perm_id', $id)->orderBy('segu_id', 'desc')->paginate(15)->withPath(route('page-seguimiento'));
    }

    public function PageSeguimiento(){
        $seguimientos = $this->GetSeguimientos(Session::get('perm_id'));
        return view('main.permisos.detalle-tab.seguimientos.data', compact('seguimientos'));
    }

    public function GenerarActa($id, $tipo){

        $vistas = ['OT2', 'RRP', 'CDC', 'CDH', 'SHO', 'SRFC', 'CDF', 'CDF', 'NG'];
        $vista = 'ActaProyecto';

        // try{
        //     $tipo = PermisoDetalle::where('preg_id', 246)->where('perm_id', $id)->first()->perd_respuesta;
        // }catch(Exception $ex){ }

        // if($tipo == 246){
        //     $vista = 'ActaComunidad';
        // }



        $preguntas = $this->GetArrayPreguntas($id);
        $permiso = Permiso::find($id);

        $pdf = PDF::loadView('main.permisos.actas.' . $vistas[$tipo], compact('preguntas', 'permiso'));
        return $pdf->stream('acta.pdf');

    }

    public function GetArrayPreguntas($id){
        $preguntas = PermisoFormulario::where('perm_id', $id)->join('permiso_detalle as pd', 'pd.perf_id', 'permisos_formulario.perf_id')
        ->join('preguntas as p', 'p.preg_id', 'pd.preg_id')->get(['p.preg_id', 'tipe_id', DB::raw('if(TIPE_ID = 2 OR TIPE_ID = 14, (select opci_nombre from opciones where opci_id = pd.perd_respuesta), perd_respuesta) as respuesta')]);
        $Datos = [];
        foreach($preguntas as $item){
            $Datos[$item->preg_id] = $item->respuesta;
        }
        return $Datos;
    }

    public function detallePreguntas($idForm, $IdPermiso, $perf_id){

        $Permiso = Permiso::join('formularios','permisos.form_id','formularios.form_id')
        ->where('perm_id', $IdPermiso)->first();

        $Preguntas = DB::select('select preg_nombre, tipe_id, if(TIPE_ID = 2 OR TIPE_ID = 14, (select opci_nombre from opciones where opci_id = pd.perd_respuesta), perd_respuesta) as RESULTADO from preguntas p inner join pregunta_seccion ps on  ps.preg_id = p.preg_id inner join seccion_formulario
        sf on sf.secc_id = ps.secc_id inner join permiso_detalle pd on pd.preg_id = p.preg_id where sf.form_id = ?
        and pd.perf_id = ?', [$idForm, $perf_id]);

        return view('main.permisos.detalle_preguntas',
        compact('Permiso','Preguntas'));
    }

    public function detallePreguntasFormulario($id, $idForm)
    {
        $PreguntasForm = DB::select('select preg_nombre, if(tipe_id = 2, (select opci_nombre from opciones '
        . ' where opci_id = pd.perd_respuesta), perd_respuesta) as RESULTADO from preguntas p inner join pregunta_seccion ps on  ps.preg_id = p.preg_id inner join seccion_formulario
        sf on sf.secc_id = ps.secc_id inner join permiso_detalle pd on pd.preg_id = p.preg_id where sf.form_id = ?
        and pd.perm_id = ?', [$id, $idForm]);

        return view('main.permisos.detalle_preguntasForm',
        ['PreguntasForm' => $PreguntasForm]);
    }

    public function tabFormularios($id){
        return view('main.permisos.detalle-tab.tab-formularios');
    }

    public function ModalEstados(Request $request){
        $idPermiso = $request->id;
        $idFormulario = $request->idForm;
        $estado = $request->estado;
        $nombre = Estado::find($request->estado)->esta_nombre;
        return view('main.permisos.modal-comentario', compact('request', 'idPermiso', 'idFormulario', 'estado', 'nombre'));
    }

    public function EstadosSolicitud(Request $request){

        return DB::transaction(function() use ($request){

            $seguimiento = new Seguimientos();
            $seguimiento->segu_fecha = Carbon::now()->toDateString();
            $seguimiento->perm_id = $request->id;
            $seguimiento->segu_comentario = $request->Comentario;
            $seguimiento->tecn_id = Session::get('user-info')->usua_id;
            $seguimiento->esta_id = $request->estado;
            $seguimiento->save();

            if($request->type){

                $permiso = Permiso::where('perm_id', $request->id)
                ->Leftjoin('usuarios as u', 'u.usua_id', 'permisos.usua_id')
                ->join('estado_control as ec', function($query) use($request){
                    $query->on('ec.esta_id', 'permisos.esta_id');
                    $query->where('ec.estc_id', $request->estado);
                })->select('usua_mail', 'estc_correo')->first();

                DB::update('update permisos set esta_id = ? where perm_id = ?', [$request->estado, $request->id]);

                try{
                    if($permiso->estc_correo){

                        $mensaje = new Mensaje();
                        $mensaje->mens_cuerpo = $request->Comentario;
                        $mensaje->usua_emisor = Session::get('user-info')->UsuarioId;
                        $mensaje->usua_receptor = $permiso->UsuarioId;
                        $mensaje->mens_fecha = date('Y-m-d');
                        $mensaje->mens_hora = date('H:i:s');
                        $mensaje->save();

                        Mail::to($permiso->usua_mail)->send(new DenuncianteCorreo());
                    }
                }catch(Exception $ex){ }

            }else{
                return view('main.permisos.detalle-tab.seguimientos.data', ['seguimientos' => []]);
            }
        });
    }

    public function ModaPermisoSolventes(Request $request){
        $id = $request->Id;


        $listaSolventes = PermisoDetalle::join('permisos_formulario as pf', function($query) use ($request){
                            $query->on('permiso_detalle.perf_id', 'pf.perf_id');
                            $query->where('pf.perm_id', $request->Id);
                        })
                        ->join('permisos as pe', 'pe.perm_id', 'pf.perm_id')
                        ->join('estados as e', 'e.esta_id', 'pe.esta_id')
                        ->join('preguntas as p', 'p.preg_id', 'permiso_detalle.preg_id')
                        ->leftjoin('permiso_solvencia as ps', function($query){
                            $query->on( 'ps.preg_id', 'p.preg_id');
                            $query->on('ps.estg_id', 'e.estg_id');
                            $query->on('ps.perm_id', 'pe.perm_id');
                        })
                        ->whereIn('permiso_detalle.preg_id', [174,193,421])
                        ->get(['preg_nombre', 'perd_respuesta', 'e.estg_id', 'pers_fecha', 'p.preg_id']);

        $estg_id = (isset($listaSolventes[0]) ? $listaSolventes[0]->estg_id : 0);

        return view('main.permisos.solventes.modal-solventes', compact('id','listaSolventes', 'estg_id'));
    }

     public function Ordenar($palabra){
        Log::info($palabra);

        if(Session::get('order') == 'asc'){
            Session::put('order', 'desc');
            $orden = Session::get('order');
        }else{
            Session::put('order', 'asc');
            $orden = Session::get('order');
        }

       $asi = array(0=>'perm_codigo', 1=>'name', 2=>'form_nombre', 3=>'perm_fecha', 4=>'esta_nombre', 5=>'estg_nombre');

                $Permisos = Permiso::join('formularios as f', 'f.form_id', 'permisos.form_id')
                ->leftjoin('users as us', 'us.id', 'permisos.usua_id')
                ->join('estados as e', 'e.esta_id', 'permisos.esta_id')
                ->join('estado_general as eg', 'eg.estg_id', 'e.estg_id')
                ->orderBy($asi[$palabra], $orden )
                ->paginate(10)
                ->withPath(route('buscar-permisos'));

                 return view('main.permisos.data', compact('Permisos'));
        }


}
