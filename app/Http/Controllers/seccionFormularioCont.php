<?php

namespace App\Http\Controllers;

use App\Models\Pregunta;
use App\Models\PreguntaSeccion;
use App\Models\Seccion;
use App\Models\SeccionFormulario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class seccionFormularioCont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_req = Seccion::orderBy('secc_id')->paginate(50)->withPath(route('buscar-secciones'));
        return view('main.seccion_formulario.index', compact('data_req'));
    }


    public function Buscar($palabra = ''){
        $data_req = Seccion::orderBy('secc_after', 'asc')->where('secc_nombre', 'like', "%$palabra%")->paginate(50)->withPath(route('buscar-secciones'));
        return view('main.seccion_formulario.data', compact('data_req'));
    }

    public function TableInfo(){
        $data_req = Seccion::orderBy('secc_after', 'asc')->paginate(50)->withPath(route('buscar-secciones'));
        return view('main.seccion_formulario.info-table', compact('data_req'));
    }

    public function CambiarPregunta($tipo, $seccion, $pregunta){

        if($tipo){
            DB::delete('delete from pregunta_seccion where preg_id = ? and secc_id = ?', [$pregunta, $seccion]);
        }else{
            $total = DB::select('select count(*)  as total from pregunta_seccion where secc_id = ?', [$seccion])[0]->total;
            DB::insert('insert into pregunta_seccion values(?,?, ?)',
             [$pregunta, $seccion, $total]);
        }
    }

    public function GetPreguntasSeccion($Info){

        $Info = Seccion::where('secc_id', $Info)->select('secc_id as id', 'secc_nombre as nombre')->first();

        $DataAdd = Pregunta::where('secc_id', $Info->id)->join('pregunta_seccion as ps', 'ps.preg_id', 'preguntas.preg_id')
        ->select('preg_nombre as nombre', 'preguntas.preg_id as id')->orderBy('preg_orden')->paginate(50);
        $DataFree = $this->PreguntasFree($Info->id);

        $ruta = 'cambiar-preg-seccion';
        $rutaBack = 'seccion-info-table';
        $rutaBuscar = route('buscar-preg-seccion', [$Info->id]);
        $rutaOrden = route('update-carga-pregunta');

        return view('main.preguntas.preguntas-asign', compact('DataAdd', 'DataFree', 'Info', 'ruta', 'rutaBack', 'rutaBuscar', 'rutaOrden'));
    }

    private function PreguntasFree($id, $palabra = ''){
        return Pregunta::whereNotIn('preg_id', PreguntaSeccion::where('secc_id', $id)->select('preg_id'))->where('preg_nombre', 'like', "%$palabra%")
        ->where('preg_padre', 1)->select('preg_nombre as nombre', 'preguntas.preg_id as id')->paginate(50)->withPath(route('buscar-preg-seccion', [$id]));
    }

    public function BuscarPreguntaAdd($id, $palabra = ''){
        $Info = Seccion::where('secc_id', $id)->select('secc_id as id')->first();
        $DataFree = $this->PreguntasFree($id, $palabra);
        return view('main.preguntas.result-asing', compact('DataFree', 'Info'));
    }


    public function ShowModal(Request $request)
    {
        if(!$request->ajax()){
            abort(404);
            return;
        }

        if($request->Id > 0){
            $Info = Seccion::find($request->Id);
        }else{
            $Info = new Seccion();
        }

        return view('main.seccion_formulario.crear', compact('Info','request'));
    }

    public function guardar(Request $request)
    {
        try {

            if(!$request->ajax()){
                abort(404);
                return;
            }

            $dat = new Seccion();

            if($request->Id > 0){
                $dat = Seccion::find($request->Id);
            }

            $dat->secc_nombre = $request->nombre;
            $dat->save();

            if($request->Id == 0){
                $dat->secc_after = $request->secc_id;
                $dat->save();
            }

            return view('main.seccion_formulario.data', ['data_req' => [$dat]]);

        } catch (\Throwable $th) {
            return response()->json(['mensaje' => 'err']);
        }

    }

    public function OrdenarSecciones($palabra){

        if(Session::get('order') == 'asc'){
            Session::put('order', 'desc');
            $orden = Session::get('order');
        }else{
            Session::put('order', 'asc');
            $orden = Session::get('order');
        }

       $asi = array(0=>'secc_id', 1=>'secc_nombre');
   
                $data_req = Seccion::where('secc_id', '>=', 0)
                ->orderBy($asi[$palabra], $orden)
                ->paginate(50)
                ->withPath(route('buscar-secciones'));   
                
                 return view('main.seccion_formulario.data', compact('data_req'));
        }

}
