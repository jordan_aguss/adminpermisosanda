<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Redirect;

class UsuarioController extends Controller
{
    const PERMISSIONS = [
        'create'    => 'admin-user-create',
        'show'      => 'admin-user-show',
        'edit'      => 'admin-user-edit'
    ];

    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rows = Usuario::all();
        return view('main.user.index', [
            'rows' => $rows,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        //
        return view('main.user.create', [
            'roles' => Role::all(),
        ]);
    }


    public function SaveUser(Request $request)
    {

        try {

            $Info = Usuario::where('usua_mail', $request->correo)->first();

            if (isset($Info->usua_id)) {
                throw new Exception('^Ya existe un usuario con ese correo');
            }

            return DB::transaction(function () use ($request) {

                $user = new Usuario();
                $user->usua_mail = $request->correo;
                $user->usua_pass = password_hash($request->pass, PASSWORD_DEFAULT);
                $user->usua_nombre = $request->nombre;
                $user->save();
                $user->roles()->sync($request->roles);
                if (isset($request->campoPregunta)) {
                    foreach ($request->campoPregunta as $id => $valor) {
                        DB::insert('insert into usuario_detalle (preg_id, usua_id, usud_valor) values (?,?,?)', [$id, $user->usua_id, $valor]);
                    }
                }

                return redirect()->route('admin.user.index');
            });
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('main.user.show',[
            'row'   => Usuario::find($id),
            'roles' => Role::all(),
        ]);
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('main.user.edit',[
            'row'   => Usuario::find($id),
            'roles' => Role::all(),
        ]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'nombre' => ['required', 'string', 'max:255'],
            'correo' => ['required', 'string', 'email', 'max:255']
        ]);


        try {
            $prev_user = Usuario::firstWhere('usua_mail', $request->correo);
            if ($prev_user) {
                $get_usuario = $prev_user;
                if($id != $get_usuario->usua_id){
                    return Redirect::back()->withErrors(['error' => 'Correo ya existente en base de datos']);
                }
            }

            return DB::transaction(function () use ($request, $id) {

                $user = Usuario::find($id);
                $user->usua_mail = $request->correo;
                if(isset($request->pass)){
                    if($request->pass !== ""){
                        $user->usua_pass = password_hash($request->pass, PASSWORD_DEFAULT);
                    }
                }
                $user->usua_nombre = $request->nombre;
                $user->update();
                $user->roles()->sync($request->roles);
                if (isset($request->campoPregunta)) {
                    foreach ($request->campoPregunta as $id => $valor) {
                        DB::insert('insert into usuario_detalle (preg_id, usua_id, usud_valor) values (?,?,?)', [$id, $user->usua_id, $valor]);
                    }
                }

                return redirect()->route('admin.user.index');
            });
        } catch (Exception $ex) {
            return response()->json($ex->getMessage(), 500);
        }
    }

}
