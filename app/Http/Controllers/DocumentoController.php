<?php

namespace App\Http\Controllers;

use App\Models\Documentos;
use App\Models\Permiso;
use App\Models\TipoDocumento;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use PDF;
use Svg\Tag\Rect;

class DocumentoController extends Controller
{
    public function generarActa()
    {
        /*foreach ($variable as $key => $value) {
            # code...
        }
        $data = [

        ];
        $pdf = PDF::loadView('',$data);
        return $pdf->download('Prueba.pdf');*/
    }

    public function generarReporte()
    {
        $data = [

        ];
        /*$pdf = PDF::loadView('',$data);
        return $pdf->download('Prueba.pdf');*/
    }

    public function ModalDocumento(Request $request){

        $Tipos = TipoDocumento::all();
        return view('main.permisos.detalle-tab.documentos.modal', compact('request', 'Tipos'));
    }

    public function SaveDocument(Request $request){

        $Permiso = Permiso::join('estados as e', 'e.esta_id', 'permisos.esta_id')->select('perm_id', 'e.esta_id', 'esta_nombre')->first();

        $Documento = new Documentos();

        $files = $request->file('adjunto');
        $ext = $files->getClientOriginalExtension();
        $nombre = str_replace(['.' . $ext, ' '], ['', ''],$files->getClientOriginalName());
        $identificador = $nombre . '_' . Str::random(20) . '.' . $ext;
        $files->move(storage_path( 'documentos/' . $request->permiso), $identificador);


        $Documento->docu_fecha = date('Y-m-d H:i:s');
        $Documento->tecn_id = Session::get('user-info')->tecn_id;
        $Documento->perm_id = $request->permiso;
        $Documento->docu_nombre = $nombre . '.' . $ext;
        $Documento->docu_comentario = $request->comentario;
        $Documento->esta_id = $Permiso->esta_id;
        $Documento->tipd_id = $Permiso->tipo;
        $Documento->docu_url = $identificador;
        $Documento->save();

        $Documento->esta_nombre = $Permiso->esta_nombre;
        $Documento->esta_nombre = TipoDocumento::find($request->tipo)->tipd_nombre;

        return view('main.permisos.detalle-tab.documentos.item-documentos', ['documentos' => [$Documento]]);
    }

    public function descargarAdjunto($ruta, $permiso){
        Log::info(storage_path() . $ruta);
        return response()->download( storage_path('documentos/' . $permiso . '/' . $ruta));
    }

    public function DeleteDocument(Request $request){
        Documentos::find($request->id)->delete();
    }

}
