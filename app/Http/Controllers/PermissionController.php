<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionController extends Controller
{
    const PERMISSIONS = [
        'show'      => 'admin-permission-show',
    ];

    public function __construct(){

        $this->middleware('auth');
        $this->middleware('permission:'.self::PERMISSIONS['show'])->only(['index','show']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $permissions = Permission::where('type', 1)->orderby('name')->paginate(1000);

        return view('main.permission.index', [
            'rows' => $permissions,
        ]);

    }

    public function ShowModal(Request $request){

        if($request->Id == 0){
            $Info = new Permission();
        }else{
            $Info = Permission::find($request->Id);
        }

        return view('main.permission.modal-add', compact('Info', 'request'));
    }

    public function SaveInfo(Request $request){

        if($request->id == 0){
            $Info = new Permission();
            $Info->guard_name = 'web';
            $Info->type = 1;
        }else{
            $Info = Permission::find($request->id);
        }

        $Info->name = $request->nombre;
        $Info->description = $request->descripcion;
        $Info->save();

        return view('main.permission.data', ['rows' => [$Info]]);
    }

    public function DeleteInfo($id){
        Permission::find($id)->delete();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
        return view('main.permission.show', [
            "row" => $permission
        ]);
    }
}
