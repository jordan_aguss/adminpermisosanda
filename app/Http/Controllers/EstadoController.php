<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Estado;
use App\Models\EstadoControl;
use App\Models\EstadoGeneral;
use App\Models\Formulario;
use Exception;
use Spatie\Permission\Models\Permission;

class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_req = Estado::orderBy('esta_id', 'asc')->paginate(50)->withPath(route('buscar-secciones'));
        return view('main.estados.index', compact('data_req'));
    }

    public function AsignStatus(Request $request){

        $Info = new EstadoControl();
        $Info->esta_id = $request->estado;
        $Info->estc_id = $request->Id;
        $Info->form_id = $request->form;
        $Info->form_new = $request->newform;
        $Info->estc_tipo = $request->tipo;
        $Info->priv_id = $request->privilegio;
        $Info->estc_correo = $request->correo;
        $Info->estc_cambio = $request->cambio;
        $Info->estc_documento = $request->documento;
        $Info->estc_pago = $request->pago;
        $Info->estc_color = $request->color;
        $Info->esta_vencido = $request->vencido;

        if($request->tipoModal){
            EstadoControl::where('esta_id', $request->estado)->where('estc_id', $request->Id)
            ->where('form_id', $request->form)->update(['estc_correo' => $request->correo, 'estc_cambio' => $request->cambio,
            'form_new' => $request->newform, 'priv_id' => $request->privilegio, 'estc_color' => $request->color,'estc_tipo' => $request->tipo,
            'esta_vencido' => $request->vencido, 'estc_vencimiento' => $request->FLimite, 'estc_documento' => $request->documento, 'estc_pago' => $request->pago]);
        }else{
            $Info->save();
        }

        $Info->esta_id  = $request->Id;
        $Info->esta_nombre = $request->nombre;

        return view( 'main.estados.asignar-estados.lista', ['Estados' => [$Info], 'request' => $request]);
    }



    public function SubEstados(Request $request){

        $Estados = Estado::join('estado_control as ec', 'ec.estc_id', 'estados.esta_id')->where('ec.esta_id', $request->estado)
        ->where('form_id', $request->form)->get(['ec.estc_id as esta_id', 'esta_nombre', 'estc_tipo']);
        return view('main.estados.asignar-estados.lista', compact('Estados', 'request'));
    }

    public function TableAdd(Request $request){
        return view('main.estados.asignar-estados.modal-asign', compact('request'));
    }

    public function EliminarEstado($estado, $form, $padre){
        EstadoControl::where('esta_id', $padre)->where('estc_id', $estado)->where('form_id', $form)->delete();
        return '';
    }

    public function EstadosForm($id){

        $Info = Formulario::where('form_id', $id)->leftjoin('estados as e', 'e.esta_id', 'formularios.esta_id')->first();
        return view('main.estados.asignar-estados.index', compact('Info'));
    }

    public function ShowModal(Request $request)
    {
        if($request->Id > 0){
            $Info = Estado::find($request->Id);
        }else{
            $Info = new Estado();
        }

        $Estados = EstadoGeneral::orderBy('estg_nombre')->get();

        return view('main.estados.crear', compact('Info','request', 'Estados'));
    }

    public function guardar(Request $request)
    {

            if($request->Id > 0){
                $dat = Estado::find($request->Id);
            }else{
                $dat = new Estado();
            }

            $dat->esta_nombre = $request->nombre;
            $dat->estg_id = $request->estadog;
            $dat->esta_descripcion = $request->descripcion;
            $dat->save();

            return view('main.estados.data', ['data_req' => [$dat]]);
    }


    public function GetSubEstado(Request $request){
        $Info = Estado::where('estados.esta_id', $request->id)->where('ec.esta_id', $request->estado)->leftjoin('estado_control as ec',function($query) use($request){
            $query->on( 'ec.estc_id', 'estados.esta_id');
            $query->where('form_id', $request->form);
        })
        ->select('estados.esta_id', 'esta_nombre', 'form_new', 'priv_id', 'estc_tipo', 'estc_cambio', 'estc_color', 'estc_vencimiento', 'estc_pago', 'estc_documento')->first();

        if(!isset($Info->esta_id)){
            $Info = Estado::find($request->id);
        }

        $Forms = Formulario::all();
        $Permisos = Permission::orderBy('name')->get();

        return view('main.estados.asignar-estados.modal-add', compact('Info', 'request', 'Forms', 'Permisos'));
    }

    public function Buscar($palabra = '', $vista = 0){

        $vistas = ['main.estados.data', 'main.estados.asignar-estados.options'];

        $data_req = Estado::orderBy('esta_id', 'asc')->where('esta_nombre', 'like', "%$palabra%")->paginate(50)->withPath(route('buscar-estado'));
        return view( $vistas[$vista], compact('data_req'));
    }

    public function SEstadoAsign(Request $request){
        $Estados = Estado::orderBy('esta_id', 'asc')->where('esta_nombre', 'like', "%$request->palabra%")->get();
        return view('main.estados.asignar-estados.estado-busqueda', compact('Estados', 'request'));
    }


}
