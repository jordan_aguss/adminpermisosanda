<?php
    namespace App\Http\LibreriaPDF;

    use App\Http\LibreriaPDF\fpdf;
    
    class pdf extends fpdf
    {
        // variable to store widths and aligns of cells, and line height
        private $widths;
        private $aligns;
        private $lineHeight;
        var $cellspacing = 1;
        
        // Page header
        function Header()
        {
            // $this->SetFont('Arial','',8);
            // // Move to the right
            // $this->Cell(165);
            // Line break
            // $this->Ln(20);
        }

        // Page footer
        function Footer()
        {
            $this->SetFont('Arial','',8);
            $this->SetY(-18);
            $this->Write(5,'Vers. 1.0 SEA-2021');
            $this->SetX(-30);
            // Page number
            $this->AliasNbPages();
            $this->Write(5,utf8_decode('Pag.').$this->PageNo().'/{nb}');
            $this->SetY(-15);
        }

        //Set the array of column widths
        function SetWidths($w){
            $this->widths=$w;
        }
        //Set the array of column alignments
        function SetAligns($a){
            $this->aligns=$a;
        }
        //Set line height
        function SetLineHeight($h){
            $this->lineHeight=$h;
        }
        //Calculate the height of the row
        function Row($data, $FontX = 0)
        {
            // number of line
            $nb=0;
            // loop each data to find out greatest line number in a row.
            for($i=0;$i<count($data);$i++){
                // NbLines will calculate how many lines needed to display text wrapped in specified width.
                // then max function will compare the result with current $nb. Returning the greatest one. And reassign the $nb.
                $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
            }
            
            //multiply number of line with line height. This will be the height of current row
            $h=$this->lineHeight * $nb;
            //Issue a page break first if needed
            $this->CheckPageBreak($h);
            //Draw the cells of current row
            for($i=0;$i<count($data);$i++)
            {
                // width of the current col
                $w=$this->widths[$i];
                // alignment of the current col. if unset, make it left.
                $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
                //Save the current position
                $x=$this->GetX();
                $y=$this->GetY();
                //Draw the border
                $this->Rect($x,$y,$w,$h);

                if($i == 6){
                    $this->SetTextColor(177,19,15);
                }

                if($i == 0){   
                    $this->SetFont('Arial','B',6);
                }
                
//                $pdf->tMargin = 0;
//                $pdf->bMargin = 0;
                $this->MultiCell($w,5,$data[$i],0,$a);
//                $pdf->tMargin = $tM;
//                $pdf->bMargin = $bM;
                
                $this->SetTextColor(4,4,4);
                if($FontX){
                    $this->SetFont('Arial','B',6);
                }else{
                    $this->SetFont('Arial','',6);
                }
                
                //Put the position to the right of the cell
                $this->SetXY($x+$w,$y);
            }
            //Go to the next line
            $this->Ln($h);
        }

        function CheckPageBreak($h)
        {
            //If the height h would cause an overflow, add a new page immediately
            if($this->GetY()+$h>$this->PageBreakTrigger)
                $this->AddPage("PORTRAIT","letter");
        }

        function NbLines($w,$txt)
        {
            //calculate the number of lines a MultiCell of width w will take
            $cw=&$this->CurrentFont['cw'];
            if($w==0)
                $w=$this->w-$this->rMargin-$this->x;
            $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
            $s=str_replace("\r",'',$txt);
            $nb=strlen($s);
            if($nb>0 and $s[$nb-1]=="\n")
                $nb--;
            $sep=-1;
            $i=0;
            $j=0;
            $l=0;
            $nl=1;
            while($i<$nb)
            {
                $c=$s[$i];
                if($c=="\n")
                {
                    $i++;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                    continue;
                }
                if($c==' ')
                    $sep=$i;
                $l+=$cw[$c];
                if($l>$wmax)
                {
                    if($sep==-1)
                    {
                        if($i==$j)
                            $i++;
                    }
                    else
                        $i=$sep+1;
                    $sep=-1;
                    $j=$i;
                    $l=0;
                    $nl++;
                }
                else
                    $i++;
            }
            return $nl;
        }   
        
        function RoundedBorderCell($w, $h=0, $txt='', $ln=0, $align='',$fill=false, $link='') {
            $spaceLeft = ($this->h - $this->getY() - $this->bMargin);
            $cellHeight = $h + $this->cellspacing;
            if ($spaceLeft < $cellHeight) {
                $this->AddPage();
            }
            $this->RoundedRect($this->getX() + $this->cellspacing / 2,
                $this->getY() + $this->cellspacing / 2,
                $w - $this->cellspacing, $h, 1, 'DF');
            $this->Cell($w, $cellHeight, $txt, $ln, 0, $align, $fill, $link);
        }
        
        function BohdanTable($header, $data, $widths, $aligns,$BorderWidth = 0.25,$HeaderColor = array(255,255,255), $rowAltura = 0, $LMargin = 0) {
            $this->SetLineWidth($BorderWidth);
            
            if($rowAltura){
                $rowHeight = $rowAltura;
            }else{
                $rowHeight = $this->FontSizePt - 2;
            }

            $this->SetFillColor($HeaderColor[0],$HeaderColor[1],$HeaderColor[2]);
            for ($i = 0, $j = sizeof($header); $i < $j; $i++) {
                $this->RoundedBorderCell($widths[$i], $rowHeight,
                    $header[$i], 0, $aligns[$i]);
            }
            $this->Ln();
            $rowHeight = $this->FontSizePt - 2;
            
            if($LMargin){
                
            }else{
                $this->SetX($LMargin);
            }
            
            foreach ($data as $rowId => $row) {
                $this->SetFillColor($rowId % 2 == 1 ? 240 : 255);
                for ($i = 0, $j = sizeof($row); $i < $j; $i++) {
                    $this->RoundedBorderCell($widths[$i], $rowHeight,
                        $row[$i], 0, $aligns[$i]);
                }
                $this->Ln();
            }
        }

        function SetCellspacing($cellspacing) {
            $this->cellspacing = $cellspacing;
        }
        
        function RoundedRect($x, $y, $w, $h, $r, $style = '')
	{
		$k = $this->k;
		$hp = $this->h;
		if($style=='F')
			$op='f';
		elseif($style=='FD' || $style=='DF')
			$op='B';
		else
			$op='S';
		$MyArc = 4/3 * (sqrt(2) - 1);
		$this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));
		$xc = $x+$w-$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));

		$this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);
		$xc = $x+$w-$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
		$this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);
		$xc = $x+$r ;
		$yc = $y+$h-$r;
		$this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
		$this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);
		$xc = $x+$r ;
		$yc = $y+$r;
		$this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
		$this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
		$this->_out($op);
	}

	function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
	{
		$h = $this->h;
		$this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
			$x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
	}                
    }