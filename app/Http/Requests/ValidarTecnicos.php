<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarTecnicos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'TECN_ALIAS' => 'required',
            'USUA_ID' => 'required',
            'SUPE_ID'=> 'required',
            'GRUE_ID'=> 'required',
            'ABOA_ID'=> 'required',
            'TECN_ACTIVO'=> 'required',
            'TECN_SUPERVISOR'=> 'required',
            'TECN_SUPERVISORCATEGORIA'=> 'required',
        ];
    }
}
