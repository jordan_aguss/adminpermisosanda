<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarDepartamentos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'DEPA_NOMBRE' => 'required',
            'DEPA_REGION' => 'required',
            'PAIS_ID'=>'required',
            'DEPA_ESTADO'=> 'required'
        ];
    }
}
