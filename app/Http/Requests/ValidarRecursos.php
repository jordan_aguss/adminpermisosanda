<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarRecursos extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'RECU_NOMBRE'=>'required',
            'MODU_ID'=>'required',
            'RECU_MODELO'=>'required',
            'RECU_URL'=>'required',
            'RECU_TIPO'=>'required',
            'RECU_UBICACION'=>'required',
            'RECU_ACTIVO'=>'required',
        ];
    }
}
