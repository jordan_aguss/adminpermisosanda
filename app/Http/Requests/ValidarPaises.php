<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidarPaises extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //ESTAR LOGUEADO



        
        return true;
        
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'PAIS_NOMBRE' => 'required',
            'PAIS_ABREV' => 'required',
            'PAIS_CODAREA' => 'required',
            'PAIS_ESTADO'=> 'required'
        ];
    }
}
